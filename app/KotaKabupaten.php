<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class KotaKabupaten extends Model
{
    use SoftDeletes;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'province_id','name',
    ];

    protected $dates = ['deleted_at'];

    public function provinsi() {
        return $this->hasMany('App\Provinsi');
    }

    public function locus() {
        return $this->hasMany('App\Locus');
    }
    public function instansi() {
        return $this->hasMany('App\Instansi');
    }
    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan');
    }

    public function universitas() {
        return $this->hasMany('App\Universitas');
    }

    public function userProfile() {
        return $this->hasMany(UserProfile::class);
    }
}
