<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class KategoriPertanyaan extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name', 'alias', 'logo', 'bobot_minimal'
    ];

    protected $dates = ['deleted_at'];

    public function pertanyaan()
    {
        return $this->hasMany(Pertanyaan::class);
    }
}
