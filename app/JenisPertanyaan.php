<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class JenisPertanyaan extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name', 'alias', 'logo',
    ];

    protected $dates = ['deleted_at'];

    public function pertanyaan()
    {
        return $this->belongsTo(Pertanyaan::class);
    }
}
