<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Notifications\Notifiable;

class KategoriSurvey extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name',
    ];

    protected $dates = ['deleted_at'];

    public function survey()
    {
        return $this->hasMany(Survey::class);
    }
}
