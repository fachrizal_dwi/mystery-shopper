<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Universitas extends Model
{
    use SoftDeletes;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','provinsi_id','kota_kabupaten_id','kecamatan_id','kelurahan_id','alamat','phone','logo',
    ];

    protected $dates = ['deleted_at'];

    public function provinsi()
    {
        return $this->belongsTo('App\Provinsi');
    }
    public function kotaKabupaten()
    {
        return $this->belongsTo('App\KotaKabupaten');
    }
    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan');
    }
    public function kelurahan()
    {
        return $this->belongsTo('App\Kelurahan');
    }

    public function userProfile()
    {
        return $this->hasMany(UserProfile::class);
    }

}
