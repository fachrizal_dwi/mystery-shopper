<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class SurveyLocusDokumentasi extends Model
{
    use SoftDeletes;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_locus_id', 'path', 'longitude', 'latitude',
    ];

    protected $dates = ['deleted_at'];

    public function surveyLocus() {
        return $this->belongsTo(SurveyLocus::class);
    }
}
