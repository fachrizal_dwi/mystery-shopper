<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Survey extends Model
{
    use SoftDeletes;
    use Notifiable;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_status_id', 'kategori_survey_id','name','from','until', 'bobot_minimal', 'is_publish'
    ];

    protected $dates = ['deleted_at'];

    public function kategoriSurvey()
    {
        return $this->belongsTo(KategoriSurvey::class);
    }

    public function surveyStatus()
    {
        return $this->belongsTo(SurveyStatus::class);
    }

    public function surveyLocus()
    {
        return $this->hasMany(SurveyLocus::class);
    }
    public function surveyApplicants()
    {
        return $this->hasMany(SurveyApplicant::class);
    }
    public function pertanyaan()
    {
        return $this->hasMany(Pertanyaan::class);
    }
}
