<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Locus extends Model
{
    use SoftDeletes;
    use Notifiable;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'instansi_code', 'name',
    ];

    protected $dates = ['deleted_at'];

    public function kategoriLocus()
    {
        return $this->belongsTo('App\KategoriLocus');
    }

    public function surveyLocus()
    {
        return $this->hasMany(SurveyLocus::class);
    }
}
