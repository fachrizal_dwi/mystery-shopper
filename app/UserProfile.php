<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class UserProfile extends Model
{
    use SoftDeletes;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'universitas_id', 'nama_lengkap','nik','nim','phone','emergency_call','tanggal_lahir','tempat_lahir','provinsi_id',
        'kota_kabupaten_id','kecamatan_id','kelurahan_id','alamat','photo_profile','photo_ktp','photo_selfie','photo_legal','longitude','latitude',
    ];

    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function universitas() {
        return $this->belongsTo(Universitas::class);
    }

    public function provinsi() {
        return $this->belongsTo(Provinsi::class);
    }

    public function kecamatan() {
        return $this->belongsTo(Kecamatan::class);
    }

    public function kelurahan() {
        return $this->belongsTo(Kelurahan::class);
    }

    public function kotaKabupaten() {
        return $this->belongsTo(KotaKabupaten::class);
    }
}
