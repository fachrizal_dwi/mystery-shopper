<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Notification extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'user_id', 'survey_id',
    ];

    protected $dates = ['deleted_at'];
}
