<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Pertanyaan extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'survey_id', 'kategori_pertanyaan_id','jenis_pertanyaan_id','bobot','name', 'alias', 'logo',
    ];

    protected $dates = ['deleted_at'];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function kategoriPertanyaan()
    {
        return $this->belongsTo(KategoriPertanyaan::class);
    }

    public function jenisPertanyaan()
    {
        return $this->belongsTo(JenisPertanyaan::class);
    }

    public function penilaian()
    {
        return $this->hasMany(Penilaian::class)->orderBy('id');
    }
    public function surveyLocusPenilaians()
    {
        return $this->hasMany(SurveyLocusPenilaian::class);
    }
}
