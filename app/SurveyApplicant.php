<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class SurveyApplicant extends Model
{
    use SoftDeletes;
    use Notifiable;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','survey_id','survey_applicant_status_id',
    ];

    protected $dates = ['deleted_at'];

    public function surveyApplicantStatus()
    {
        return $this->belongsTo('App\SurveyApplicantStatus');
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

}
