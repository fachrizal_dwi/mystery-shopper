<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class SurveyLocus extends Model
{
    use SoftDeletes;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_id', 'locus_id', 'user_id', 'status_survey_locus', 'status'
    ];

    protected $dates = ['deleted_at'];


    public function survey() {
        return $this->belongsTo(Survey::class);
    }

    public function locus() {
        return $this->belongsTo(Locus::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function surveyLocusDokumentasi() {
        return $this->hasMany(SurveyLocusDokumentasi::class);
    }

    public function surveyLocusPenilaian() {
        return $this->hasMany(SurveyLocusPenilaian::class);
    }

}
