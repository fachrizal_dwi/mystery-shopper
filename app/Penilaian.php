<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Penilaian extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name', 'pertanyaan_id','bobot',
    ];

    protected $dates = ['deleted_at'];

    public function pertanyaan()
    {
        return $this->belongsTo(Penilaian::class);
    }
}
