<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class SurveyLocusPenilaian extends Model
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_locus_id', 'pertanyaan_id', 'penilaian_id',
    ];

    protected $dates = ['deleted_at'];

    public function pertanyaan()
    {
        return $this->belongsTo(Pertanyaan::class);
    }

    public function surveyLocus()
    {
        return $this->belongsTo(SurveyLocus::class);
    }
}
