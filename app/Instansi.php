<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Instansi extends Model
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'kategori_instansi_id', 'provinsi_id', 'kota_kabupaten_id',
    ];

    protected $dates = ['deleted_at'];

    public function provinsi() {
        return $this->belongsTo(Provinsi::class);
    }

    public function kecamatan() {
        return $this->belongsTo(Kecamatan::class);
    }

    public function kelurahan() {
        return $this->belongsTo(Kelurahan::class);
    }

    public function kotaKabupaten() {
        return $this->belongsTo(KotaKabupaten::class);
    }
    public function kategoriInstansi() {
        return $this->belongsTo(KategoriInstansi::class);
    }
}
