<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewBobotPenilaianBySurveyLocus extends Model
{
    protected $table = 'view_bobot_penilaian_by_survey_locuses';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pertanyaan()
    {
        return $this->belongsTo(Pertanyaan::class);
    }

    public function kategoriPertanyaan()
    {
        return $this->belongsTo(KategoriPertanyaan::class);
    }
}
