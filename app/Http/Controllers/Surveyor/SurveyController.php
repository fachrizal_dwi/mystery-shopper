<?php

namespace App\Http\Controllers\Surveyor;

use App\Http\Controllers\Controller;
use App\Pertanyaan;
use App\Survey;
use App\SurveyApplicant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;

class SurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('surveyor.survey.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Survey::with('kategoriSurvey', 'surveyStatus')->find($id);
        $pertanyaan = Pertanyaan::with('kategoriPertanyaan', 'jenisPertanyaan', 'penilaian')->where('survey_id', $id)->get();
        return view('surveyor.survey.show', compact('model','pertanyaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input['survey_id'] = $id;
        $input['user_id'] = Auth::user()->id;
        $input['survey_applicant_status_id'] = 1;
        SurveyApplicant::create($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dataTable()
    {
        $model = Survey::query()
            ->select('surveys.*','kategori_surveys.name as kategori_survey_name','survey_statuses.name as survey_status_name','survey_applicant_statuses.name as survey_applicant_statuse_name')
            ->leftJoin('kategori_surveys', 'surveys.kategori_survey_id', '=', 'kategori_surveys.id')
            ->leftJoin('survey_statuses', 'surveys.survey_status_id', '=', 'survey_statuses.id')
            ->leftJoin('survey_applicants', function ($leftJoin) {
                $leftJoin->on('surveys.id', '=', 'survey_applicants.survey_id')
                    ->where('survey_applicants.user_id', '=', Auth::user()->id);
                })
            ->leftJoin('survey_applicant_statuses', 'survey_applicants.survey_applicant_status_id', '=', 'survey_applicant_statuses.id')
        ->where('surveys.is_publish', '=', true)
        ->where('surveys.until', '>=', now());
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('surveyor.survey._action', [
                    'model' => $model,
                    'url_show' => route('surveyor.survey.show', $model->id),
                    'url_apply' => route('surveyor.survey.update', $model->id)
                ]);
            })
            ->editColumn('from', function ($model) {
                return $model->from ? with(new Carbon($model->from))->format('j F Y') : '';
            })
            ->editColumn('until', function ($model) {
                return $model->until ? with(new Carbon($model->until))->format('j F Y') : '';
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
