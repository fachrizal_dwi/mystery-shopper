<?php

namespace App\Http\Controllers\Surveyor;

use App\Http\Controllers\Controller;
use App\Instansi;
use App\Locus;
use App\Pertanyaan;
use App\Survey;
use App\SurveyApplicant;
use App\SurveyLocus;
use App\SurveyLocusDokumentasi;
use App\SurveyLocusPenilaian;
use App\UserProfile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;


class LocusController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('surveyor.locus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new SurveyLocus();
        $profile = UserProfile::select('provinsi_id', 'user_id')->where('user_id', '=', Auth::user()->id)->first();
        $instansi = Instansi::where('provinsi_id', '=', $profile->provinsi_id)->pluck('name', 'code');
        $survey = SurveyApplicant::select('survey_id','name')
            ->leftJoin('surveys', 'survey_applicants.survey_id', '=', 'surveys.id')
            ->where('user_id', '=',  $profile->user_id)
            ->where('surveys.until', '>=',  now())
            ->pluck('name','survey_id');
        return view('surveyor.locus.form', compact ('model', 'instansi', 'profile', 'survey'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'survey_id' => 'required',
            'locus_id' => 'required|unique:survey_locuses,locus_id,NULL,id,deleted_at,NULL,survey_id,' . $request->survey_id,
        ]);

        $input['user_id'] = Auth::id();
        $input['survey_id'] = $request->survey_id;
        $input['locus_id'] = $request->locus_id;
        SurveyLocus::create($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey_locus_id = SurveyLocus::find($id);
        $survey_name = Survey::select('name')->find($survey_locus_id->survey_id);
        $instansi_name = '-';
        $locus = Locus::select('id', 'name')->find($survey_locus_id->locus_id);
        $dokumentasis = SurveyLocusDokumentasi::query()->where('survey_locus_id', '=', $survey_locus_id->id)->get();
        return view('surveyor.locus.show', compact('locus', 'survey_name', 'instansi_name', 'dokumentasis', 'survey_locus_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey_locus = SurveyLocus::find($id);
        $pertanyaan_locus = SurveyLocusPenilaian::select('pertanyaan_id')->distinct('pertanyaan_id')->where('survey_locus_id', '=', $survey_locus->id)->get();
        $kategori_pertanyaan = Pertanyaan::with('kategoriPertanyaan')
            ->distinct('kategori_pertanyaan_id')
            ->where('survey_id', '=', $survey_locus->survey_id)
            ->whereNotIn('id', $pertanyaan_locus)
            ->get('kategori_pertanyaan_id');
        $pertanyaan = Pertanyaan::with('kategoriPertanyaan', 'penilaian')
            ->where('survey_id', '=', $survey_locus->survey_id)
            ->whereNotIn('id', $pertanyaan_locus)->get();
        $qty_pertanyaans = Pertanyaan::query()
            ->where('survey_id', '=', $survey_locus->survey_id)
            ->whereNotIn('id', $pertanyaan_locus)->count();
        return view('surveyor.locus.proses', compact( 'survey_locus', 'kategori_pertanyaan', 'pertanyaan', 'qty_pertanyaans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'penilaian_id' => 'required'
        ]);
        $check = $request->jenis_pertanyaan_id;
        $input = $request->all();
        if($check == '1'):
            SurveyLocusPenilaian::create([
                'survey_locus_id' => $input['survey_locus_id'],
                'pertanyaan_id' => $input['pertanyaan_id'],
                'penilaian_id'=> $input['penilaian_id']
            ]);
        elseif ($check == '2'):
            foreach ($request->get('penilaian_id') as $penilaianId) {
                SurveyLocusPenilaian::create([
                    'survey_locus_id' => $input['survey_locus_id'],
                    'pertanyaan_id' => $input['pertanyaan_id'],
                    'penilaian_id'=> $penilaianId
                ]);
            }
        else:
            SurveyLocusPenilaian::create([
                'survey_locus_id' => $input['survey_locus_id'],
                'pertanyaan_id' => $input['pertanyaan_id'],
                'penilaian_id'=> $input['penilaian_id']
            ]);
        endif;
        return redirect()->route('surveyor.lokasi.edit', $id);
    }

    /** begin aplut putu survey */
    public function aplut(Request $request, $id)
    {
        $this->validate($request, [
            'path' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:100240',
            'latitude' => 'required',
            'longitude' => 'required'
        ]);
        $input = $request->all();
        if ($request->hasFile('path')){
            $input['path'] = 'storage/images/surveyor/survey-locus/'.$input['survey_locus_id'].'-'.uniqid().'.'.$request->path->getClientOriginalExtension();
            $request->path->move(public_path('storage/images/surveyor/survey-locus/'), $input['path']);
        }
        SurveyLocusDokumentasi::create([
            'survey_locus_id' => $input['survey_locus_id'],
            'path' => $input['path'],
            'latitude' => $input['latitude'],
            'longitude' => $input['longitude']
        ]);
        return redirect()->route('surveyor.lokasi.edit', $id);
    }
    /** end aplut putu survey */

    /** begin finish survey */
    public function finish(Request $request, $id)
    {
        $input = $request->all();
        $model = SurveyLocus::find($id);
        $model->update($input);
        return redirect()->route('surveyor.lokasi.index');
    }
    /** end aplut putu survey */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = SurveyLocus::with('surveyLocusDokumentasi', 'surveyLocusPenilaian')->findOrFail($id);
        $model->surveyLocusDokumentasi->each->delete();
        $model->surveyLocusPenilaian->each->delete();
        $model->delete();
    }

    public function dataTable()
    {
        $model = SurveyLocus::query()
            ->select('survey_locuses.id as id', 'survey_locuses.status_survey_locus','surveys.name as survey_name', 'surveys.until','locuses.name as locus_name', 'instansis.name as instansi_name')
            ->leftJoin('surveys', 'survey_locuses.survey_id', '=', 'surveys.id')
            ->leftJoin('locuses', 'survey_locuses.locus_id', '=', 'locuses.id')
            ->leftJoin('instansis', 'locuses.instansi_code', '=', 'instansis.code')
            ->where('survey_locuses.user_id', '=', Auth::user()->id)
            ->where('surveys.is_publish', '=', true)
            ->where('surveys.until', '>=', now());
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('surveyor.locus._action', [
                    'model' => $model,
                    'url_show' => route('surveyor.lokasi.show', $model->id),
                    'url_edit' => route('surveyor.lokasi.edit', $model->id),
                    'url_destroy' => route('surveyor.lokasi.destroy', $model->id)
                ]);
            })
            ->editColumn('until', function ($model) {
                return $model->until ? with(new Carbon($model->until))->format('j F Y') : '';
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
