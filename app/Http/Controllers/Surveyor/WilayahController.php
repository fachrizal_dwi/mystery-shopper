<?php

namespace App\Http\Controllers\Surveyor;

use App\Http\Controllers\Controller;
use App\Kecamatan;
use App\Kelurahan;
use App\Locus;
use Illuminate\Http\Request;
use App\KotaKabupaten;

class WilayahController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function getKota(Request $request)
    {
        $kota = KotaKabupaten::where('province_id', $request->get('id'))
            ->pluck('name', 'id');
        return response()->json($kota);
    }

    public function getKecamatan(Request $request)
    {
        $kecamatan = Kecamatan::where('kota_kabupaten_id', $request->get('id'))
            ->pluck('name', 'id');
        return response()->json($kecamatan);
    }

    public function getKelurahan(Request $request)
    {
        $kelurahan = Kelurahan::where('kecamatan_id', $request->get('id'))
            ->pluck('name', 'id');
        return response()->json($kelurahan);
    }

    public function getLocus(Request $request)
    {
        $locus = Locus::where('instansi_code', $request->get('id'))
            ->pluck('name', 'id');
        return response()->json($locus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
