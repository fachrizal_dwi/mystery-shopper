<?php

namespace App\Http\Controllers\Surveyor;

use App\Http\Controllers\Controller;
use App\Kecamatan;
use App\Kelurahan;
use App\KotaKabupaten;
use App\Provinsi;
use App\Universitas;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;
use Gate;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = UserProfile::with('universitas', 'provinsi', 'kotaKabupaten', 'kecamatan', 'kelurahan')->where('user_id', Auth::user()->id)->first();
        return view('surveyor.profile.index', compact('model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new UserProfile();
        $universitas = Universitas::pluck('name', 'id');
        $provinsi = Provinsi::pluck('name', 'id');
        $kota = KotaKabupaten::pluck('name', 'id')->where('id',1);
        $kecamatan = Kecamatan::pluck('name', 'id')->where('id',1);
        $kelurahan = Kelurahan::pluck('name', 'id')->where('id',1);
        return view('surveyor.profile.form', compact ('model', 'universitas', 'provinsi', 'kota', 'kecamatan', 'kelurahan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            //'universitas_id' => 'required|max:255',
            'nama_lengkap' => 'required|string',
            'nik' => 'required|numeric|digits:16',
            //'nim' => 'required',
            'phone' => 'required|numeric',
            'emergency_call' => 'required|numeric',
            'tanggal_lahir' => 'required|date',
            'tempat_lahir' => 'required|string',
            'provinsi_id' => 'required',
            'kota_kabupaten_id' => 'required',
            'kecamatan_id' => 'required',
            'kelurahan_id' => 'required',
            'alamat' => 'required',
            'photo_profile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'photo_ktp' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'photo_selfie' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'photo_legal' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'longitude' => 'required',
            'latitude' => 'required',
            'is_agree' => 'required'
        ]);

        /** check Universitas */
        $input = $request->all();
        $input = array_merge($input,['user_id' => Auth::user()->id]);
        $universitas = null;
        if(is_numeric($request['universitas_id'])):
            $universitas  = Universitas::find($request['universitas_id']);
        endif;
        if(is_null($universitas)):
            $input['universitas_id'] = Universitas::insertGetId([
                'name' => $input['universitas_id']
            ]);
        endif;
        /** end check Universitas */

        /** aplut photo_ktp */
        $input['photo_ktp']= null;
        if ($request->hasFile('photo_ktp')){
            $input['photo_ktp'] = 'storage/images/surveyor/ktp/'.uniqid().'.'.$request->photo_ktp->getClientOriginalExtension();
            $request->photo_ktp->move(public_path('storage/images/surveyor/ktp/'), $input['photo_ktp']);
        }
        /** end photo_ktp */

        /** aplut photo_selfie */
        $input['photo_selfie']= null;
        if ($request->hasFile('photo_selfie')){
            $input['photo_selfie'] = 'storage/images/surveyor/selfie/'.uniqid().'.'.$request->photo_selfie->getClientOriginalExtension();
            $request->photo_selfie->move(public_path('storage/images/surveyor/selfie/'), $input['photo_selfie']);
        }
        /** end photo_selfie */

        /** aplut photo_legal */
        $input['photo_legal']= null;
        if ($request->hasFile('photo_legal')){
            $input['photo_legal'] = 'storage/images/surveyor/legal/'.uniqid().'.'.$request->photo_legal->getClientOriginalExtension();
            $request->photo_legal->move(public_path('storage/images/surveyor/legal/'), $input['photo_legal']);
        }
        /** end photo_legal */

        /** aplut photo_profile */
        $input['photo_profile']= null;
        if ($request->hasFile('photo_profile')){
            $input['photo_profile'] = 'storage/images/surveyor/profile/'.uniqid().'.'.$request->photo_profile->getClientOriginalExtension();
            $request->photo_profile->move(public_path('storage/images/surveyor/profile/'), $input['photo_profile']);
        }
        /** end photo_profile */

        UserProfile::create($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
