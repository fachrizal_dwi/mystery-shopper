<?php


namespace App\Http\Controllers\Api;


use App\SurveyLocusPenilaian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

class SurveyLocusPenilaianController extends LoginController
{
    public function store(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'survey_locus_id' => ['required', 'string'],
            'penilaian_id' => ['required', 'string'],
            'pertanyaan_id' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                'survey_locus_id'=> $request['survey_locus_id'],
                'penilaian_id'=> $request['penilaian_id'],
                'pertanyaan_id'=> $request['pertanyaan_id'],
            ];
            $create = SurveyLocusPenilaian::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Survey Locus Penilaian',
                    'id' => $create->id
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Survey Locus Penilaian',
                ],Response::HTTP_BAD_REQUEST);
            }

        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'survey_locus_id' => ['required', 'string'],
            'pertanyaan_id' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                ['survey_locus_id', '=', $request['survey_locus_id']],
                ['pertanyaan_id', '=', $request['pertanyaan_id']]
            ];
            SurveyLocusPenilaian::where($data)->delete();
            return response()->json([
                'status' => true,
                'message' => 'Berhasil hapus Survey Locus Penilaian'
            ],Response::HTTP_CREATED);

        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_BAD_REQUEST);
        }
    }

    public function getData(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'survey_locus_id' => ['required', 'string'],
            'pertanyaan_id' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                ['survey_locus_id', '=', $request['survey_locus_id']],
                ['pertanyaan_id', '=', $request['pertanyaan_id']]
            ];
            $data = SurveyLocusPenilaian::where($data)->get();
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Survey Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_BAD_REQUEST);
        }    
    }
}
