<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\KotaKabupaten;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MasterKotaDanKabupatenController extends Controller

{
    public function all(Request $request){
        $data = KotaKabupaten::all();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Kota Kabupaten Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getDataAll(Request $request){
        $validator = Validator::make($request->all(), [
            'id_provinsi' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = KotaKabupaten::where('deleted_at')->where('province_id',$request['id_provinsi'])->get();
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Kota Kabupaten Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kotaKabupaten' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = KotaKabupaten::where('deleted_at')->firstWhere('id',$request['id_kotaKabupaten']);;
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Kota Kabupaten Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function saveKotaKabupaten(Request $request){
        $validator = Validator::make($request->all(), [
            'id_provinsi' => ['required', 'string'],
            'namaKotaKabupaten' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                'province_id'=>$request['id_provinsi'],
                'name'=> $request['namaKotaKabupaten'],
            ];
            $create = KotaKabupaten::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Kota Kabupaten'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'messages' => 'Gagal create Kota Kabupaten',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateKotaKabupaten(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kotaKabupaten' => ['required', 'string'],
            'id_provinsi' => ['required', 'string'],
            'namaKotaKabupaten' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = [
                'province_id'=>$request['id_provinsi'],
                'name'=> $request['namaProvinsi'],
            ];
            $update = KotaKabupaten::where('id',$request['id_kotaKabupaten'])->update($data);
            if($update){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update Kota Kabupaten'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal update Kota Kabupaten',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
    public function deleteKotaKabupaten(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kotaKabupaten' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $delete = KotaKabupaten::find($request['id_kotaKabupaten'])->delete();
            if($delete){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil delete Kota Kabupaten'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal delete Kota Kabupaten',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
