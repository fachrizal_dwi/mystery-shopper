<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Locus;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MasterLocusController extends Controller
{
    public function all(Request $request){
        $data = Locus::all();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Locus Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getDataAll(Request $request){
        $instansi_code= $request['instansi_code'];
        if ($instansi_code!=null ||$instansi_code!="" ){
            $data = Locus::where('instansi_code',$instansi_code)->where('deleted_at')->get();
        }else{
            $data = Locus::where('deleted_at')->get();
        }
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Locus Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_locus' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Locus::where('deleted_at')->firstWhere('id',$request['id_locus']);
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Locus Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }
    public function saveLocus(Request $request){
        $validator = Validator::make($request->all(), [
            'kategoriLocusId' => ['required', 'string'],
            'name' => ['required', 'string'],
            'provinsiId' => ['required', 'string'],
            'kotaKabupatenId' => ['required', 'string'],
            'kecamatanId' => ['required', 'string'],
            'kelurahanId' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                'kategori_locus_id'=> $request['kategoriLocusId'],
                'name'=> $request['name'],
                'provinsi_id'=> $request['provinsiId'],
                'kota_kabupaten_id'=> $request['kotaKabupatenId'],
                'kecamatan_id'=> $request['kecamatanId'],
                'kelurahan_id'=> $request['kelurahanId'],
            ];
            $create = Locus::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Locus'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Locus',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateLocus(Request $request){
        $validator = Validator::make($request->all(), [
            'id_locus' => ['required', 'string'],
            'kategoriLocusId' => ['required', 'string'],
            'name' => ['required', 'string'],
            'provinsiId' => ['required', 'string'],
            'kotaKabupatenId' => ['required', 'string'],
            'kecamatanId' => ['required', 'string'],
            'kelurahanId' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = [
                'kategori_locus_id'=> $request['kategoriLocusId'],
                'name'=> $request['name'],
                'provinsi_id'=> $request['provinsiId'],
                'kota_kabupaten_id'=> $request['kotaKabupatenId'],
                'kecamatan_id'=> $request['kecamatanId'],
                'kelurahan_id'=> $request['kelurahanId'],
            ];
            $update = Locus::where('id',$request['id_locus'])->update($data);
            if($update){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update Locus'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal update Locus',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
    public function deleteLocus(Request $request){
        $validator = Validator::make($request->all(), [
            'id_locus' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $delete = Locus::find($request['id_locus'])->delete();
            if($delete){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil delete Locus'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal delete Locus',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
