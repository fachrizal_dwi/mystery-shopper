<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Universitas;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\UserProfile;
use Exception;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
     private $jwt;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $key = "U2FsdGVkX18qJV4mJCFAI1Mx5rGuokT8NPlIHZK087";
    public function __construct()
    {

    }
    public function registrasiUser(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'unique:users'],
            'password' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data1 = [
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ];
            //generet token
            $jwt = JWT::encode($data1, $this->key);
            $data = [
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'remember_token' => $jwt,
                'uid'=>uniqid(),
            ];

            $createUser = User::create($data);
            if($createUser){
                return response()->json([
                    'access_token' => $jwt,
                    'token_type' => 'Bearer',
                    'data' => $data,
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create  user',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);

        }

    }
    public function registrasi(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'unique:users'],
            'password' => ['required', 'string'],
            'nik' => ['required', 'string', 'unique:user_profiles'],
            //'nim' => ['required', 'string', 'unique:user_profiles'],
            'phone' => ['required', 'string'],
            'photo_ktp' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
            'photo_profile' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
            'photo_selfie' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
            'photo_legal' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
        ]);

        if(!$validator->fails()){
            $univ= $request['universitas_id'];
            if ( $univ == null ||  $univ == ""){
                if($request['universitas_nama']!= null ||  $request['universitas_nama'] != ""){
                    $data= [
                        'name' => $request['universitas_nama'],
                        'provinsi_id' => $request['provinsi_id'],
                    ];
                    $create = Universitas::create($data);
                    $datauniv=Universitas::where('deleted_at')->firstWhere('name',$request['universitas_nama']);
                    $univ= $datauniv->id;
                }
            }
            $data1 = [
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ];
            //generet token
            $jwt = JWT::encode($data1, $this->key);
            $data = [
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'remember_token' => $jwt,
                'uid'=>uniqid(),
            ];

            $createUser = User::create($data);

            if ($createUser){
                $idUser=User::where('deleted_at')->firstWhere('email',$request['email']);
                $dataProfile = [
                    'user_id'=> $idUser->id,
                    'universitas_id'=>  $univ == 0 ? null : $univ,
                    'nama_lengkap'=>$request['name'],
                    'nik'=>$request['nik'],
                    'nim'=>$request['nim'],
                    'phone'=>$request['phone'],
                    'emergency_call'=>$request['emergency_call'],
                    'tanggal_lahir'=>$request['tanggal_lahir'],
                    'tempat_lahir'=>$request['tempat_lahir'],
                    'provinsi_id'=>$request['provinsi_id'],
                    'kota_kabupaten_id'=>$request['kota_kabupaten_id'],
                    'kecamatan_id'=>$request['kecamatan_id'],
                    'kelurahan_id'=>$request['kelurahan_id'],
                    'alamat'=>$request['alamat'],
                    'latitude'=>$request['latitude'],
                    'longitude'=>$request['longitude'],
                    'photo_ktp'=> 'storage/images/surveyor/ktp/'.uniqid().".".$request->file('photo_ktp')->getClientOriginalExtension(),
                    'photo_profile'=> 'storage/images/surveyor/profile/'.uniqid().".".$request->file('photo_profile')->getClientOriginalExtension(),
                    'photo_selfie'=> 'storage/images/surveyor/selfie/'.uniqid().".".$request->file('photo_selfie')->getClientOriginalExtension(),
                    'photo_legal'=> 'storage/images/surveyor/legal/'.uniqid().".".$request->file('photo_legal')->getClientOriginalExtension(),

                ];
                $createUserProfile = UserProfile::create($dataProfile);
                if($createUserProfile){
                    //upload photo ktp
                    $file = $request->file('photo_ktp');
                    $file->move(public_path('storage/images/surveyor/ktp/'), uniqid().".".$file->getClientOriginalExtension());
                    //upload photo profile
                    $file1 = $request->file('photo_profile');
                    $file1->move(public_path('storage/images/surveyor/profile/'), uniqid().".".$file1->getClientOriginalExtension());
                    //upload photo selfie
                    $file2 = $request->file('photo_selfie');
                    $file2->move(public_path('storage/images/surveyor/selfie/'), uniqid().".".$file2->getClientOriginalExtension());
                    //upload photo legal
                    $file3 = $request->file('photo_legal');
                    $file3->move(public_path('storage/images/surveyor/legal/'), uniqid().".".$file3->getClientOriginalExtension());
                    return response()->json([
                        'access_token' => $jwt,
                        'token_type' => 'Bearer',
                        'data' => $data,
                    ],Response::HTTP_CREATED);
                }else{
                    return response()->json([
                        'message' => 'Gagal create profile user',
                    ],Response::HTTP_NOT_FOUND);
                }
            }else{
                return response()->json([
                    'message' => 'Gagal create user',
                ],Response::HTTP_NOT_FOUND);
            }

        }else{
            return response()->json([
                'message' => implode(", ", $validator->messages()->all()),
            ],Response::HTTP_NOT_FOUND);

        }
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $user=User::where('deleted_at')->firstWhere('email',$request['email']);
            // dd($user);
            if ($user!=""||$user!=null ) {
                if (Hash::check($request['password'], $user->password)) {
                    if ($user->remember_token == null)
                        {
                            $data1 = [
                                'email' => $request['email'],
                                'password' => Hash::make($request['password']),
                            ];
                            $jwt = JWT::encode($data1, $this->key);
                            $user->remember_token = $jwt;
                            $user->save();
                        }
                        return response()->json([
                            'status' => true,
                            'token' => $user->remember_token,
                            'data' =>$user,
                        ], Response::HTTP_OK);

                }else{
                    return response()->json([
                        'status' => false,
                        'message' => 'Password atau username salah'
                    ], Response::HTTP_BAD_REQUEST);
                }
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Password atau username salah'
                ], Response::HTTP_BAD_REQUEST);
            }

        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

     // method untuk mengecek token setiap melakukan post, put, etc
     public function cektoken(Request $request)
     {
        $header = $request->header('Authorization', '');

        if (Str::startsWith($header, 'Bearer ')) {
            $token = Str::substr($header, 7);
            // $decoded = JWT::decode($token, $this->key, array('HS256'));

            try {
                $cektoken=User::where('deleted_at')->firstWhere('remember_token',$token);

                if ($cektoken!=""||$cektoken!=null||$cektoken->id !=0 ) {
                    return true;
                }
            } catch (Exception $e) {
                return false;
            }

        }else{
            return false;

        }

     }

     public function changePassword(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],

        ]);
        if(!$validator->fails()){
            $data1 = [
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ];
            //generet token
            $jwt = JWT::encode($data1, $this->key);
            $data = [
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'remember_token' => $jwt,
            ];
            $user = User::where('id',$request['id_user'])->update($data);
            if($user){
                return response()->json([
                    'status' => true,
                    'token' => $jwt,
                    'message' => 'Berhasil update password'
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'message' => 'Gagal update password',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

     }

}
