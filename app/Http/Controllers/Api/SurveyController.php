<?php

namespace App\Http\Controllers\Api;

use App\Survey;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SurveyController extends LoginController
{
    public function index(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $data = Survey::with(['kategoriSurvey', 'surveyStatus', 'surveyLocus', 'surveyLocus.locus', 'surveyApplicants'])->where('deleted_at')->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Data Survey Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function available(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $id_user = $request->id_user;
        $data = Survey::with(['kategoriSurvey', 'surveyStatus'])
            ->whereDoesntHave('surveyApplicants', function ($query) use($id_user) {
                $query->where('user_id', '=', $id_user);
            })
            ->where('survey_status_id', '=', 1)
            ->whereDate('until', '>=', Carbon::now())
            ->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Data Survey Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getDataById(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_survey' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Survey::with(['surveyLocus','surveyApplicants'])->where('deleted_at')->firstWhere('id',$request['id_survey']);
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Survey Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
