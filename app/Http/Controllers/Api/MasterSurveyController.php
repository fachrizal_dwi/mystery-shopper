<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\KategoriSurvey;
use App\Survey;
use App\SurveyStatus;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MasterSurveyController extends Controller
{
    public function getDataAll(Request $request){
        $data = Survey::with(['kategoriSurvey','surveyStatus'])->where('deleted_at')->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Survey Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_survey' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Survey::with(['kategoriSurvey','surveyStatus'])->where('deleted_at')->firstWhere('id',$request['id_survey']);
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Survey Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }
    public function getDataKategoriSurvey(Request $request){
        $data = KategoriSurvey::where('deleted_at')->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Kategori Suevey Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }

    }
    public function getDataSurveyStatus(Request $request){
        $data = SurveyStatus::where('deleted_at')->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Survey Status Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }

    }
    public function saveSurvey(Request $request){
        $validator = Validator::make($request->all(), [
            'surveyStatusId' => ['required', 'string'],
            'name' => ['required', 'string'],
            'bobot_minimal'=>['required'],
            'from' => ['required', 'string'],
            'until' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $publis=$request['is_publish'];
            if ($publis==null||$publis==""){
                $publis=false;
            }
            $data= [
                'kategori_survey_id'=>$request['kategoriSurveyId'],
                'survey_status_id'=> $request['surveyStatusId'],
                'name'=> $request['name'],
                'bobot_minimal'=> $request['bobot_minimal'],
                'from'=> $request['from'],
                'until'=> $request['until'],
                'is_publish'=> $publis,
            ];
            $create = Survey::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Survey'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Survey',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateSurvey(Request $request){
        $validator = Validator::make($request->all(), [
            'id_survey' => ['required', 'string'],
            'surveyStatusId' => ['required', 'string'],
            'name' => ['required', 'string'],
            'bobot_minimal'=>['required'],
            'from' => ['required', 'string'],
            'until' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $publis=$request['is_publish'];
            if ($publis==null||$publis==""){
                $publis=false;
            }
            $data = [
                'kategori_survey_id'=>$request['kategoriSurveyId'],
                'survey_status_id'=> $request['surveyStatusId'],
                'name'=> $request['name'],
                'bobot_minimal'=> $request['bobot_minimal'],
                'from'=> $request['from'],
                'until'=> $request['until'],
                'is_publish'=> $publis,
            ];
            $update = Survey::where('id',$request['id_survey'])->update($data);
            if($update){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update Survey'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal update Survey',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
    public function deleteSurvey(Request $request){
        $validator = Validator::make($request->all(), [
            'id_survey' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $delete = Survey::find($request['id_survey'])->delete();
            if($delete){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil delete Survey'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal delete Survey',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
