<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Survey;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class KonfigurasiSurveySurveyController extends Controller
{
    public function getDataAll(Request $request){
        $data = Survey::with(['kategoriSurvey','surveyStatus','surveyLocus','surveyLocus.locus','surveyLocus.user','surveyApplicants','surveyApplicants.user','surveyApplicants.surveyApplicantStatus','pertanyaan','pertanyaan.kategoriPertanyaan','pertanyaan.jenisPertanyaan'])->where('deleted_at')->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Survey Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_survey' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Survey::with(['kategoriSurvey','surveyStatus','surveyLocus','surveyLocus.locus','surveyLocus.user','surveyApplicants','surveyApplicants.user','surveyApplicants.surveyApplicantStatus','pertanyaan','pertanyaan.kategoriPertanyaan','pertanyaan.jenisPertanyaan'])->where('deleted_at')->firstWhere('id',$request['id_survey']);
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Survey Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }
}
