<?php

namespace App\Http\Controllers\Api;

use App\Universitas;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\UserProfile;

class ProfileController extends LoginController
{
    public function getDataProfileByUserId(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $user=UserProfile::with('user', 'universitas', 'provinsi', 'kotaKabupaten', 'kecamatan', 'kelurahan')
                ->where('deleted_at')->firstWhere('user_id',$request['id_user']);
            if ($user!=""||$user!=null ) {
                return response()->json([
                    'status' => true,
                    'data' =>$user,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'id user tidak di temukan',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }
    public function getDataUserByUserId(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $user=User::with('userType')
                ->where('deleted_at')->firstWhere('id',$request['id_user']);
            if ($user!=""||$user!=null ) {
                return response()->json([
                    'status' => true,
                    'data' =>$user,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'id user tidak di temukan',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function insert(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }

        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
            'name' => ['required', 'string'],
            // 'universitas_id' => ['required', 'string'],
            'nik' => ['required', 'string'],
            'nim' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'photo_ktp' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
            'photo_profile' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
            'photo_selfie' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
            'photo_legal' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
        ]);

        if(!$validator->fails()){
            $univ= $request['universitas_id'];
            if ( $univ == null ||  $univ == ""){
                if($request['universitas_nama']!= null ||  $request['universitas_nama'] != ""){
                    $data= [
                        'name' => $request['universitas_nama'],
                    ];
                    $create = Universitas::create($data);
                    $datauniv=Universitas::where('deleted_at')->firstWhere('name',$request['universitas_nama']);
                    $univ= $datauniv->id;
                }else{
                    return response()->json([
                        'message' => 'The university field is required',
                    ],Response::HTTP_NOT_FOUND);
                }
            }
            $dataProfile = [
                'user_id'=> $request['id_user'],
                'universitas_id'=>  $univ,
                'nama_lengkap'=>$request['name'],
                'nik'=>$request['nik'],
                'nim'=>$request['nim'],
                'phone'=>$request['phone'],
                'emergency_call'=>$request['emergency_call'],
                'tanggal_lahir'=>$request['tanggal_lahir'],
                'tempat_lahir'=>$request['tempat_lahir'],
                'provinsi_id'=>$request['provinsi_id'],
                'kota_kabupaten_id'=>$request['kota_kabupaten_id'],
                'kecamatan_id'=>$request['kecamatan_id'],
                'kelurahan_id'=>$request['kelurahan_id'],
                'alamat'=>$request['alamat'],
                'photo_ktp'=> 'storage/images/surveyor/ktp/'.uniqid().".".$request->file('photo_ktp')->getClientOriginalExtension(),
                'photo_profile'=> 'storage/images/surveyor/profile/'.uniqid().".".$request->file('photo_profile')->getClientOriginalExtension(),
                'photo_selfie'=> 'storage/images/surveyor/selfie/'.uniqid().".".$request->file('photo_selfie')->getClientOriginalExtension(),
                'photo_legal'=> 'storage/images/surveyor/legal/'.uniqid().".".$request->file('photo_legal')->getClientOriginalExtension(),
            ];
            $insert = UserProfile::create($dataProfile);
            if($insert){
                 //upload photo ktp
                 $file = $request->file('photo_ktp');
                 $file->move(public_path('storage/images/surveyor/ktp/'), uniqid().".".$file->getClientOriginalExtension());
                 //upload photo profile
                 $file1 = $request->file('photo_profile');
                 $file1->move(public_path('storage/images/surveyor/profile/'), uniqid().".".$file1->getClientOriginalExtension());
                 //upload photo selfie
                 $file2 = $request->file('photo_selfie');
                 $file2->move(public_path('storage/images/surveyor/selfie/'), uniqid().".".$file2->getClientOriginalExtension());
                 //upload photo legal
                 $file3 = $request->file('photo_legal');
                 $file3->move(public_path('storage/images/surveyor/legal/'), uniqid().".".$file3->getClientOriginalExtension());
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil insert profile'
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'message' => 'Gagal insert profile',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateProfile(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
            'name' => ['required', 'string'],
            // 'universitas_id' => ['required', 'string'],
            'nik' => ['required', 'string'],
            'nim' => ['required', 'string'],
            'phone' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $univ= $request['universitas_id'];
            if ( $univ == null ||  $univ == ""){
                if($request['universitas_nama']!= null ||  $request['universitas_nama'] != ""){
                    $data= [
                        'name' => $request['universitas_nama'],
                    ];
                    $create = Universitas::create($data);
                    $datauniv=Universitas::where('deleted_at')->firstWhere('name',$request['universitas_nama']);
                    $univ= $datauniv->id;
                }else{
                    return response()->json([
                        'message' => 'The university field is required',
                    ],Response::HTTP_NOT_FOUND);
                }
            }
            $dataProfile = [
                // 'user_id'=> $request['id_user'],
                'universitas_id'=> $univ,
                'nama_lengkap'=>$request['name'],
                'nik'=>$request['nik'],
                'nim'=>$request['nim'],
                'phone'=>$request['phone'],
                'emergency_call'=>$request['emergency_call'],
                'tanggal_lahir'=>$request['tanggal_lahir'],
                'tempat_lahir'=>$request['tempat_lahir'],
                'provinsi_id'=>$request['provinsi_id'],
                'kota_kabupaten_id'=>$request['kota_kabupaten_id'],
                'kecamatan_id'=>$request['kecamatan_id'],
                'kelurahan_id'=>$request['kelurahan_id'],
                'alamat'=>$request['alamat'],
            ];
            $updateProfile = UserProfile::where('user_id',$request['id_user'])->update($dataProfile);
            if($updateProfile){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update profile'
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'message' => 'Gagal update profile',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updatePhotoProfile(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
            'photo_profile' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
        ]);
        if(!$validator->fails()){
            $dataProfile = [
                'photo_profile'=> uniqid()."-".$request->file('photo_profile')->getClientOriginalName(),
            ];
            $updateProfile = UserProfile::where('user_id',$request['id_user'])->update($dataProfile);
            if($updateProfile){
                $file1 = $request->file('photo_profile');
                $file1->move(storage_path('app\public\images\surveyor\profile'),  uniqid()."-".$file1->getClientOriginalName());
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update profile'
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'message' => 'Gagal update profile',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updatePhotoSelfie(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
            'photo_selfie' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
        ]);
        if(!$validator->fails()){
            $dataProfile = [
                'photo_selfie'=> uniqid()."-".$request->file('photo_selfie')->getClientOriginalName(),
            ];
            $updateProfile = UserProfile::where('user_id',$request['id_user'])->update($dataProfile);
            if($updateProfile){
                $file2 = $request->file('photo_selfie');
                $file2->move(storage_path('app\public\images\surveyor\selfie'),  uniqid()."-".$file2->getClientOriginalName());
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update profile'
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'message' => 'Gagal update profile',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updatePhotoLegal(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
            'photo_legal' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
        ]);
        if(!$validator->fails()){
            $dataProfile = [
                'photo_legal'=> uniqid()."-".$request->file('photo_legal')->getClientOriginalName(),
            ];
            $updateProfile = UserProfile::where('user_id',$request['id_user'])->update($dataProfile);
            if($updateProfile){
                $file3 = $request->file('photo_legal');
                $file3->move(storage_path('app\public\images\surveyor\legal'),  uniqid()."-".$file3->getClientOriginalName());
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update profile'
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'message' => 'Gagal update profile',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

}
