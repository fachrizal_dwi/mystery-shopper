<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\SurveyApplicant;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ApplySurveyController extends LoginController
{
    public function getDataAll(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $data = SurveyApplicant::with(['survey','surveyApplicantStatus'])->where('deleted_at')->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Survey Applicant Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_surveyApplicant' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = SurveyApplicant::with(['survey','surveyApplicantStatus'])->where('deleted_at')->firstWhere('id',$request['id_surveyApplicant']);
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Locus Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function hasilSurvey(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $status = false;
            $tanggalAwal= null;
            $tanggalAkhir= null;
            $data = null;
            if ($request['tanggal_awal'] != null || $request['tanggal_awal'] != ""){
                if ($request['tanggal_akhir'] != null || $request['tanggal_akhir'] != ""){
                    $status= true;
                    $tanggalAwal= $request['tanggal_awal'];
                    $tanggalAkhir= $request['tanggal_akhir'];
                }else{
                    $status= true;
                    $tanggalAwal= $request['tanggal_awal'];
                    $tanggalAkhir= $request['tanggal_awal'];
                }
            }else if ($request['tanggal_akhir'] != null || $request['tanggal_akhir'] != ""){
                if ($request['tanggal_awal'] != null || $request['tanggal_awal'] != ""){
                    $status= true;
                    $tanggalAwal= $request['tanggal_awal'];
                    $tanggalAkhir= $request['tanggal_akhir'];
                }else{
                    $status= true;
                    $tanggalAwal= $request['tanggal_akhir'];
                    $tanggalAkhir= $request['tanggal_akhir'];
                }
            }
            // DB::enableQueryLog();
            if($status){
                $data = SurveyApplicant::with('survey','surveyApplicantStatus')->whereBetween('created_at',[$tanggalAwal,$tanggalAkhir ])->where('deleted_at')->where('user_id',$request['id_user'])->orderBy('created_at', 'desc')->get();
            }else{
                $data = SurveyApplicant::with('survey','surveyApplicantStatus')->where('deleted_at')->where('user_id',$request['id_user'])->orderBy('created_at', 'desc')->get();
            }
            // dd(DB::getQueryLog());
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Hasil Survey Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function saveSurveyApplicant(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'user_id' => ['required', 'string'],
            'survey_id' => ['required', 'string'],
            'surveyApplicantStatus_id' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                'user_id'=> $request['user_id'],
                'survey_id'=> $request['survey_id'],
                'survey_applicant_status_id'=> $request['surveyApplicantStatus_id'],
            ];
            $create = SurveyApplicant::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Survey Applicant',
                    'id' => $create->id
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Survey Applicant',
                ],Response::HTTP_NOT_FOUND);
            }

        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateSurveyApplicant(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_SurveyApplicant'=> ['required', 'string'],
            'surveyApplicantStatus_id' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = [
                'survey_applicant_status_id'=> $request['surveyApplicantStatus_id'],
            ];
            $update = SurveyApplicant::where('id',$request['id_SurveyApplicant'])->update($data);
            if($update){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update Survey Applicant'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'messages' => 'Gagal update Survey Applicant',
                ],Response::HTTP_NOT_FOUND);
            }

        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
    public function deleteSurveyApplicant(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_SurveyApplicant'=> ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $delete = SurveyApplicant::find($request['id_SurveyApplicant'])->delete();
            if($delete){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil delete Survey Applicant'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal delete Survey Applicant',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
