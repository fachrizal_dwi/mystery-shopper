<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Kecamatan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MasterKecamatanController extends Controller
{
    public function all(Request $request){
        $data = Kecamatan::all();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Kecamatan Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }


    public function getDataAll(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kotaKabupaten' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Kecamatan::where('deleted_at')->where('kota_kabupaten_id',$request['id_kotaKabupaten'])->get();
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Kecamatan Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kecamatan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Kecamatan::where('deleted_at')->firstWhere('id',$request['id_kecamatan']);;
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Kecamatan Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function saveKecamatan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kotaKabupaten' => ['required', 'string'],
            'namaKecamatan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                'kota_kabupaten_id'=>$request['id_kotaKabupaten'],
                'name'=> $request['namaKecamatan'],
            ];
            $create = Kecamatan::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Kecamatan'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Kecamatan',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateKecamatan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kecamatan' => ['required', 'string'],
            'id_kotaKabupaten' => ['required', 'string'],
            'namaKecamatan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = [
                'kota_kabupaten_id'=>$request['id_kotaKabupaten'],
                'name'=> $request['namaKecamatan'],
            ];
            $update = Kecamatan::where('id',$request['id_kecamatan'])->update($data);
            if($update){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update Kecamatan'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal update Kecamatan',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
    public function deleteKecamatan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kecamatan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $delete = Kecamatan::find($request['id_kecamatan'])->delete();
            if($delete){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil delete Kecamatan'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal delete Kecamatan',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
