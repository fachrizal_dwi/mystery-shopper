<?php


namespace App\Http\Controllers\Api;


use App\SurveyLocus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class SurveyLocusController extends LoginController
{
    public function store(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'user_id' => ['required', 'string'],
            'survey_id' => ['required', 'string'],
            'locus_id' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                'user_id'=> $request['user_id'],
                'survey_id'=> $request['survey_id'],
                'locus_id'=> $request['locus_id'],
            ];
            $create = SurveyLocus::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Survey Locus',
                    'id' => $create->id
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Survey Locus',
                ],Response::HTTP_BAD_REQUEST);
            }

        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete(Request $request, $id)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $create = SurveyLocus::destroy($id);
        if($create){
            return response()->json([
                'status' => true,
                'message' => 'Berhasil menghapus Survey Locus'
            ],Response::HTTP_CREATED);
        }else{
            return response()->json([
                'message' => 'Gagal create Survey Locus',
            ],Response::HTTP_BAD_REQUEST);
        }
    }

    public function done(Request $request)
    {
        $id = $request->id;
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $create = SurveyLocus::find($id);
        $create->status = 1;
        $create->save();
        if($create){
            return response()->json([
                'status' => true,
                'message' => 'Berhasil mengubah Survey Locus'
            ],Response::HTTP_CREATED);
        }else{
            return response()->json([
                'message' => 'Gagal mengubah Survey Locus',
            ],Response::HTTP_BAD_REQUEST);
        }
    }

    public function getData(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $data = SurveyLocus::with(['locus', 'surveyLocusPenilaian', 'surveyLocusPenilaian.pertanyaan.penilaian'])->where('deleted_at')->where('user_id', $request->user_id)->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Data Survey Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }
}
