<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Kelurahan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class MasterKelurahanController extends Controller
{
    public function all(Request $request){
        $data = Kelurahan::all();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Kelurahan Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getDataAll(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kecamatan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Kelurahan::where('deleted_at')->where('kecamatan_id',$request['id_kecamatan'])->get();
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Kelurahan Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kelurahan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Kelurahan::where('deleted_at')->firstWhere('id',$request['id_kelurahan']);;
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Kelurahan Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function saveKelurahan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kecamatan' => ['required', 'string'],
            'namaKelurahan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                'kecamatan_id'=>$request['id_kecamatan'],
                'name'=> $request['namaKelurahan'],
            ];
            $create = Kelurahan::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Kelurahan'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Kelurahan',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateKelurahan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kelurahan' => ['required', 'string'],
            'id_kecamatan' => ['required', 'string'],
            'namaKelurahan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = [
                'kecamatan_id'=>$request['id_kecamatan'],
                'name'=> $request['namaKelurahan'],
            ];
            $update = Kelurahan::where('id',$request['id_kelurahan'])->update($data);
            if($update){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update Kelurahan'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal update Kelurahan',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
    public function deleteKelurahan(Request $request){
        $validator = Validator::make($request->all(), [
            'id_kelurahan' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $delete = Kelurahan::find($request['id_kelurahan'])->delete();
            if($delete){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil delete Kelurahan'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal delete Kelurahan',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
