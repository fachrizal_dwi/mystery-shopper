<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Provinsi;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MasterProvinsiController extends Controller
{
    public function getDataAll(Request $request){
        $data = Provinsi::where('deleted_at')->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Provinsi Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_provinsi' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Provinsi::where('deleted_at')->firstWhere('id',$request['id_provinsi']);;
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Provinsi Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function saveProvinsi(Request $request){
        $validator = Validator::make($request->all(), [
            'namaProvinsi' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data= [
                'name'=> $request['namaProvinsi'],
            ];
            $create = Provinsi::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create provinsi'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create provinsi',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateProvinsi(Request $request){
        $validator = Validator::make($request->all(), [
            'id_provinsi' => ['required', 'string'],
            'namaProvinsi' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = [
                'name'=> $request['namaProvinsi'],
            ];
            $update = Provinsi::where('id',$request['id_provinsi'])->update($data);
            if($update){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update provinsi'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal update provinsi',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
    public function deleteProvinsi(Request $request){
        $validator = Validator::make($request->all(), [
            'id_provinsi' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $delete = Provinsi::find($request['id_provinsi'])->delete();
            if($delete){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil delete provinsi'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal delete provinsi',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

}
