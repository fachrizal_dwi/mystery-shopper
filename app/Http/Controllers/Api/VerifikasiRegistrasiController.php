<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class VerifikasiRegistrasiController extends LoginController
{
    public function index(Request $request){
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id_user' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $user=user::where('deleted_at')->firstWhere('id',$request['id_user']);
             
            if ($user!=""||$user!=null ) {
                $data = [
                    'email_verified_at' => now(),
                ];
                $update = user::where('id',$request['id_user'])->update($data);
                if($update){
                    return response()->json([
                        'status' => true,
                        'message' => 'Berhasil update user'
                    ], Response::HTTP_OK);
                }else{
                    return response()->json([
                        'message' => 'Gagal update user',
                    ],Response::HTTP_NOT_FOUND);
                }
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'id user tidak di temukan',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
