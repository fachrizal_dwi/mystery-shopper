<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Universitas;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MasterUniversitasController extends Controller
{
    public function getDataAll(Request $request){
            $data = Universitas::with(['provinsi','kotaKabupaten','kecamatan','kelurahan'])->where('deleted_at')->get();
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Universitas Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
    }

    public function getDataByProvince(Request $request){
        $data = Universitas::with(['provinsi','kotaKabupaten','kecamatan','kelurahan'])->where('deleted_at')->where('provinsi_id', $request->provinsi_id)->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Data Universitas Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_universitas' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Universitas::with(['provinsi','kotaKabupaten','kecamatan','kelurahan'])->where('deleted_at')->firstWhere('id',$request['id_universitas']);;
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Universitas Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function saveUniversitas(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'logo' => ['mimes:png,JPG,jpg,jpeg','max:2048'],
        ]);
        if(!$validator->fails()){
            $files=$request->file('logo');
            if($files!=null||$files!=""){
                $files= $request->file('logo')->getClientOriginalName();
            }
            $data= [
                'name' => $request['name'],
                'provinsi_id' => $request['provinsi_id'],
                'kota_kabupaten_id' => $request['kota_kabupaten_id'],
                'kecamatan_id' => $request['kecamatan_id'],
                'kelurahan_id' => $request['kelurahan_id'],
                'alamat' => $request['alamat'],
                'phone' => $request['phone'],
                'logo' => $files,
            ];
            $create = Universitas::create($data);
            if($create){
                if($files!=null||$files!=""){
                    $file = $request->file('logo');
                    $file->move(storage_path('app\public\images\surveyor\logoUniv'), uniqid()."-".$file->getClientOriginalName());
                }
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Universitas'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Universitas',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }

    public function updateUniversitas(Request $request){
        $validator = Validator::make($request->all(), [
            'id_Universitas' => ['required', 'string'],
            'name' => ['required', 'string'],
            'logo' => ['mimes:png,JPG,jpg,jpeg','max:2048'],
        ]);
        if(!$validator->fails()){
            $files=$request->file('logo');
            if($files!=null||$files!=""){
                $files= $request->file('logo')->getClientOriginalName();
            }
            $data = [
                'name' => $request['name'],
                'provinsi_id' => $request['provinsi_id'],
                'kota_kabupaten_id' => $request['kota_kabupaten_id'],
                'kecamatan_id' => $request['kecamatan_id'],
                'kelurahan_id' => $request['kelurahan_id'],
                'alamat' => $request['alamat'],
                'phone' => $request['phone'],
                'logo' =>$files,
            ];
            $update = Universitas::where('id',$request['id_Universitas'])->update($data);
            if($update){
                if($files!=null||$files!=""){
                    $file = $request->file('logo');
                    $file->move(storage_path('app\public\images\surveyor\logoUniv'), uniqid()."-".$file->getClientOriginalName());
                }
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil update Universitas'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal update Universitas',
                ],Response::HTTP_NOT_FOUND);
            }
            
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
    public function deleteUniversitas(Request $request){
        $validator = Validator::make($request->all(), [
            'id_Universitas' => ['required', 'string']
        ]);
        if(!$validator->fails()){
            $delete = Universitas::find($request['id_Universitas'])->delete();
            if($delete){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil delete Universitas'
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal delete Universitas',
                ],Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }
    }
}
