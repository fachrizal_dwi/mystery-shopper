<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Instansi;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MasterIntansiController extends Controller
{
    public function all(Request $request){
        $data = Instansi::all();
        if ($data != "" || $data != null) {
            return response()->json([
                'status' => true,
                'data' => $data,
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'status' => false,
                'data' => 'Data Instansi Tidak Ditemukan'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getDataAll(Request $request){
        $validator = Validator::make($request->all(), [
            'id_provinsi' => ['required', 'string'],
        ]);
        if(!$validator->fails()) {
            $data = Instansi::with(['provinsi', 'kotaKabupaten', 'kecamatan', 'kelurahan', 'kategoriInstansi'])->where('deleted_at')->where('provinsi_id', '=', $request->id_provinsi)->get();
            if ($data != "" || $data != null) {
                return response()->json([
                    'status' => true,
                    'data' => $data,
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'status' => false,
                    'data' => 'Data Instansi Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        } else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }

    public function getDataById(Request $request){
        $validator = Validator::make($request->all(), [
            'id_instansi' => ['required', 'string'],
        ]);
        if(!$validator->fails()){
            $data = Instansi::with(['provinsi','kotaKabupaten','kecamatan','kelurahan','kategoriInstansi'])->where('deleted_at')->firstWhere('id',$request['id_instansi']);
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Data Instansi Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_NOT_FOUND);
        }

    }
}
