<?php


namespace App\Http\Controllers\Api;


use App\Pertanyaan;
use App\KategoriPertanyaan;
use App\SurveyLocus;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PertanyaanController extends LoginController
{
    public function getData(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $data = Pertanyaan::with('kategoriPertanyaan', 'jenisPertanyaan', 'penilaian', 'surveyLocusPenilaian')->where('survey_id', $request->survey_id)->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Data Pertanyaan Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function grouped(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $data = KategoriPertanyaan::with('pertanyaan', 'pertanyaan.jenisPertanyaan', 'pertanyaan.penilaian')->with(['pertanyaan.surveyLocusPenilaians' => function($query) use ($request){
            $query->where('survey_locus_id', '=', $request->survey_locus_id);
        }])->whereHas('pertanyaan', function($query) use ($request){
            $query->where('survey_id', '=', $request->survey_id);
        })->get();
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Data Pertanyaan Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function find(Request $request, $id)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $data = Pertanyaan::with('kategoriPertanyaan', 'jenisPertanyaan', 'surveyLocusPenilaians', 'penilaian')->find($id);
        if($data!=""||$data!=null){
            return response()->json([
                'status' => true,
                'data' =>$data,
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Data Pertanyaan Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }
}
