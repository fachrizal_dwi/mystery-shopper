<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\JenisPertanyaan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class JenisPertanyaanController extends Controller
{
    public function all(Request $request){
        $data = JenisPertanyaan::all();
        if ($data != "" || $data != null) {
            return response()->json([
                'status' => true,
                'data' => $data,
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'status' => false,
                'data' => 'Data JenisPertanyaan Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

}