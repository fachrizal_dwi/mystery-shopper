<?php


namespace App\Http\Controllers\Api;


use App\SurveyLocusDokumentasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

class SurveyLocusDocumentController extends LoginController
{
    public function store(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'survey_locus_id' => ['required', 'string'],
            'file' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
            'longitude' => ['required', 'numeric'],
            'latitude' => ['required', 'numeric'],
        ]);
        if(!$validator->fails()){
            $data= $request->all();
            $data['path'] = 'storage/images/survey/locus/documents/'.uniqid().'.'.$request->file->getClientOriginalExtension();
            $request->file->move(public_path('storage/images/survey/locus/documents/'), $data['path']);
            $create = SurveyLocusDokumentasi::create($data);
            if($create){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil create Survey Locus Document',
                    'id' => $create->id
                ],Response::HTTP_CREATED);
            }else{
                return response()->json([
                    'message' => 'Gagal create Survey Locus Document',
                ],Response::HTTP_BAD_REQUEST);
            }

        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'id' => ['required', 'string'],
            'file' => ['required','mimes:png,JPG,jpg,jpeg','max:2048'],
            'longitude' => ['required', 'numeric'],
            'latitude' => ['required', 'numeric'],
        ]);
        if(!$validator->fails()){
            $data= $request->all();

            $request->file->move(public_path('storage/images/survey/locus/documents/'), $data['file']);
            $item = SurveyLocusDokumentasi::find($data['id']);
            $item->path = 'storage/images/survey/locus/documents/'.uniqid().'.'.$request->file->getClientOriginalExtension();
            $item->longitude = $data['longitude'];
            $item->latitude = $data['latitude'];
            $item->save();
            return response()->json([
                'status' => true,
                'message' => 'Berhasil update Survey Locus Document'
            ],Response::HTTP_CREATED);

        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_BAD_REQUEST);
        }
    }

    public function delete(Request $request, $id)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $create = SurveyLocusDokumentasi::destroy($id);
        if($create){
            return response()->json([
                'status' => true,
                'message' => 'Berhasil menghapus Survey Locus Document'
            ],Response::HTTP_CREATED);
        }else{
            return response()->json([
                'message' => 'Gagal create Survey Locus Document',
            ],Response::HTTP_BAD_REQUEST);
        }
    }

    public function getData(Request $request)
    {
        $cek= $this->cektoken( $request);
        if(!$cek){
            return response()->json([
                'status' => false,
                'message' => 'Token tidak terbaca'
            ],Response::HTTP_BAD_REQUEST);
        }
        $validator = Validator::make($request->all(), [
            'survey_locus_id' => ['required', 'string']
        ]);
        if(!$validator->fails()){        
            $data = SurveyLocusDokumentasi::where('survey_locus_id', '=', $request->survey_locus_id)->get();
            if($data!=""||$data!=null){
                return response()->json([
                    'status' => true,
                    'data' =>$data,
                ], Response::HTTP_OK);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Data Survey Locus Document Tidak Ditemukan',
                ], Response::HTTP_NOT_FOUND);
            }
        }else{
            return response()->json([
                'messages' => $validator->errors(),
            ],Response::HTTP_BAD_REQUEST);
        }    
    }
}
