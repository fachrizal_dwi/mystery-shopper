<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;

class StorageController extends Controller
{
    public function get(Request $request)
    {
        $path = $request->get('path');
        return Storage::download($path);
    }

    public function read(Request $request)
    {
        $path = $request->get('path');
        return Storage::get($path);
    }
}