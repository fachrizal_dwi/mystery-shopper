<?php

namespace App\Http\Controllers\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\KategoriPertanyaan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class KategoriPertanyaanController extends Controller
{
    public function all(Request $request){
        $data = KategoriPertanyaan::all();
        if ($data != "" || $data != null) {
            return response()->json([
                'status' => true,
                'data' => $data,
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'status' => false,
                'data' => 'Data KategoriPertanyaan Tidak Ditemukan',
            ], Response::HTTP_NOT_FOUND);
        }
    }

}