<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\KategoriPertanyaan;
use Illuminate\Http\Request;
use DataTables;

class KategoriPertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pertanyaan.kategori.show');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new KategoriPertanyaan();
        return view('admin.pertanyaan.kategori.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** vallidate dis input */
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'bobot_minimal' => 'required|numeric|min:-100|max:100',
            'alias' => 'max:20',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        /** end validate */

        /** @var throw request $input */
        $input = $request->all();

        /** aplut logo */
        $input['logo']= null;
        if ($request->hasFile('logo')){
            $input['logo'] = 'storage/images/kategori-pertanyaan/logo/'.uniqid().'.'.$request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('storage/images/kategori-pertanyaan/logo/'), $input['logo']);
        }
        KategoriPertanyaan::create($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = KategoriPertanyaan::findOrFail($id);
        return view('admin.pertanyaan.kategori.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** vallidate dis input */
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'bobot_minimal' => 'required|numeric|min:-100|max:100',
            'alias' => 'max:20',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        /** end validate */

        /** @var start update pertanyaan $model */
        $model = KategoriPertanyaan::findOrFail($id);
        $model->update([
            'name' => $request['name'],
            'alias' => $request['alias'],
            'bobot_minimal' => $request['bobot_minimal'],
        ]);
        /** @var end update pertanyaan $input */

        /** @var check logo & update if has image $input */
        $input = $model;
        if ($request->hasFile('logo')){
            if (!$model->logo == NULL){
                unlink(public_path($model->logo));
            }
            $model->logo = 'storage/images/kategori-pertanyaan/logo/'.uniqid().'.'.$request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('storage/images/kategori-pertanyaan/logo/'), $input['logo']);
            $model->save();
        }
        /** @var END check logo & update if has image $input */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = KategoriPertanyaan::findOrFail($id);
        $model->delete();
    }

    public function dataTable()
    {
        $model = KategoriPertanyaan::query();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('admin.pertanyaan.kategori._action', [
                    'model' => $model,
                    'url_edit' => route('admin.kategori-pertanyaan.edit', $model->id),
                    'url_destroy' => route('admin.kategori-pertanyaan.destroy', $model->id)
                ]);
            })
            ->addColumn('logo', function($model){
                if ($model->logo == NULL){
                    $url_photo = url('dist/images/no-image-20.png');
                }else{
                    $url_photo = url($model->logo);
                }
                return view('admin.pertanyaan._image',[
                    'url_photo' => $url_photo
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
