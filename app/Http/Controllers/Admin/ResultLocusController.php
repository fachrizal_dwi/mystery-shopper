<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Instansi;
use App\Locus;
use App\Survey;
use App\SurveyLocus;
use App\SurveyLocusDokumentasi;
use App\ViewBobotPenilaianBySurveyLocus;
use App\ViewBobotPenilaianBySurveyLocusDetail;
use App\ViewPenilaianKategoriByLocus;
use Illuminate\Http\Request;
use DataTables;

class ResultLocusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($survey_id, $instansi_id, $locus_id)
    {
        $survey_name = Survey::select('name')->find($survey_id);
        $instansi_name = Instansi::select('name')->find($instansi_id);
        $locus = Locus::select('id', 'name')->find($locus_id);
        $survey_locus_id = SurveyLocus::select('id')
            ->where('survey_id', '=', $survey_id)
            ->where('locus_id', '=', $locus_id)
            ->first();
        $dokumentasis = SurveyLocusDokumentasi::query()->where('survey_locus_id', '=', $survey_locus_id->id)->get();
        return view('admin.result.locus.show', compact( 'survey_id', 'instansi_id', 'locus', 'survey_name', 'instansi_name', 'dokumentasis', 'survey_locus_id'));
    }


    public function map($survey_locus_id, $locus_id)
    {
        $model = SurveyLocusDokumentasi::with('surveyLocus')->find($survey_locus_id);
        $locus = Locus::select('name')->find($locus_id);
        return view('admin.result.locus.map', compact('model', 'locus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = SurveyLocus::with('surveyLocusDokumentasi', 'surveyLocusPenilaian')->findOrFail($id);
        $model->surveyLocusDokumentasi->each->delete();
        $model->surveyLocusPenilaian->each->delete();
        $model->delete();
    }

    public function dataTable($survey_id, $instansi_id)
    {
        $model = ViewPenilaianKategoriByLocus::query()
            ->select('view_penilaian_kategori_by_locuses.*','locuses.name as locus_name', 'kota_kabupatens.name as kota_kabupaten_name')
            ->leftJoin('kota_kabupatens', 'view_penilaian_kategori_by_locuses.kota_kabupaten_id', '=', 'kota_kabupatens.id')
            ->leftJoin('locuses', 'view_penilaian_kategori_by_locuses.locus_id', '=', 'locuses.id')
            ->where('survey_id','=', $survey_id)
            ->where('instansi_id','=', $instansi_id);
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('admin.result.locus._action', [
                    'model' => $model,
                    'url_show' => route('admin.result-locus.show', [$model->survey_id ,$model->instansi_id, $model->locus_id])
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function dataDetail($survey_id, $locus_id)
    {
        $model = ViewBobotPenilaianBySurveyLocus::query()
            ->select('view_bobot_penilaian_by_survey_locuses.*', 'users.name as surveyor_name', 'pertanyaans.name as pertanyaan', 'kategori_pertanyaans.name as kategori')
            ->leftJoin('users', 'view_bobot_penilaian_by_survey_locuses.user_id', '=', 'users.id')
            ->leftJoin('pertanyaans', 'view_bobot_penilaian_by_survey_locuses.pertanyaan_id', '=', 'pertanyaans.id')
            ->leftJoin('kategori_pertanyaans', 'view_bobot_penilaian_by_survey_locuses.kategori_pertanyaan_id', '=', 'kategori_pertanyaans.id')
            ->where('view_bobot_penilaian_by_survey_locuses.survey_id','=', $survey_id)
            ->where('view_bobot_penilaian_by_survey_locuses.locus_id','=', $locus_id);
        return DataTables::of($model)
            ->addIndexColumn()
            ->make(true);
    }
}
