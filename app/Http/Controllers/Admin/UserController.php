<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new User();
        $userTypes = UserType::where('id','>', Auth::user()->user_type_id)
            ->where('id','<', 3)
            ->pluck('name', 'id');
        return view('admin.user.form', compact ('model', 'userTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'user_type_id' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $data = $request->all();
        User::create([
            'name' => $data['name'],
            'user_type_id' => $data['user_type_id'],
            'email' => $data['email'],
            'email_verified_at' => $data['email_verified_at'],
            'uid' => uniqid(),
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = User::find($id);
        if ($request['toggle'] == 1):
            $model->email_verified_at = now();
        else:
            $model->email_verified_at = null;
        endif;
        $model->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = User::findOrFail($id);
        $model->delete();
    }

    public function dataTable()
    {
        $model = User::query()
            ->leftJoin('user_types', 'users.user_type_id', '=', 'user_types.id')
            ->select('users.*','user_types.name as user_type_name')
            ->whereIn('user_type_id', [0,1,2])
            ->where('user_type_id', '>', Auth::user()->user_type_id);
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('admin.user._action', [
                    'model' => $model,
                    'url_destroy' => route('admin.user.destroy', $model->id)
                ]);
            })
            ->addColumn('toggle', function ($model) {
                return view('admin.user._toggle', [
                    'model' => $model,
                    'url_toggle' => route('admin.user.update', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'toggle'])
            ->make(true);
    }
}
