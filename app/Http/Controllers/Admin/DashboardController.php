<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ShpProvinsi;
use App\Survey;
use App\ViewPenilaianInstansiByPercentage;
use App\ViewPenilaianSurveyByLocusData;
use App\ViewPenilaianSurveyByInstansi;
use App\ViewShpProvinceMap;
use App\ViewShpProvinceMapByKotaKabupatenMap;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Survey::pluck('name', 'id');
        return view('admin.dashboard.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $models = Survey::pluck('name', 'id');
        $province_id = ViewPenilaianInstansiByPercentage::where('survey_id', '=', $id)
            ->select('provinsi_id')
            ->distinct('provinsi_id')
            ->get();
        //view_shp_province_maps
        $shp_provinsis = ViewShpProvinceMap::whereIn('provinsi_id', $province_id)
            ->select('*')
            ->get();
        $shp_kota_kabupatens = ViewShpProvinceMapByKotaKabupatenMap::where('survey_id', '=', $id)
            ->select('*')
            ->get();
        $instansis = ViewPenilaianSurveyByInstansi::where('survey_id', '=', $id)
            ->select('*')
            ->get();
        $locuses = ViewPenilaianSurveyByLocusData::query()
            ->select('name', 'kota_kabupaten_name', 'lulus', 'class', 'instansi_id', 'style')
            ->where('survey_id', '=', $id)
            ->get();
        return view('admin.dashboard.show', compact('models', 'province_id', 'shp_provinsis', 'shp_kota_kabupatens', 'instansis', 'locuses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
