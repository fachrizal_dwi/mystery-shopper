<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\UserProfile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;

class SurveyorApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.surveyor.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = UserProfile::with('universitas', 'provinsi', 'kotaKabupaten', 'kecamatan', 'kelurahan')->find($id);
        $model->tanggal_lahir = $model->tanggal_lahir ? with(new Carbon($model->tanggal_lahir))->format('j F Y') : '';
        return view('admin.surveyor.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = User::findOrFail($id);
        if($model) {
            $model->email_verified_at =  now();
            $model->user_type_id =  3;
            $model->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = UserProfile::findOrFail($id);
        $model->delete();
    }

    public function dataTable()
    {
        $inputStatus = (!empty($_GET["inputStatus"])) ? ($_GET["inputStatus"]) : ('');
        if($inputStatus && $inputStatus == 1):
            $unapprove = User::whereNotNull('email_verified_at')->select('id');
        else:
            $unapprove = User::where('email_verified_at', null)->select('id');
        endif;
        $model = UserProfile::query()
            //with('provinsi', 'kotaKabupaten', 'kecamatan', 'kelurahan')
            ->leftJoin('universitas', 'user_profiles.universitas_id', '=', 'universitas.id')
            ->leftJoin('provinsis', 'user_profiles.provinsi_id', '=', 'provinsis.id')
            ->leftJoin('kota_kabupatens', 'user_profiles.kota_kabupaten_id', '=', 'kota_kabupatens.id')
            ->leftJoin('kecamatans', 'user_profiles.kecamatan_id', '=', 'kecamatans.id')
            ->leftJoin('kelurahans', 'user_profiles.kelurahan_id', '=', 'kelurahans.id')
            ->whereIn('user_id', $unapprove)
        ->select('user_profiles.*', 'universitas.name as universitas_name', 'provinsis.name as provinsi_name', 'kota_kabupatens.name as kota_kabupaten_name', 'kecamatans.name as kecamatan_name', 'kelurahans.name as kelurahan_name');
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('admin.surveyor._action', [
                    'model' => $model,
                    'url_show' => route('admin.surveyor-approval.show', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
