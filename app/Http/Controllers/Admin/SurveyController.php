<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notification;
use App\Penilaian;
use App\Pertanyaan;
use App\Survey;
use App\KategoriSurvey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $count = (Survey::where('until', '<=' , now())->count()) - (Notification::where('user_id', '=' , Auth::user()->id)->count());
        if ( $count > 0):
            $user_id = Auth::user()->id;
            $notification_survey_id = Notification::select('survey_id')->where('user_id', '=', Auth::user()->id)->get();
            $list_survey_id =  Survey::select("id as survey_id", DB::raw("'$user_id' as user_id"))->whereNotIn('id', $notification_survey_id)->get();
//            Notification::create($list_survey_id);
        foreach ($list_survey_id as $survey):
            Notification::create([
                'user_id' => $survey->user_id,
                'survey_id' => $survey->survey_id,
            ]);
        endforeach;;
        endif;
        return view('admin.survey.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Survey();
        $kategori = KategoriSurvey::pluck('name', 'id');
        return view('admin.survey.form', compact ('model', 'kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'kategori_survey_id' => 'required',
            'bobot_minimal' => 'required|integer|min:1',
            'from' => 'required|date',
            'until' => 'required|date'
        ]);
        $request['is_publish'] = $request['is_publish'] == 'is_publish' ? true : false ;
        $kategori = null;
        if(is_numeric($request['kategori_survey_id'])):
            $kategori  = KategoriSurvey::find($request['kategori_survey_id']);
        endif;

        if(is_null($kategori)):
            $request['kategori_survey_id'] = KategoriSurvey::insertGetId([
                'name' => $request['kategori_survey_id']
            ]);
        endif;
        $input = $request->all();
        $model = Survey::create($input);
        if ($request['is_publish'])
        {
            Http::get(config('websocket_server') . '/notifikasi', ['message' => 'Ada survey baru: ' . $request['name']]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Survey::with('kategoriSurvey', 'surveyStatus')->find($id);
        $pertanyaan = Pertanyaan::with('kategoriPertanyaan', 'jenisPertanyaan', 'penilaian')->where('survey_id', $id)->get();
        return view('admin.survey.show', compact('model','pertanyaan'));
    }

    public function test()
    {
        $pertanyaan = Pertanyaan::with('kategoriPertanyaan', 'jenisPertanyaan', 'penilaian')->where('survey_id', 1)->get();
        dd($pertanyaan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Survey::find($id);
        $kategori = KategoriSurvey::pluck('name', 'id');
        return view('admin.survey.form', compact ('model', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'kategori_survey_id' => 'required',
            'bobot_minimal' => 'required|integer|min:1',
            'from' => 'required|date',
            'until' => 'required|date'
        ]);
        $request['is_publish'] = $request['is_publish'] == 'is_publish' ? true : false ;
        $kategori = null;
        if(is_numeric($request['kategori_survey_id'])):
            $kategori  = KategoriSurvey::find($request['kategori_survey_id']);
        endif;

        if(is_null($kategori)):
            $request['kategori_survey_id'] = KategoriSurvey::insertGetId([
                'name' => $request['kategori_survey_id']
            ]);
        endif;

        $input = $request->all();
        $model = Survey::find($id);
        $valid = $model->is_publish == false && $request['is_publish'] == true;
        $model->update($input);

        if ($valid)
        {
            Http::get(config('websocket_server') . '/notifikasi', ['message' => 'Ada survey baru: ' . $request['name']]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Survey::findOrFail($id);
        $model->delete();
    }

    public function dataTable()
    {
        $model = Survey::query()
            ->leftJoin('kategori_surveys', 'surveys.kategori_survey_id', '=', 'kategori_surveys.id')
            ->leftJoin('survey_statuses', 'surveys.survey_status_id', '=', 'survey_statuses.id')
            ->select('surveys.*','kategori_surveys.name as kategori_survey_name','survey_statuses.name as survey_status_name');
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action', [
                    'model' => $model,
                    'url_show' => route('admin.survey.show', $model->id),
                    'url_edit' => route('admin.survey.edit', $model->id),
                    'url_destroy' => route('admin.survey.destroy', $model->id)
                ]);
            })
            ->addColumn('manage', function ($model) {
                return '<a href="'.route('admin.pertanyaan.index', $model->id).'" class="btn btn-outline-primary" title="Manage Soal"><i class="icon-settings"></i></a>';
            })
            ->editColumn('from', function ($model) {
                return $model->from ? with(new Carbon($model->from))->format('j F Y') : '';
            })
            ->editColumn('until', function ($model) {
                return $model->until ? with(new Carbon($model->until))->format('j F Y') : '';
            })
            ->addIndexColumn()
            ->rawColumns(['action','manage'])
            ->make(true);
    }
}
