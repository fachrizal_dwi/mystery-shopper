<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Survey;
use Illuminate\Http\Request;
use App\ViewPenilaianInstansiByPercentage;
use DataTables;

class ResultInstansiController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = Survey::pluck('name', 'id');
        return view('admin.result.instansi.index', compact('surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($survey_id, $instansi_id)
    {
        return view('admin.result.instansi.show', compact('survey_id', 'instansi_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dataTable()
    {
        $filterSurvey = (!empty($_GET["filterSurvey"])) ? ($_GET["filterSurvey"]) : ('');
        if ($filterSurvey):
            $model = ViewPenilaianInstansiByPercentage::query()
                ->select('view_penilaian_instansi_by_percentages.*', 'surveys.name as survey_name', 'instansis.name as instansi_name', 'provinsis.name as provinsi_name')
                ->leftJoin('surveys', 'view_penilaian_instansi_by_percentages.survey_id', '=', 'surveys.id')
                ->leftJoin('instansis', 'view_penilaian_instansi_by_percentages.instansi_id', '=', 'instansis.id')
                ->leftJoin('provinsis', 'view_penilaian_instansi_by_percentages.provinsi_id', '=', 'provinsis.id')
                ->where('survey_id','=', $filterSurvey);
        else:
            $model = ViewPenilaianInstansiByPercentage::query()
                ->select('view_penilaian_instansi_by_percentages.*', 'surveys.name as survey_name', 'instansis.name as instansi_name', 'provinsis.name as provinsi_name')
                ->leftJoin('surveys', 'view_penilaian_instansi_by_percentages.survey_id', '=', 'surveys.id')
                ->leftJoin('instansis', 'view_penilaian_instansi_by_percentages.instansi_id', '=', 'instansis.id')
                ->leftJoin('provinsis', 'view_penilaian_instansi_by_percentages.provinsi_id', '=', 'provinsis.id');
        endif;
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('admin.result.instansi._action', [
                    'model' => $model,
                    'url_show' => route('admin.result-instansi.show', [$model->survey_id ,$model->instansi_id])
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
