<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\JenisPertanyaan;
use App\KategoriPertanyaan;
use App\Penilaian;
use App\Pertanyaan;
use DataTables;
use Illuminate\Http\Request;
use App\Survey;

class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($survey_id)
    {
        $survey = Survey::find($survey_id);
        return view('admin.pertanyaan.index',compact('survey'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($survey_id)
    {
        $model = new Pertanyaan();
        $jenis = JenisPertanyaan::pluck('name', 'id');
        $kategori = KategoriPertanyaan::pluck('name', 'id');
        return view('admin.pertanyaan.form', compact ('model','jenis','kategori', 'survey_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** validate */
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'bobot' => 'required|numeric|min:-100|max:100',
            'alias' => 'max:20',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'kategori_pertanyaan_id' => 'required',
            'jenis_pertanyaan_id' => 'required',
            'penilaian_name.*'  => 'required|string',
            'penilaian_bobot.*'  => 'required|numeric|min:-100|max:100'
        ]);
        /** end validate */

        /** check kategori */
        $input = $request->all();
        $kategori = null;
        if(is_numeric($request['kategori_pertanyaan_id'])):
            $kategori  = KategoriPertanyaan::find($request['kategori_pertanyaan_id']);
        endif;
        if(is_null($kategori)):
            $input['kategori_pertanyaan_id'] = KategoriPertanyaan::insertGetId([
                'name' => $input['kategori_pertanyaan_id']
            ]);
        endif;
        /** end check kategori */

        /** aplut logo */
        $input['logo']= null;
        if ($request->hasFile('logo')){
            $input['logo'] = 'storage/images/pertanyaan/logo/'.uniqid().'.'.$request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('storage/images/pertanyaan/logo/'), $input['logo']);
        }
        /** end aplut */

        /** insert pertanyaan */
        $pertanyaan_id = Pertanyaan::insertGetId([
            'survey_id' => $input['survey_id'],
            'kategori_pertanyaan_id' => $input['kategori_pertanyaan_id'],
            'jenis_pertanyaan_id'=> $input['jenis_pertanyaan_id'],
            'bobot'=> $input['bobot'],
            'name'=> $input['name'],
            'alias'=> $input['alias'],
            'logo'=> $input['logo'],
            'created_at' => now(),
            'updated_at' => now()
        ]);
        /** end insert pertanyaan */

        /** insert penilaian */
        $penilaian_name = $request->penilaian_name;
        $penilaian_bobot = $request->penilaian_bobot;
        for($count = 0; $count < count($penilaian_name); $count++)
        {
            $data = array(
                'pertanyaan_id' => $pertanyaan_id,
                'name' => $penilaian_name[$count],
                'bobot'  => $penilaian_bobot[$count],
                'created_at' => now(),
                'updated_at' => now()
            );
            $insert_data[] = $data;
        }
        Penilaian::insert($insert_data);
        /** end insert penilaian */

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Pertanyaan::with('kategoriPertanyaan', 'jenisPertanyaan', 'penilaian')->find($id);
        return view('admin.pertanyaan.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Pertanyaan::with('penilaian')->find($id);
        $jenis = JenisPertanyaan::pluck('name', 'id');
        $kategori = KategoriPertanyaan::pluck('name', 'id');
        $survey_id = $model->survey_id;
        return view('admin.pertanyaan.form', compact ('model','jenis','kategori', 'survey_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'bobot' => 'required|numeric|min:-100|max:100',
            'alias' => 'max:20',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'kategori_pertanyaan_id' => 'required',
            'jenis_pertanyaan_id' => 'required',
            'penilaian_name.*'  => 'required|string',
            'penilaian_bobot.*'  => 'required|numeric|min:-100|max:100'
        ]);
        /** end validate */

        /** check kategori */
        $kategori = null;
        if(is_numeric($request['kategori_pertanyaan_id'])):
            $kategori  = KategoriPertanyaan::find($request['kategori_pertanyaan_id']);
        endif;

        if(is_null($kategori)):
            $request['kategori_pertanyaan_id'] = KategoriPertanyaan::insertGetId([
                'name' => $request['kategori_pertanyaan_id']
            ]);
        endif;
        /** end check kategori */

        /** @var start update pertanyaan $model */
        $model = Pertanyaan::findOrFail($id);
        $model->update([
            'name' => $request['name'],
            'bobot' => $request['bobot'],
            'alias' => $request['alias'],
            'kategori_pertanyaan_id' => $request['kategori_pertanyaan_id'],
            'jenis_pertanyaan_id' => $request['jenis_pertanyaan_id']
        ]);
        /** @var end update pertanyaan $input */

        /** @var check logo & update if has image $input */
        $input = $model;
        if ($request->hasFile('logo')){
            if (!$model->logo == NULL){
                unlink(public_path($model->logo));
            }
            $model->logo = 'storage/images/pertanyaan/logo/'.uniqid().'.'.$request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('storage/images/pertanyaan/logo/'), $input['logo']);
            $model->save();
        }
        /** @var END check logo & update if has image $input */

        /** Create or update penilaian */
        Penilaian::where('pertanyaan_id', $model->id)->delete();
        $penilaian_name = $request->penilaian_name;
        $penilaian_bobot = $request->penilaian_bobot;
        for($count = 0; $count < count($penilaian_name); $count++)
        {
            $data = array(
                'pertanyaan_id' => $model->id,
                'name' => $penilaian_name[$count],
                'bobot'  => $penilaian_bobot[$count],
                'created_at' => now(),
                'updated_at' => now()
            );
            $insert_data[] = $data;
        }

        Penilaian::insert($insert_data);
        /** end CREATE or Update penilaian */

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Pertanyaan::with('penilaian')->findOrFail($id);
        $model->penilaian->each->delete();
        $model->delete();
    }

    public function dataTable()
    {
        $surveyId = (!empty($_GET["surveyId"])) ? ($_GET["surveyId"]) : ('');
        $model = Pertanyaan::with('kategoriPertanyaan', 'jenisPertanyaan')->where('survey_id', '=', $surveyId);
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action', [
                    'model' => $model,
                    'url_show' => route('admin.pertanyaan.show', $model->id),
                    'url_edit' => route('admin.pertanyaan.edit', $model->id),
                    'url_destroy' => route('admin.pertanyaan.destroy', $model->id)
                ]);
            })
            ->addColumn('logo', function($model){
                if ($model->logo == NULL){
                    $url_photo = url('dist/images/no-image-20.png');
                }else{
                    $url_photo = url($model->logo);
                }
                return view('admin.pertanyaan._image',[
                    'url_photo' => $url_photo
                ]);
            })
            ->addColumn('jenis', function($model){
                if ($model->jenis_pertanyaan_id == 1){
                    $url_photo = url('dist/images/radio.png');
                }elseif($model->jenis_pertanyaan_id == 2){
                    $url_photo = url('dist/images/checkbox.png');
                }elseif($model->jenis_pertanyaan_id == 3){
                $url_photo = url('dist/images/star.png');
                }
                return view('admin.pertanyaan._image',[
                    'url_photo' => $url_photo
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action','manage'])
            ->make(true);
    }

}
