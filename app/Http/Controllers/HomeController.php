<?php

namespace App\Http\Controllers;

use App\FrontEndContent;
use Illuminate\Http\Request;
use Gate;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Gate::allows('SuperAdmin') || Gate::allows('Verifikator') || Gate::allows('Stakeholder')):
            return (new Admin\DashboardController())->index();
        elseif (Gate::allows('Unasigned')):
            return (new Surveyor\ProfileController())->index();
        elseif (Gate::allows('Surveyor')):
            $model = FrontEndContent::first();
            return view('front.index', compact('model'));
        else:
            Auth::logout();
            return redirect('/login');
        endif;
    }
}
