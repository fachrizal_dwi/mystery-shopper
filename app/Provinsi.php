<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Provinsi extends Model
{
    use SoftDeletes;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    protected $dates = ['deleted_at'];

    public function kotaKabupaten()
    {
        return $this->belongsTo('App\KotaKabupaten');
    }

    public function locus() {
        return $this->hasMany('App\Locus');
    }

    public function instansi() {
        return $this->hasMany('App\Instansi');
    }

    public function universitas() {
        return $this->hasMany('App\Universitas');
    }

    public function userProfile() {
        return $this->hasMany(UserProfile::class);
    }

}
