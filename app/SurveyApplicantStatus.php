<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class SurveyApplicantStatus extends Model
{
    // use SoftDeletes;
    use Notifiable;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    // protected $dates = ['deleted_at'];

    public function surveyApplicant() {
        return $this->hasMany('App\SurveyApplicant');
    }

}
