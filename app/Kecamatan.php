<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;


class Kecamatan extends Model
{
    use SoftDeletes;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kota_kabupaten_id','name',
    ];

    protected $dates = ['deleted_at'];

    public function kotaKabupaten() {
        return $this->hasMany('App\KotaKabupaten');
    }
    
    public function locus() {
        return $this->hasMany('App\Locus');
    }

    public function kelurahan()
    {
        return $this->belongsTo('App\Kelurahan');
    }
    public function instansi() {
        return $this->hasMany('App\Instansi');
    }
    public function universitas() {
        return $this->hasMany('App\Universitas');
    }
}
