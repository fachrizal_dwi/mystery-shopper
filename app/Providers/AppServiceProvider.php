<?php

namespace App\Providers;

use App\Notification;
use App\Survey;
use App\User;
use App\UserProfile;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');
        $unapprove = User::where('email_verified_at', null)->select('id');
        $data['unapprove'] = UserProfile::whereIn('user_id', $unapprove)->count();
        View::share($data);
    }
}
