<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class HowToContent extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'title', 'content',
    ];

    protected $dates = ['deleted_at'];
}
