<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_statuses', function (Blueprint $table) {
            $table->id()->index();
            $table->string('name')->index();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('survey_statuses')->insert([
            'id' => 1,
            'name' => 'OPEN',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('survey_statuses')->insert([
            'id' => 2,
            'name' => 'ON GOING',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('survey_statuses')->insert([
            'id' => 3,
            'name' => 'CLOSED',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_statuses');
    }
}
