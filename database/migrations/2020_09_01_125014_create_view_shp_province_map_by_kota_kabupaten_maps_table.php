<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewShpProvinceMapByKotaKabupatenMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_shp_province_map_by_kota_kabupaten_maps AS
        SELECT a.survey_id,
               a.provinsi_id,
               a.kota_kabupaten_id,
               a.pertanyaan,
               a.hasil,
               a.qty_locus,
               a.qty_lulus,
               b.kota_kabupaten_name,
               CASE
                   WHEN a.pertanyaan = a.hasil THEN 'blue'::text
                   ELSE 'red'::text
                   END              AS color,
               st_asgeojson(b.geom) AS geojson
        FROM view_shp_province_map_by_kota_kabupatens a
                 LEFT JOIN shp_kota_kabupatens b ON a.kota_kabupaten_id = b.kota_kabupaten_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_shp_province_map_by_kota_kabupaten_maps" CASCADE');
    }
}
