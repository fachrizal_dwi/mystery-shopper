<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstansisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instansis', function (Blueprint $table) {
            $table->id()->index();
            $table->string('name');
            $table->unsignedBigInteger('code')->unsigned()->unique()->index();
            $table->bigInteger('kategori_instansi_id')->nullable();
            $table->foreign('kategori_instansi_id')->references('id')->on('kategori_instansis');
            $table->unsignedBigInteger('provinsi_id')->nullable();
            $table->foreign('provinsi_id')->references('id')->on('provinsis');
            $table->unsignedBigInteger('kota_kabupaten_id')->nullable();
            $table->foreign('kota_kabupaten_id')->references('id')->on('kota_kabupatens');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instansis');
    }
}
