<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewPenilaianByKategoriPertanyaanSurveyLocusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_penilaian_by_kategori_pertanyaan_survey_locuses AS
        select a.survey_id,
               a.locus_id,
               a.user_id,
               a.kategori_pertanyaan_id,
               b.bobot_minimal,
               sum(a.nilai_pertanyaan) as summary,
               case when b.bobot_minimal > sum(a.nilai_pertanyaan) then false else true end as result
        from view_bobot_penilaian_by_survey_locuses a
        left join kategori_pertanyaans b on a.kategori_pertanyaan_id = b.id
        group by a.survey_id,
                 a.locus_id,
                 a.user_id,
                 b.bobot_minimal,
                 a.kategori_pertanyaan_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_penilaian_by_kategori_pertanyaan_survey_locuses" CASCADE');
    }
}
