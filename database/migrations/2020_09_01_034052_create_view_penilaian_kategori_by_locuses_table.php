<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewPenilaianKategoriByLocusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_penilaian_kategori_by_locuses AS
        SELECT a.survey_id,
               a.locus_id,
               i.code                                                 as instansi_code,
               i.id                                                 as instansi_id,
               i.provinsi_id,
               i.kota_kabupaten_id,
               count(a.kategori_pertanyaan_id)                        as kategori_pertanyaan_count,
               COALESCE(sum(CASE WHEN a.result THEN 1 ELSE 0 END), 0) as result,
		        (select sum (summary) from view_penilaian_by_kategori_pertanyaan_survey_locuses k where k.survey_id = a.survey_id and k.locus_id = a.locus_id) as summary
        FROM view_penilaian_by_kategori_pertanyaan_survey_locuses a
                 left join locuses l on a.locus_id = l.id
                 left join instansis i on l.instansi_code = i.code
        group by a.survey_id, a.locus_id, i.code, i.provinsi_id,
               i.id, i.kota_kabupaten_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_penilaian_kategori_by_locuses" CASCADE');
    }
}
