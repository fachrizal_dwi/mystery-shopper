<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyApplicantStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_applicant_statuses', function (Blueprint $table) {
            $table->id()->index();
            $table->string('name')->index();
            $table->timestamps();
        });
        DB::table('survey_applicant_statuses')->insert([
            'name' => 'Peserta',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('survey_applicant_statuses')->insert([
            'name' => 'Rejected',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_applicant_statuses');
    }
}
