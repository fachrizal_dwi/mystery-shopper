<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewPenilaianBySurveyLocusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_penilaian_by_survey_locuses AS
        select d.survey_id,
               d.locus_id,
               d.user_id,
               a.pertanyaan_id,
               b.kategori_pertanyaan_id,
               b.bobot as bobot_pertanyaan,
               c.bobot
        from survey_locus_penilaians a
                 left join pertanyaans b on a.pertanyaan_id = b.id
                 left join penilaians c on a.penilaian_id = c.id
                 left join survey_locuses d on a.survey_locus_id = d.id
        where a.deleted_at is null
          and b.jenis_pertanyaan_id = 1
        UNION
        select d.survey_id,
               d.locus_id,
               d.user_id,
               a.pertanyaan_id,
               b.kategori_pertanyaan_id,
               b.bobot as bobot_pertanyaan,
               sum(c.bobot) as bobot
        from survey_locus_penilaians a
                 left join pertanyaans b on a.pertanyaan_id = b.id
                 left join penilaians c on a.penilaian_id = c.id
                 left join survey_locuses d on a.survey_locus_id = d.id
        where a.deleted_at is null
          and b.jenis_pertanyaan_id = 2
        group by d.survey_id,
                 d.locus_id,
                 d.user_id,
                 a.pertanyaan_id,
                 b.kategori_pertanyaan_id,
                 b.bobot
        UNION
        select d.survey_id,
               d.locus_id,
               d.user_id,
               a.pertanyaan_id,
               b.kategori_pertanyaan_id,
               b.bobot as bobot_pertanyaan,
               (select sum(bobot) from penilaians where pertanyaan_id = a.pertanyaan_id and id >= a.penilaian_id) as bobot
        from survey_locus_penilaians a
                 left join pertanyaans b on a.pertanyaan_id = b.id
                 left join penilaians c on a.penilaian_id = c.id
                 left join survey_locuses d on a.survey_locus_id = d.id
        where a.deleted_at is null
          and b.jenis_pertanyaan_id = 3
        order by locus_id, pertanyaan_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_penilaian_by_survey_locuses" CASCADE');
    }
}
