<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewPenilaianInstansiByPercentagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_penilaian_instansi_by_percentages AS
        SELECT survey_id,
               instansi_code,
               instansi_id,
               provinsi_id,
               sum(kategori_pertanyaan_count)                       as kategori_pertanyaan_count,
               sum(result)                                          as result,
               round((sum(result) / sum(kategori_pertanyaan_count) * 100),2) as percentage
        from view_penilaian_kategori_by_locuses
        group by survey_id, instansi_code, instansi_id, provinsi_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_penilaian_instansi_by_percentages" CASCADE');
    }
}
