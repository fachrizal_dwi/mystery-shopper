<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyLocusPenilaiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_locus_penilaians', function (Blueprint $table) {
            $table->id();
            $table->integer('survey_locus_id')->index();
            $table->foreign('survey_locus_id')->references('id')->on('survey_locuses');
            $table->integer('pertanyaan_id')->index();
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaans');
            $table->integer('penilaian_id')->index();
            $table->foreign('penilaian_id')->references('id')->on('penilaians');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_locus_penilaians');
    }
}
