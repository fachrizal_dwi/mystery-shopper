<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewShpProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_shp_provinces AS
        SELECT DISTINCT view_penilaian_instansi_by_percentages.provinsi_id,
                        view_penilaian_instansi_by_percentages.survey_id,
                        count(distinct (view_penilaian_instansi_by_percentages.instansi_id))  as qty_instansi,
                        count(*) FILTER (WHERE kategori_pertanyaan_count = result)            as qty_lulus,
                        sum(view_penilaian_instansi_by_percentages.kategori_pertanyaan_count) AS pertanyaan,
                        sum(view_penilaian_instansi_by_percentages.result)                    AS lulus
        FROM view_penilaian_instansi_by_percentages
        GROUP BY view_penilaian_instansi_by_percentages.survey_id, view_penilaian_instansi_by_percentages.provinsi_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_shp_provinces" CASCADE');
    }
}
