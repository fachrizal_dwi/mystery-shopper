<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJenisPertanyaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_pertanyaans', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->string('alias')->nullable();
            $table->string('logo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        //jenis pertanyaan
        DB::table('jenis_pertanyaans')->insert([
            'id' => 1,
            'name' => 'Choice',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('jenis_pertanyaans')->insert([
            'id' => 2,
            'name' => 'CheckBox',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('jenis_pertanyaans')->insert([
            'id' => 3,
            'name' => 'Rating',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_pertanyaans');
    }
}
