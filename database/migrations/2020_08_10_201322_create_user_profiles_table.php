<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('universitas_id')->nullable()->unsigned()->index();
            $table->foreign('universitas_id')->references('id')->on('universitas');
            $table->string('nama_lengkap')->nullable()->index();
            $table->string('nik')->index();
            $table->string('nim')->nullable()->index();
            $table->string('phone')->index();
            $table->string('emergency_call')->nullable()->index();
            $table->date('tanggal_lahir')->nullable()->index();
            $table->string('tempat_lahir')->nullable()->index();
            $table->unsignedBigInteger('provinsi_id')->nullable()->index();
            $table->unsignedBigInteger('kota_kabupaten_id')->nullable()->index();
            $table->unsignedBigInteger('kecamatan_id')->nullable()->index();
            $table->unsignedBigInteger('kelurahan_id')->nullable()->index();
            $table->string('alamat')->nullable()->index();
            $table->string('photo_profile')->nullable()->index();
            $table->string('photo_ktp')->nullable()->index();
            $table->string('photo_selfie')->nullable()->index();
            $table->string('photo_legal')->nullable()->index();
            $table->decimal('longitude', 20, 15)->nullable();
            $table->decimal('latitude', 20, 15)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
