<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_applicants', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('survey_id')->index();
            $table->foreign('survey_id')->references('id')->on('surveys');
            $table->integer('survey_applicant_status_id')->default(1);
            $table->foreign('survey_applicant_status_id')->references('id')->on('survey_applicant_statuses');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_applicants');
    }
}
