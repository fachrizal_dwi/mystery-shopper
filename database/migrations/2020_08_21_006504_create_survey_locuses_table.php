<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyLocusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_locuses', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('survey_id')->index();
            $table->foreign('survey_id')->references('id')->on('surveys');
            $table->integer('locus_id')->index();
            $table->foreign('locus_id')->references('id')->on('locuses');
            $table->integer('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('status_survey_locus')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_locuses');
    }
}
