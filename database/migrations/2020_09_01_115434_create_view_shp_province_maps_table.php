<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewShpProvinceMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_shp_province_maps AS
        select a.*,
               case when cast(pertanyaan as numeric) = cast(lulus as numeric) then 'blue' else 'red' end as color,
               b.provinsi_name,
               ST_AsGeoJSON(geom)                                                                        as geojson
        from view_shp_provinces a
                 left join shp_provinsis b on a.provinsi_id = b.provinsi_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_shp_province_maps" CASCADE');
    }
}
