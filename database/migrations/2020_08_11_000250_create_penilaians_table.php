<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //as soal
        Schema::create('penilaians', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('pertanyaan_id')->unsigned()->default(-1);
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaans');
            $table->string('name')->index();
            $table->float('bobot')->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaians');
    }
}
