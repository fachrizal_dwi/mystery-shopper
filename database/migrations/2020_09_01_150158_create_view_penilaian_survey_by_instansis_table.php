<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewPenilaianSurveyByInstansisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_penilaian_survey_by_instansis AS
        select distinct (v.instansi_id),
                        i.name as instansi_name,
                        v.survey_id,
                        v.provinsi_id,
                        v.kota_kabupaten_id,
                        p.name as provinsi_name,
                        case when sum(v.kategori_pertanyaan_count) = sum(v.result) then 'btn btn-success btn-sm' else 'btn btn-warning btn-sm' end as class,
                        case when sum(v.kategori_pertanyaan_count) = sum(v.result) then 'background-color:#5F76ED' else 'background-color:#F87D7A' end as style
        from view_penilaian_kategori_by_locuses v
        left join instansis i on v.instansi_id = i.id
        left join provinsis p on i.provinsi_id = p.id
        group by v.survey_id, v.instansi_id, v.provinsi_id, i.name, p.name, v.kota_kabupaten_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_penilaian_survey_by_instansis" CASCADE');
    }
}
