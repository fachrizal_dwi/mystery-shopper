<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewBobotPenilaianBySurveyLocusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_bobot_penilaian_by_survey_locuses AS
        select a.*,
               (a.bobot_pertanyaan * (a.bobot /100)) as nilai_pertanyaan
        from view_penilaian_by_survey_locuses a;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_bobot_penilaian_by_survey_locus_detail" CASCADE');
    }
}
