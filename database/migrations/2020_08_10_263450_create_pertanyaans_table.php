<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaans', function (Blueprint $table) {
            $table->id();
            $table->integer('survey_id')->unsigned()->default(-1);
            $table->foreign('survey_id')->references('id')->on('surveys');
            $table->integer('kategori_pertanyaan_id')->unsigned();
            $table->foreign('kategori_pertanyaan_id')->references('id')->on('kategori_pertanyaans');
            $table->integer('jenis_pertanyaan_id')->unsigned();
            $table->foreign('jenis_pertanyaan_id')->references('id')->on('jenis_pertanyaans');
            $table->float('bobot')->unsigned()->default(0);
            $table->string('name')->index();
            $table->string('alias')->nullable();
            $table->string('logo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaans');
    }
}
