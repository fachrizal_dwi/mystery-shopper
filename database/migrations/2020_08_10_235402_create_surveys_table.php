<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id()->index();
            $table->integer('kategori_survey_id')->nullable()->index();
            $table->foreign('kategori_survey_id')->references('id')->on('kategori_surveys');
            $table->integer('survey_status_id')->default(1);
            $table->foreign('survey_status_id')->references('id')->on('survey_statuses');
            $table->string('name')->index();
            $table->integer('bobot_minimal');
            $table->date('from');
            $table->date('until');
            $table->boolean('is_publish')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
