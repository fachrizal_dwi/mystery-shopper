<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewPenilaianSurveyByLocusDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_penilaian_survey_by_locus_data AS
        SELECT a.survey_id,
               a.locus_id,
               l.name,
               a.instansi_id,
               a.provinsi_id,
               a.kota_kabupaten_id,
               kk.name as kota_kabupaten_name,
               a.kategori_pertanyaan_count,
               a.result,
               CASE
                   WHEN a.kategori_pertanyaan_count = a.result THEN 'Lulus'::text
                   ELSE 'Tidak Lulus'::text
                   END AS lulus,
               CASE
                   WHEN a.kategori_pertanyaan_count = a.result THEN 'btn btn-sm'::text
                   ELSE 'btn btn-sm'::text
                   END AS class,
               CASE
                   WHEN a.kategori_pertanyaan_count = a.result THEN 'background-color:#5F76ED'::text
                   ELSE 'background-color:#F87D7A'::text
                   END AS style
        FROM view_penilaian_kategori_by_locuses a
                 LEFT JOIN locuses l on a.locus_id = l.id
                 LEFT JOIN kota_kabupatens kk on a.kota_kabupaten_id = kk.id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_penilaian_survey_by_locus_data" CASCADE');
    }
}
