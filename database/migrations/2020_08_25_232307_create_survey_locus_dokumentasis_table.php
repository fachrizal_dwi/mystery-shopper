<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyLocusDokumentasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_locus_dokumentasis', function (Blueprint $table) {
            $table->id();
            $table->integer('survey_locus_id')->index();
            $table->foreign('survey_locus_id')->references('id')->on('survey_locuses');
            $table->string('path');
            $table->decimal('longitude', 20, 15)->nullable();
            $table->decimal('latitude', 20, 15)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_locus_dokumentasis');
    }
}
