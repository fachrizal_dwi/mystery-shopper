<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniversitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universitas', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->integer('provinsi_id')->nullable()->index();
            $table->integer('kota_kabupaten_id')->nullable()->index();
            $table->integer('kecamatan_id')->nullable()->index();
            $table->integer('kelurahan_id')->nullable()->index();
            $table->string('alamat')->nullable()->index();
            $table->string('phone')->nullable()->index();
            $table->string('logo')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universitas');
    }
}
