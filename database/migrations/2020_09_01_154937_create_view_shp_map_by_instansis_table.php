<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewShpMapByInstansisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_shp_map_by_instansis AS
        SELECT view_penilaian_kategori_by_locuses.survey_id,
               view_penilaian_kategori_by_locuses.provinsi_id,
               view_penilaian_kategori_by_locuses.kota_kabupaten_id,
               view_penilaian_kategori_by_locuses.instansi_id,
               i.name                                                            as instansi_name,
               sum(view_penilaian_kategori_by_locuses.kategori_pertanyaan_count) AS pertanyaan,
               sum(view_penilaian_kategori_by_locuses.result)                    AS hasil,
               count(view_penilaian_kategori_by_locuses.locus_id)                AS qty_locus,
               count(*) FILTER (WHERE view_penilaian_kategori_by_locuses.kategori_pertanyaan_count =
                                      view_penilaian_kategori_by_locuses.result) AS qty_lulus
        FROM view_penilaian_kategori_by_locuses
                 left join instansis i on view_penilaian_kategori_by_locuses.instansi_id = i.id
        GROUP BY view_penilaian_kategori_by_locuses.survey_id, view_penilaian_kategori_by_locuses.provinsi_id,
                 view_penilaian_kategori_by_locuses.kota_kabupaten_id,
                 view_penilaian_kategori_by_locuses.instansi_id,
                 i.name;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS "view_shp_map_by_instansis" CASCADE');
    }
}
