<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locuses', function (Blueprint $table) {
            $table->id()->index();
            $table->unsignedBigInteger('code')->unsigned();
            $table->unsignedBigInteger('instansi_code')->unsigned()->nullable();
            $table->foreign('instansi_code')->references('code')->on('instansis');
            $table->string('name')->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locuses');
    }
}
