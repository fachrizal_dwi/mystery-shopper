<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Malesin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //table users
        DB::table('users')->insert([
            'name'=> 'PeTreuQ',
            'user_type_id' => '0',
            'email'=>'petreuq@gmail.com',
            'password'=>bcrypt('password'),
            'email_verified_at' => now(),
            'uid' => uniqid()
        ]);
        DB::table('users')->insert([
            'name'=> 'Admin',
            'user_type_id' => '0',
            'email'=>'admin@mail.com',
            'password'=>bcrypt('password'),
            'email_verified_at' => now(),
            'uid' => uniqid()
        ]);

        //kategori pertanyaan
//        DB::table('kategori_pertanyaans')->insert([
//            'name' => 'Pelayanan',
//            'created_at' => now(),
//            'updated_at' => now()
//        ]);
        /*

        //pertanyaan
        DB::table('pertanyaans')->insert([
            'id' => 1,
            'kategori_pertanyaan_id' => 1,
            'jenis_pertanyaan_id' => 1,
            'name' => 'Prosedur pelayanan yang mudah dan tidak berbelit-belit',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('pertanyaans')->insert([
            'id' => 2,
            'kategori_pertanyaan_id' => 1,
            'jenis_pertanyaan_id' => 1,
            'name' => 'Petugas memenuhi pelayanan yang telah dijanjikan',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('pertanyaans')->insert([
            'id' => 3,
            'kategori_pertanyaan_id' => 1,
            'jenis_pertanyaan_id' => 1,
            'name' => 'Penerima layanan mendapatkan sesuatu yang dibutuhkan dengan cepat',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('pertanyaans')->insert([
            'id' => 4,
            'kategori_pertanyaan_id' => 1,
            'jenis_pertanyaan_id' => 1,
            'name' => 'Penerima layanan mendapatkan kemudahan dalam memperoleh akses dan informasi pelayanan',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('pertanyaans')->insert([
            'id' => 5,
            'kategori_pertanyaan_id' => 1,
            'jenis_pertanyaan_id' => 2,
            'name' => 'Kemampuan petugas untuk cepat tanggap memahami kebutuhan dan permintaan penerima layanan',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('pertanyaans')->insert([
            'id' => 6,
            'kategori_pertanyaan_id' => 1,
            'jenis_pertanyaan_id' => 2,
            'name' => 'Tindakan segera petugas dalam menyelesaikan kebutuhan dan permintaan penerima layanan',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        //kategori penilaian
        DB::table('kategori_penilaians')->insert([
            'name' => 'Buruk Sekali',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        //penilaian

        DB::table('penilaians')->insert([
            'id' => 1,
            'kategori_penilaian_id' => 1,
            'name' => 'Buruk Sekali',
            'bobot' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('penilaians')->insert([
            'id' => 2,
            'kategori_penilaian_id' => 1,
            'name' => 'Buruk',
            'bobot' => 25,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('penilaians')->insert([
            'id' => 3,
            'kategori_penilaian_id' => 1,
            'name' => 'Cukup',
            'bobot' => 50,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('penilaians')->insert([
            'id' => 4,
            'kategori_penilaian_id' => 1,
            'name' => 'Baik',
            'bobot' => 75,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('penilaians')->insert([
            'id' => 5,
            'kategori_penilaian_id' => 1,
            'name' => 'Baik',
            'bobot' => 100,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('penilaians')->insert([
            'id' => 6,
            'kategori_penilaian_id' => 2,
            'name' => 'Buruk Sekali',
            'bobot' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('penilaians')->insert([
            'id' => 7,
            'kategori_penilaian_id' => 2,
            'name' => 'Buruk',
            'bobot' => 25,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('penilaians')->insert([
            'id' => 8,
            'kategori_penilaian_id' => 2,
            'name' => 'Cukup',
            'bobot' => 50,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('penilaians')->insert([
            'id' => 9,
            'kategori_penilaian_id' => 2,
            'name' => 'Baik',
            'bobot' => 75,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('penilaians')->insert([
            'id' => 10,
            'kategori_penilaian_id' => 2,
            'name' => 'Baik',
            'bobot' => 100,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        */

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
