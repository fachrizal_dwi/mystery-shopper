<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(ProvinsisTableSeeder::class);
        $this->call(KotaKabupatensTableSeeder::class);
        $this->call(KecamatansTableSeeder::class);
        $this->call(KelurahansTableSeeder::class);
        $this->call(KategoriInstansisTableSeeder::class);
        $this->call(InstansisTableSeeder::class);
        $this->call(LocusesTableSeeder::class);
    }
}
