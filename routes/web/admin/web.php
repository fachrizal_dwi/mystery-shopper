<?php
Auth::routes();
Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () {
    Route::resource('survey', 'SurveyController');
    Route::get('/survey/{survey_id}/pertanyaan', 'PertanyaanController@index')->name('pertanyaan.index');
    Route::get('/survey/{survey_id}/pertanyaan/create/', 'PertanyaanController@create')->name('pertanyaan.create');
    Route::resource('pertanyaan', 'PertanyaanController',['except' => ['index', 'create']]);
    Route::resource('surveyor-approval', 'SurveyorApprovalController');
    Route::get('/result-instansi/{survey_id}/{instansi_id}', 'ResultInstansiController@show')->name('result-instansi.show');
    Route::resource('result-instansi', 'ResultInstansiController', ['except' => ['show']]);
    Route::get('/result-locus/{survey_id}/{instansi_id}/{locus_id}', 'ResultLocusController@show')->name('result-locus.show');
    Route::get('/map-result-locus/{survey_locus_id}/{locus_id}', 'ResultLocusController@map')->name('result-locus.map');
    Route::resource('result-locus', 'ResultLocusController', ['except' => ['show']]);
    Route::resource('dashboard', 'DashboardController');
    Route::resource('kategori-pertanyaan', 'KategoriPertanyaanController');
    Route::resource('user', 'UserController');
    Route::resource('landing-page', 'LandingPageController');
    Route::resource('how-to', 'HowToPageController');
});
