<?php
Auth::routes();
Route::name('admin.data.')->group(function () {
    Route::get('admin/data/surveys', 'Admin\SurveyController@dataTable')->name('survey');
    Route::get('admin/data/test', 'Admin\SurveyController@test')->name('test');
    Route::get('admin/data/pertanyaans', 'Admin\PertanyaanController@dataTable')->name('pertanyaan');
    Route::get('admin/data/surveyor-approval', 'Admin\SurveyorApprovalController@dataTable')->name('surveyor-approval');
    Route::get('admin/data/result-instansi', 'Admin\ResultInstansiController@dataTable')->name('result-instansi');
    Route::get('admin/data/result-locus/{survey_id}/{instansi_id}', 'Admin\ResultLocusController@dataTable')->name('result-locus');
    Route::get('admin/data/result-locus/detail/{survey_id}/{locus_id}', 'Admin\ResultLocusController@dataDetail')->name('result-locus.details');
    Route::get('admin/data/kategori-pertanyaan', 'Admin\KategoriPertanyaanController@dataTable')->name('kategori-pertanyaan');
    Route::get('admin/data/users', 'Admin\UserController@dataTable')->name('user');
});
