<?php
Auth::routes();
Route::name('surveyor.data.')->group(function () {
    Route::post('surveyor/data/kota', 'Surveyor\WilayahController@getKota')->name('kota');
    Route::post('surveyor/data/kecamatan', 'Surveyor\WilayahController@getKecamatan')->name('kecamatan');
    Route::post('surveyor/data/kelurahan', 'Surveyor\WilayahController@getKelurahan')->name('kelurahan');
    Route::post('surveyor/data/locus', 'Surveyor\WilayahController@getLocus')->name('locus');
    Route::get('surveyor/data/survey', 'Surveyor\SurveyController@dataTable')->name('survey');
    Route::get('surveyor/data/lokasi', 'Surveyor\LocusController@dataTable')->name('lokasi');
});
