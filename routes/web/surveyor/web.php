<?php
Auth::routes();
Route::resource('/', 'LandingController');
Route::resource('how-to', 'HowToController');
Route::namespace('Surveyor')->prefix('surveyor')->name('surveyor.')->group(function () {
    Route::resource('profile', 'ProfileController');
    Route::resource('survey', 'SurveyController');
    Route::resource('lokasi', 'LocusController');
    Route::match(['put', 'patch'], '/surveyor/lokasi/add-photo/{id}','LocusController@aplut')->name('lokasi.addPhoto');
    Route::match(['put', 'patch'], '/surveyor/lokasi/finish/{id}','LocusController@finish')->name('lokasi.finish');
});
