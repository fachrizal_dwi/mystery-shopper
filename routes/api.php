<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api.', 'namespace' => 'Api'], function () {
    /*
     * Outlets Endpoints
     */
    Route::get('cektoken', 'LoginController@cektoken')->name('login.cektoken');
    Route::post('registrasi', 'LoginController@registrasi')->name('login.registrasi');
    Route::post('registrasiUser', 'LoginController@registrasiUser')->name('login.registrasiUser');
    Route::post('login', 'LoginController@login')->name('login.login');
    Route::post('changePassword', 'LoginController@changePassword')->name('login.changePassword');
    Route::post('getDataProfileByUserId', 'ProfileController@getDataProfileByUserId')->name('profile.getDataProfileByUserId');
    Route::post('getDataUserByUserId', 'ProfileController@getDataUserByUserId')->name('profile.getDataUserByUserId');
    Route::post('insertProfile', 'ProfileController@insert')->name('profile.insert');
    Route::post('updatePhotoKtp', 'ProfileController@updatePhotoKtp')->name('profile.updatePhotoKtp');
    Route::post('updatePhotoProfile', 'ProfileController@updatePhotoProfile')->name('profile.updatePhotoProfile');
    Route::post('updatePhotoSelfie', 'ProfileController@updatePhotoSelfie')->name('profile.updatePhotoSelfie');
    Route::post('updatePhotoLegal', 'ProfileController@updatePhotoLegal')->name('profile.updatePhotoLegal');
    Route::post('updateProfile', 'ProfileController@updateProfile')->name('profile.updateProfile');
    Route::post('verifikasiRegistrasi', 'VerifikasiRegistrasiController@index')->name('verifikasi.index');
    Route::post('hasilSurvey', 'ApplySurveyController@hasilSurvey')->name('hasilSurvey');
    Route::name('provinsi.')->group(function () {
        Route::prefix('provinsi')->group(function () {
            Route::get('getData', 'MasterProvinsiController@getDataAll')->name('getDataAll');
            Route::post('getDataById', 'MasterProvinsiController@getDataById')->name('getDataById');
            Route::post('saveProvinsi', 'MasterProvinsiController@saveProvinsi')->name('saveProvinsi');
            Route::post('updateProvinsi', 'MasterProvinsiController@updateProvinsi')->name('updateProvinsi');
            Route::post('deleteProvinsi', 'MasterProvinsiController@deleteProvinsi')->name('deleteProvinsi');
        });
    });
    Route::name('kotaKabupaten.')->group(function () {
        Route::prefix('kotaKabupaten')->group(function () {
            Route::get('all', 'MasterKotaDanKabupatenController@all')->name('all');
            Route::post('getData', 'MasterKotaDanKabupatenController@getDataAll')->name('getDataAll');
            Route::post('getDataById', 'MasterKotaDanKabupatenController@getDataById')->name('getDataById');
            Route::post('saveKotaKabupaten', 'MasterKotaDanKabupatenController@saveKotaKabupaten')->name('saveKotaKabupaten');
            Route::post('updateKotaKabupaten', 'MasterKotaDanKabupatenController@updateKotaKabupaten')->name('updateKotaKabupaten');
            Route::post('deleteKotaKabupaten', 'MasterKotaDanKabupatenController@deleteKotaKabupaten')->name('deleteKotaKabupaten');
        });
    });
    Route::name('kecamatan.')->group(function () {
        Route::prefix('kecamatan')->group(function () {
            Route::get('all', 'MasterKecamatanController@all')->name('all');
            Route::post('getData', 'MasterKecamatanController@getDataAll')->name('getDataAll');
            Route::post('getDataById', 'MasterKecamatanController@getDataById')->name('getDataById');
            Route::post('saveKecamatan', 'MasterKecamatanController@saveKecamatan')->name('saveKecamatan');
            Route::post('updateKecamatan', 'MasterKecamatanController@updateKecamatan')->name('updateKecamatan');
            Route::post('deleteKecamatan', 'MasterKecamatanController@deleteKecamatan')->name('deleteKecamatan');
        });
    });
    Route::name('kelurahan.')->group(function () {
        Route::prefix('kelurahan')->group(function () {
            Route::get('all', 'MasterKelurahanController@all')->name('all');
            Route::post('getData', 'MasterKelurahanController@getDataAll')->name('getDataAll');
            Route::post('getDataById', 'MasterKelurahanController@getDataById')->name('getDataById');
            Route::post('saveKelurahan', 'MasterKelurahanController@saveKelurahan')->name('saveKelurahan');
            Route::post('updateKelurahan', 'MasterKelurahanController@updateKelurahan')->name('updateKelurahan');
            Route::post('deleteKelurahan', 'MasterKelurahanController@deleteKelurahan')->name('deleteKelurahan');
        });
    });
    Route::name('locus.')->group(function () {
        Route::prefix('locus')->group(function () {
            Route::get('all', 'MasterLocusController@all')->name('all');
            Route::get('getData', 'MasterLocusController@getDataAll')->name('getDataAll');
            Route::post('getDataById', 'MasterLocusController@getDataById')->name('getDataById');
            Route::post('saveLocus', 'MasterLocusController@saveLocus')->name('saveLocus');
            Route::post('updateLocus', 'MasterLocusController@updateLocus')->name('updateLocus');
            Route::post('deleteLocus', 'MasterLocusController@deleteLocus')->name('deleteLocus');
        });
    });
    Route::name('masterSurvey.')->group(function () {
        Route::prefix('masterSurvey')->group(function () {
            Route::get('getData', 'MasterSurveyController@getDataAll')->name('getDataAll');
            Route::get('getDataKategoriSurvey', 'MasterSurveyController@getDataKategoriSurvey')->name('getDataKategoriSurvey');
            Route::get('getDataSurveyStatus', 'MasterSurveyController@getDataSurveyStatus')->name('getDataSurveyStatus');
            Route::post('getDataById', 'MasterSurveyController@getDataById')->name('getDataById');
            Route::post('saveSurvey', 'MasterSurveyController@saveSurvey')->name('saveSurvey');
            Route::post('updateSurvey', 'MasterSurveyController@updateSurvey')->name('updateSurvey');
            Route::post('deleteSurvey', 'MasterSurveyController@deleteSurvey')->name('deleteSurvey');
        });
    });

    Route::name('applySurvey.')->group(function () {
        Route::prefix('applySurvey')->group(function () {
            Route::get('getData', 'ApplySurveyController@getDataAll')->name('getDataAll');
            Route::post('getDataById', 'ApplySurveyController@getDataById')->name('getDataById');
            Route::post('saveSurveyApplicant', 'ApplySurveyController@saveSurveyApplicant')->name('saveSurveyApplicant');
            Route::post('updateSurveyApplicant', 'ApplySurveyController@updateSurveyApplicant')->name('updateSurveyApplicant');
            Route::post('deleteSurveyApplicant', 'ApplySurveyController@deleteSurveyApplicant')->name('deleteSurveyApplicant');
        });
    });
    Route::name('survey.')->group(function () {
        Route::prefix('survey')->group(function () {
            Route::get('getData', 'SurveyController@index')->name('index');
            Route::get('available', 'SurveyController@available')->name('available');
            // Route::post('getDataById', 'ApplySurveyController@getDataById')->name('getDataById');
        });
    });

    Route::name('universitas.')->group(function () {
        Route::prefix('universitas')->group(function () {
            Route::get('getData', 'MasterUniversitasController@getDataAll')->name('getDataAll');
            Route::get('getDataByProvince', 'MasterUniversitasController@getDataByProvince')->name('getDataByProvince');
            Route::post('getDataById', 'MasterUniversitasController@getDataById')->name('getDataById');
            Route::post('saveUniversitas', 'MasterUniversitasController@saveUniversitas')->name('saveUniversitas');
            Route::post('updateUniversitas', 'MasterUniversitasController@updateUniversitas')->name('updateUniversitas');
            Route::post('deleteUniversitas', 'MasterUniversitasController@deleteUniversitas')->name('deleteUniversitas');
        });
    });

    Route::name('instansi.')->group(function () {
        Route::prefix('instansi')->group(function () {
            Route::get('getData', 'MasterIntansiController@getDataAll')->name('getDataAll');
            Route::get('all', 'MasterIntansiController@all')->name('all');
            Route::post('getDataById', 'MasterIntansiController@getDataById')->name('getDataById');
        });
    });

    Route::name('konfigurasiSurvey.')->group(function () {
        Route::prefix('konfigurasiSurvey')->group(function () {
            Route::get('getData', 'KonfigurasiSurveySurveyController@getDataAll')->name('getDataAll');
            Route::post('getDataById', 'KonfigurasiSurveySurveyController@getDataById')->name('getDataById');
        });
    });

    Route::name('surveyLocus.')->group(function () {
        Route::prefix('surveyLocus')->group(function () {
            Route::get('getData', 'SurveyLocusController@getData')->name('getData');
            Route::post('/', 'SurveyLocusController@store')->name('store');
            Route::post('/done', 'SurveyLocusController@done')->name('done');
            Route::delete('/{id}', 'SurveyLocusController@delete')->name('delete');
        });
    });

    Route::name('surveyLocusDocument.')->group(function () {
        Route::prefix('surveyLocusDocument')->group(function () {
            Route::get('getData', 'SurveyLocusDocumentController@getData')->name('getData');
            Route::post('/', 'SurveyLocusDocumentController@store')->name('store');
            Route::put('/', 'SurveyLocusDocumentController@update')->name('update');
            Route::delete('/{id}', 'SurveyLocusDocumentController@delete')->name('delete');
        });
    });


    Route::name('surveyLocusPenilaian.')->group(function () {
        Route::prefix('surveyLocusPenilaian')->group(function () {
            Route::get('getData', 'SurveyLocusPenilaianController@getData')->name('getData');
            Route::post('/', 'SurveyLocusPenilaianController@store')->name('store');
            Route::delete('/', 'SurveyLocusPenilaianController@delete')->name('delete');
        });
    });

    Route::name('pertanyaan.')->group(function () {
        Route::prefix('pertanyaan')->group(function () {
            Route::get('/', 'PertanyaanController@getData')->name('all');
            Route::get('/grouped', 'PertanyaanController@grouped')->name('grouped');
            Route::get('/{id}', 'PertanyaanController@find')->name('find');
        });
    });

    Route::name('static.')->group(function () {
        Route::prefix('static')->group(function () {
            Route::get('/get', 'StorageController@get')->name('get');
            Route::get('/read', 'StorageController@read')->name('read');
        });
    });

    Route::name('jenisPertanyaan.')->group(function () {
        Route::prefix('jenisPertanyaan')->group(function () {
            Route::get('all', 'JenisPertanyaanController@all')->name('all');
        });
    });

    Route::name('kategoriPertanyaan.')->group(function () {
        Route::prefix('kategoriPertanyaan')->group(function () {
            Route::get('all', 'KategoriPertanyaanController@all')->name('all');
        });
    });

    Route::name('kategoriSurvey.')->group(function () {
        Route::prefix('kategoriSurvey')->group(function () {
            Route::get('all', 'KategoriSurveyController@all')->name('all');
        });
    });

});
