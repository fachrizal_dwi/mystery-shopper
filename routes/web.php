<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes(['verify' => true]);
require 'web/admin/web.php';
require 'web/admin/data.php';
require 'web/surveyor/web.php';
require 'web/surveyor/data.php';

Route::get('/home', 'HomeController@index')->name('home');
Route::name('front.')->group(function () {
    Route::name('survey.')->group(function () {
        Route::name('penugasan.')->group(function () {
            Route::get('/front/survey/penugasan', function () {return view('front.survey.penugasan.index');})->name('index');
            Route::get('/front/survey/penugasan/form', function () {return view('front.survey.penugasan.form');})->name('form');
        });
    });
});
