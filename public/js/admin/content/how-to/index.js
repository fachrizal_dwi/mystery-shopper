(function ($) {
    "use strict";
    toastr.options = {
        "debug": false,
        "newestOnTop": true,
        "positionClass": "toast-top-right",
        "progressBar": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "2500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear"
    };
})(jQuery);

$('body').on('click', '.modal-show', function (event) {
    event.preventDefault();
    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title');

    $('#modal-title').text(title);
    $('#modal-btn-save').show()
    .text(me.hasClass('edit') ? 'Update' : 'Create');

    $.ajax({
        url: url,
        dataType: 'html',
        success: function (response) {
            $('#modal-body').html(response);
        }
    });

    $('#modal').modal('show');
});

$('#modal-btn-save').click(function (event) {
    event.preventDefault();
    if(!event.preventDefault()){
        $('#summernote').val($('#summernote').summernote('code'))
        var form = $('#modal-body form'),
            url = form.attr('action'),
            type = 'POST';
        form.find('.help-block').remove();
        form.find('.form-group').removeClass('has-error');
        $.ajax({
            url : url,
            type: type,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : new FormData(form[0]),
            contentType : false,
            processData : false,
            cache : false,
            success: function (response) {
                form.trigger('reset');
                toastr.success('Data tersimpan');
                $('#modal').modal('hide');
                location.reload();
            },
            error: function (xhr) {
                var res = xhr.responseJSON;
                if ($.isEmptyObject(res) == false) {
                    $.each(res.errors, function (key, value) {
                        $('#' + key)
                            .closest('.form-group')
                            .addClass('has-error')
                            .append('<span class="help-block text-danger">' + value + '</span>');
                    });
                    toastr.info('Lengkapi isian terlebih dahulu');
                }
            }
        });
        return false;
    }
});

$('body').on('click', '.btn-delete', function (event) {
    event.preventDefault();
    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'),
        csrf_token = $('meta[name="csrf-token"]').attr('content');

    swal({
        title: 'Hapus ' + title + ' ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    '_method': 'DELETE',
                    '_token': csrf_token
                },
                success: function (response) {
                    toastr.success('Data Dhapus');
                },
                error: function (xhr) {
                    toastr.error('Error - Terdapat kesalahan');
                }
            });
        }
    });
});

$('body').on('click', '.btn-show', function (event) {
    event.preventDefault();
    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title');

    $('#modal-title').text(title);
    $('#modal-btn-save').hide();


    $.ajax({
        url: url,
        dataType: 'html',
        success: function (response) {
            $('#modal-body').html(response);
        }
    });

    $('#modal').modal('show');
});
