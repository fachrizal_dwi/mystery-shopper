(function ($) {
    "use strict";
    toastr.options = {
        "debug": false,
        "newestOnTop": true,
        "positionClass": "toast-top-right",
        "progressBar": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "2500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear"
    };
})(jQuery);

$('#modal-btn-save').click(function (event) {
    event.preventDefault();
    if(!event.preventDefault()){
        var form = $('#form-profile'),
            url = form.attr('action'),
            type = 'POST';
        form.find('.help-block').remove();
        form.find('.form-group').removeClass('has-error');
        $.ajax({
            url : url,
            type: type,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : new FormData(form[0]),
            contentType : false,
            processData : false,
            cache : false,
            success: function (response) {
                form.trigger('reset');
                toastr.success('Data tersimpan');
                window.location.href = window.HomeUrl;
            },
            error: function (xhr) {
                var res = xhr.responseJSON;
                if ($.isEmptyObject(res) == false) {
                    $.each(res.errors, function (key, value) {
                        $('#' + key)
                            .closest('.form-group')
                            .addClass('has-error')
                            .append('<span class="help-block text-danger">' + value + '</span>');
                    });
                    toastr.warning('Lengkapi isian terlebih dahulu');
                }
            }
        });
        return false;
    }
});
