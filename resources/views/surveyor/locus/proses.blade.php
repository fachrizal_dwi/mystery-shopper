@extends('layouts.default')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/bentang.css')}}"/>
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Quisioner</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">Surveyor</a></li>
                    <li class="breadcrumb-item active">Quisioner</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    @include('surveyor.locus._modal')
    <div class="row row-eq-height">
        <div class="col-12 col-lg-2 mt-3 todo-menu-bar flip-menu pr-lg-0">
            <a href="#" class="d-inline-block d-lg-none mt-1 flip-menu-close"><i class="icon-close"></i></a>
            <div class="card border h-100 contact-menu-section">
                <ul class="nav flex-column contact-menu">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" data-contacttype="contact">
                            <i class="icon-list"></i> All Kategori
                        </a>
                    </li>
                    @foreach($kategori_pertanyaan as $kategori)
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-contacttype="contact-{{ $kategori->kategoriPertanyaan->id }}">
                                <i class="icon-people"></i> {{ $kategori->kategoriPertanyaan->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-12 col-lg-10 mt-3 pl-lg-0">
            <div class="card border h-100 contact-list-section">
                <div class="card-header border-bottom p-1 d-flex">
                    <a href="#" class="d-inline-block d-lg-none flip-menu-toggle"><i class="icon-menu"></i></a>
                    <input type="text" class="form-control border-0 p-2 w-100 h-100 contact-search" placeholder="Search ...">
                    <div class="search-bar-menu">
                        {!! Form::open(['url' => route('surveyor.lokasi.addPhoto', $survey_locus->id), 'method' => 'put', 'id' => 'form-photo']) !!}
                        {!! Form::hidden('survey_locus_id', $survey_locus->id) !!}
                        {!! Form::hidden('longitude', '', ['id' => 'longitude']) !!}
                        {!! Form::hidden('latitude', '', ['id' => 'latitude']) !!}
                        <label for="file" class="file-upload btn btn-outline-primary btn-block rounded-pill shadow"><i class="fa fa-camera"></i><input id="file" type="file" name="path">
                        </label>
                        {!! Form::close() !!}
                    </div>
                    <a href="#" class="list-style search-bar-menu border-0"><i class="icon-list"></i></a>
                    <a href="#" class="grid-style search-bar-menu active"><i class="icon-grid"></i></a>
                </div>
                <div class="card-body p-0">
                    <div class="contacts grid">
{{--                        START PERTANYAAN --}}
                        @foreach($pertanyaan as $pertanyaan)
                        <div class="contact contact-{{$pertanyaan->kategori_pertanyaan_id}}" style="display: block; animation: 7s ease 0s 1 normal none running fadein;">
                            <div class="contact-content">
                                <div class="contact-profile">
                                    <img src="{{ asset($pertanyaan->logo)}}" alt="avatar" class="user-image img-fluid" style=" max-height: 30px;">
                                    <div class="contact-info">
                                        <p class="contact-name mb-0">{{ $pertanyaan->kategoriPertanyaan->name }}</p>
                                        <p class="contact-position mb-0 small font-weight-bold text-muted">{{ $pertanyaan->name }}</p>
                                    </div>
                                </div>
{{--                                START JAWABAN--}}
                                {!! Form::open(['url' => route('surveyor.lokasi.update', $survey_locus->id), 'method' => 'put']) !!}
                                <input type="hidden" name="survey_locus_id" value="{{ $survey_locus->id }}">
                                <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaan->id }}">
                                @if ($pertanyaan->jenis_pertanyaan_id == 3)
                                    <div class="contact-email rating">
                                @endif
                                @foreach($pertanyaan->penilaian as $penilaian)
                                    <input type="hidden" name="jenis_pertanyaan_id" value="{{ $pertanyaan->jenis_pertanyaan_id }}">

                                    @if ($pertanyaan->jenis_pertanyaan_id == 1)
                                        <div class="contact-email">
                                            <p class="user-email">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="{{ $penilaian->id }}" name="penilaian_id" required="" value="{{ $penilaian->id }}">
                                                    <label class="custom-control-label" for="{{ $penilaian->id }}">{{ $penilaian->name }}</label>
                                                </div>
                                            </p>
                                        </div>
                                    @elseif ($pertanyaan->jenis_pertanyaan_id == 2)
                                        <div class="contact-email">
                                            <p class="user-email">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="{{ $penilaian->id }}" name="penilaian_id[]" value="{{ $penilaian->id }}">
                                                <label class="custom-control-label" for="{{ $penilaian->id }}">{{ $penilaian->name }}</label>
                                            </div>
                                            </p>
                                        </div>
                                    @else
                                        <input type="radio" id="star{{ $penilaian->id }}" name="penilaian_id" value="{{ $penilaian->id }}" /><label for="star{{ $penilaian->id }}" title="{{ $penilaian->name }}"></label>
                                    @endif
                                @endforeach
                                @if ($pertanyaan->jenis_pertanyaan_id == 3)
                                    </div>
                                @endif
{{--                                END JAWABAN--}}
                                <div class="line-h-1 text-center">
                                    <button type="submit" class="btn btn-outline-primary btn-sm">Submit</button></>
                                </div>
                            {!! Form::close() !!}

                            </div>
                        </div>

                        @endforeach
{{--                        END PERTANYAAN--}}
                    </div>

                </div>
            </div>
        </div>
    </div>
        <div class="row-cols-1 text-right">
            <div class="card card-body">
                <div class="row">
                    <div class="col text-left">
                        <a href="{{ route('surveyor.lokasi.show', $survey_locus->id) }}" class="btn btn-info pull-left btn-show" title="Gallery">View Result</a>
                    </div>
                    <div class="col">
                        @if ($qty_pertanyaans == 0)
                            {!! Form::open(['url' => route('surveyor.lokasi.finish', $survey_locus->id), 'method' => 'put']) !!}
                            <input type="hidden" name="status_survey_locus" value="1">
                            <button type="submit" class="btn btn-primary text-white pull-right">Selesai</button>
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('scripts')
    <script>

        $('body').on('click', '.btn-show', function (event) {
            event.preventDefault();
            var me = $(this),
                url = me.attr('href'),
                title = me.attr('title');

            $('#modal-title').text(title);
            $('#modal-btn-save').hide();


            $.ajax({
                url: url,
                dataType: 'html',
                success: function (response) {
                    $('#modal-body').html(response);
                }
            });

            $('#modal').modal('show');
        });


        $('body').on('click', '.btn-map', function (event) {
            event.preventDefault();
            var me = $(this),
                url = me.attr('href'),
                title = me.attr('title');
            $.ajax({
                url: url,
                dataType: 'html',
                success: function (response) {
                    $('#modal-body-map').html(response);
                }
            });
            $('#modalMap').modal('show');
        });
    </script>
    <script>
        $( document ).ready(function() {
            navigator.geolocation.getCurrentPosition(function(position) {
                $("#latitude").val(position.coords.latitude);
                $("#longitude").val(position.coords.longitude);
            });
        });

        (function ($) {
            "use strict";
            toastr.options = {
                "debug": false,
                "newestOnTop": true,
                "positionClass": "toast-top-right",
                "progressBar": true,
                "showDuration": "300",
                "hideDuration": "500",
                "timeOut": "2000",
                "extendedTimeOut": "500",
                "showEasing": "swing",
                "hideEasing": "linear"
            };
        })(jQuery);
    </script>
    <script src="{{ asset('dist/js/app.contactlist.js')}}"></script>
    <script>
        $(document).ready(function(){
            $(document).on('change', '#file', function(){
                var name = document.getElementById("file").files[0].name;
                var form_data = new FormData();
                var ext = name.split('.').pop().toLowerCase();
                if(jQuery.inArray(ext, ['gif','png','jpg','jpeg', 'svg']) == -1)
                {
                    toastr.warning('Hanya ekstensi .gif, .png, .svg, .jpg, dan .jpeg yang dapat dikirim');
                }
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("file").files[0]);
                var f = document.getElementById("file").files[0];
                var fsize = f.size||f.fileSize;
                if(fsize > 10000000)
                {
                    toastr.warning('Ukuran file di bawah 8 Mb yang dapat diterima');
                }
                else
                {
                    form_data.append("file", document.getElementById('file').files[0]);
                    var form = $('#form-photo'),
                        url = form.attr('action'),
                        type = 'POST';
                    $.ajax({
                        url: url,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type: type,
                        data: new FormData(form[0]),
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend:function(){
                            toastr.info('uploading ...');
                        },
                        success:function(data)
                        {
                            toastr.success('Data tersimpan');
                        },
                        error: function (xhr) {
                            var res = xhr.responseJSON;
                            if ($.isEmptyObject(res) == false) {
                                $.each(res.errors, function (key, value) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: key + ' ' + value,
                                        footer: '<a href>Why do I have this issue?</a>'
                                    });
                                });
                                toastr.info('Terdapat kesalahan');
                            }
                        }
                    });
                }
            });
        });
    </script>
@endpush
