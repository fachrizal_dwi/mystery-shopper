@extends('layouts.default')

@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2-bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/datatable/css/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Lokasi Survey</h4></div>
                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item">Surveyor</a></li>
                    <li class="breadcrumb-item active">List Lokasi Survey</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12  mt-3">
            <div class="card overflow-hidden">
                <div class="card-header text-right">
                    <a href="{{ route('surveyor.lokasi.create') }}" class="btn btn-primary pull-right modal-show" style="margin-top: -8px;" title="Tambah Lokasi"><i class="icon-plus"></i> Tambah Lokasi </a>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable" class="table layout-primary bordered" style="font-size:12px !important;">
                                <thead>
                                <tr>
                                    <th scope="col">Nama Kegiatan</th>
                                    <th scope="col">Instansi</th>
                                    <th scope="col">Lokasi</th>
                                    <th scope="col">Tanggal Akhir</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('surveyor.locus._modal')
@push('scripts')
    <script src="{{ asset('dist/vendors/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/select2/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{ asset('dist/js/gallery.script.js')}}"></script>
    <script src="{{ asset('dist/js/axios/dist/axios.min.js') }}" defer></script>
    <script>
        window.LocusesUrl = "{{ route('surveyor.lokasi.index') }}";
        var oTable = $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            cache: false,
            ajax: "{{ route('surveyor.data.lokasi') }}",
            columns: [
                {data: 'survey_name', name: 'surveys.name'},
                {data: 'instansi_name', name: 'instansis.name'},
                {data: 'locus_name', name: 'locuses.name'},
                {data: 'until', name: 'surveys.until'},
                {data: 'action', name: 'action', orderable: false}
            ]
        });
        function reload(){
            oTable.draw();
        };
    </script>
    <script src="{{ asset('js/surveyor/locus/index.js') }}"></script>
@endpush
