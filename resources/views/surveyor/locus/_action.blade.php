<a href="{{ $url_show }}" class="btn btn-info btn-show" title="{{ $model->name }}"><i class="icon-eye" ></i></a>
@if (empty($model->status_survey_locus))
    <a href="{{ $url_edit }}" class="btn btn-primary" title="Edit {{ $model->name }}">Proses</a>
@endif
<a href="{{ $url_destroy }}" class="btn btn-danger btn-delete" title="{{ $model->name. ' di ' .$model->locus_name }}"><i class="icon-trash text-white"></i></a>

