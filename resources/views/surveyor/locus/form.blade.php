{!! Form::model($model, [
    'route' => $model->exists ? ['surveyor.lokasi.update', $model->id] : 'surveyor.lokasi.store',
    'method' => $model->exists ? 'PATCH' : 'POST',
    'enctype' => $model->exists ?  'application/x-www-form-urlencoded' : "multipart/form-data"
]) !!}

<div class="row">
    <div class="col">
        <div class="form-group">
            {!! Form::label('survey_id', 'Survey', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::select('survey_id', $survey, null , ['class' => 'form-control select-untag']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="form-group">
            {!! Form::label('instansi_id', 'Instansi', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::select('instansi_id', $instansi, null , ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            {!! Form::label('locus_id', 'Lokasi', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::select('locus_id', [], null , ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


{!! Form::close() !!}

<script>
    $(".select-untag").select2({
        placeholder : 'Pilih Salah Satu'
    });

    $(function () {
        $('#instansi_id').on('click', function () {
            axios.post('{{ route('surveyor.data.locus') }}', {id: $(this).val()})
                .then(function (response) {
                    $('#locus_id').empty();
                    $.each(response.data, function (id, name) {
                        $('#locus_id').append(new Option(name, id))
                    })
                });
        });
    });
</script>
