<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel10" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel10">Nina</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div id="grid" class="row">
                    <div class="item col-12 col-md-6 col-lg-4 mb-4 text-center">
                        <img src="{{ asset('dist/images/portfolio1.jpg')}}" alt="" class="portfolioImage img-fluid">
                        <div class="d-flex">
                            <a data-fancybox-group="gallery" href="{{ asset('dist/images/portfolio1.jpg')}}" class="fancybox btn rounded-0 btn-dark w-100">Lihat KTP</a>
                        </div>

                    </div>
                    <div class="item col-12 col-md-6 col-lg-4 mb-4 cation-hover text-center Designing">
                        <div class="modImage">
                            <img src="{{ asset('dist/images/portfolio2.jpg')}}" alt="" class="portfolioImage img-fluid">
                            <div class="d-flex">
                                <a data-fancybox-group="gallery" href="{{ asset('dist/images/portfolio2.jpg')}}" class="fancybox btn rounded-0 btn-dark w-100">Lihat KTM</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Nama</th>
                            <td>Nina</td>
                        </tr>
                        <tr>
                            <th scope="row">Universitas</th>
                            <td>Universitas C</td>
                        </tr>
                        <tr>
                            <th scope="row">NIM</th>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Provinsi</th>
                            <td>Jawa Timur</td>
                        </tr>
                        <tr>
                            <th scope="row">Kota</th>
                            <td>Malang</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success text-white" data-dismiss="modal">Approve</button>
                <button type="button" class="btn btn-warning text-white" data-dismiss="modal">Reject</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
