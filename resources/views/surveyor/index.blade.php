@extends('layouts.admin')
@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/tablesaw/tablesaw.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Pendaftaran Surveyor</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">Surveyor</a></li>
                    <li class="breadcrumb-item"><a href="#">List</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header  justify-content-between align-items-center">
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="tablesaw table-bordered tablesaw-stack" data-tablesaw="" id="tablesaw-4234">
                            <thead>
                            <tr>
                                <th scope="col">Nama</th>
                                <th scope="col">Universitas</th>
                                <th scope="col">Kota</th>
                                <th scope="col">Provinsi</th>
                                <th scope="col">#</th>
                            </tr>
                            </thead>
                            <tbody id="checkall-target">
                            <tr>
                                <td class="title"> Nurdin</td>
                                <td>Universitas A</td>
                                <td>Serang</td>
                                <td>Banten</td>
                                <td><a class="btn btn-success text-white">Approved</a></td>
                            </tr>
                            <tr>
                                <td class="title"> Yanto</td>
                                <td>Universitas B</td>
                                <td>Pekanbaru</td>
                                <td>Riau</td>
                                <td><a class="btn btn-warning text-white">Rejected</a></td>
                            </tr>
                            <tr>
                                <td class="title"> Nina</td>
                                <td>Universitas C</td>
                                <td>Malang</td>
                                <td>Jawa Timur</td>
                                <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Detail</button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('surveyor._modal')
@push('scripts')
    <script src="{{ asset('dist/vendors/tablesaw/tablesaw.js') }}"></script>
    <script src="{{ asset('dist/vendors/tablesaw/tablesaw-init.js')}}"></script>
    <script src="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{ asset('dist/js/gallery.script.js')}}"></script>
@endpush
