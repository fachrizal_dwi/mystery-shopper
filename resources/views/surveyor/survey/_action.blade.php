<a href="{{ $url_show }}" class="btn btn-info btn-show" title="{{ $model->name }}"><i class="icon-eye" ></i></a>
@if($model->survey_applicant_statuse_name == null)
<a href="{{ $url_apply }}" class="btn btn-primary btn-apply" title="{{ $model->name }}"> Ikuti Survey</a>
@endif
