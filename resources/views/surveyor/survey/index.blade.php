@extends('layouts.default')

@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2-bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/datatable/css/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Lokasi Penugasan</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item">Surveyor</a></li>
                    <li class="breadcrumb-item active">List Lokasi</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12  mt-3">
            <div class="card overflow-hidden">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable" class="table layout-primary bordered" style="font-size:12px !important;">
                                <thead>
                                <tr>
                                    <th scope="col">Nama Kegiatan</th>
                                    <th scope="col">Status Kepesertaan</th>
                                    <th scope="col">Tanggal Mulai</th>
                                    <th scope="col">Tanggal Berakhir</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts._modal')
@push('scripts')
    <script src="{{ asset('dist/vendors/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/select2/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{ asset('dist/js/gallery.script.js')}}"></script>

    <script>
        window.LocusesUrl = "{{ route('surveyor.lokasi.index') }}";
        var oTable = $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            cache: false,
            ajax: "{{ route('surveyor.data.survey') }}",
            columns: [
                {data: 'name', name: 'name'},
                {data: 'survey_applicant_statuse_name', name: 'survey_applicant_statuses.name'},
                {data: 'from', name: 'from'},
                {data: 'until', name: 'until'},
                {data: 'action', name: 'action', orderable: false}
            ]
        });
        function reload(){
            oTable.draw();
        };
    </script>
    <script src="{{ asset('js/surveyor/survey/index.js') }}"></script>
@endpush
