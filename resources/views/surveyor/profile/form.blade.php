@extends('layouts.default')
@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2-bootstrap.min.css')}}"/>
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Profile</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="position-relative">
                <div class="background-image-maker py-5" style="background-image: url("{{ asset('dist/images/portfolio13.jpg')}}");"></div>
                <div class="holder-image">
                    <img src="{{ asset('dist/images/portfolio13.jpg')}}" alt="" class="img-fluid d-none">
                </div>
                <div class="position-relative px-3 py-5">
                    <div class="media d-md-flex d-block">
                        <a href="#"><img src="{{ asset('dist/images/contact-3.jpg')}}" alt="" class="img-fluid rounded-circle" width="100"></a>
                        <div class="media-body z-index-1">
                            <div class="pl-4">
                                <h1 class="display-4 text-uppercase text-white mb-0">{{--John Deo--}}</h1>
                                <h6 class="text-uppercase text-white mb-0">{{--Nama Kampus--}}</h6>
                                <h6 class="text-uppercase text-white mb-0">{{--Kota--}}</h6>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3 text-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 col-lg-12 mt-3">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Data Diri</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 modal-body">
                                            {!! Form::model($model, [
                                                'route' => $model->exists ? ['surveyor.profile.update', $model->id] : 'surveyor.profile.store',
                                                'method' => $model->exists ? 'PATCH' : 'POST',
                                                'enctype' => $model->exists ?  'application/x-www-form-urlencoded' : "multipart/form-data",
                                                'id' => "form-profile"
                                            ]) !!}
                                            <div class="form-group row">
                                                {!! Form::label('nama_lengkap', 'Nama Lengkap', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('nama_lengkap', null , ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('nik', 'NIK', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('nik', null , ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('universitas_id', 'Universitas', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::select('universitas_id', $universitas, $model->universitas_id ?? '', ['class' => 'form-control select-tag']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('nim', 'NIM', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('nim', null , ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('phone', 'No. Kontak', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('phone', null , ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('emergency_call', 'Kontak Darurat', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('emergency_call', null , ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('tanggal_lahir', 'Tanggal Lahir', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::date('tanggal_lahir', null,['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('tempat_lahir', 'Tempat Lahir', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::text('tempat_lahir', null , ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="card-header">
                                                <h4 class="card-title">Alamat Tinggal</h4>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('provinsi_id', 'Provinsi', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::select('provinsi_id', $provinsi, $model->provinsi_id ?? '', ['class' => 'form-control select-untag']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('kota_kabupaten_id', 'Kota / Kabupaten', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::select('kota_kabupaten_id', $kota, $model->kota_kabupaten_id ?? '', ['class' => 'form-control select-untag']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('kecamatan_id', 'Kecamatan', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::select('kecamatan_id', $kecamatan, $model->kecamatan_id ?? '', ['class' => 'form-control select-untag']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('kelurahan_id', 'Kelurahan', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::select('kelurahan_id', $kelurahan, $model->kelurahan_id ?? '', ['class' => 'form-control select-untag']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-2 col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::textarea('alamat', null , ['class' => 'form-control', 'rows' => '3']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                    <label for="photo_profile" class="file-upload btn btn-primary btn-block"><i class="fa fa-upload mr-2"></i>Foto Profil ...
                                                        {!! Form::file('photo_profile', ['id'=>'photo_profile']) !!}
                                                    </label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="photo_ktp" class="file-upload btn btn-primary btn-block"><i class="fa fa-upload mr-2"></i>KTP ...
                                                        {!! Form::file('photo_ktp', ['id'=>'photo_ktp']) !!}
                                                    </label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="photo_selfie" class="file-upload btn btn-primary btn-block"><i class="fa fa-upload mr-2"></i>Selfie dengan KTP ...
                                                        {!! Form::file('photo_selfie', ['id'=>'photo_selfie']) !!}
                                                    </label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="photo_legal" class="file-upload btn btn-primary btn-block"><i class="fa fa-upload mr-2"></i>Surat Penugasan ...
                                                        {!! Form::file('photo_legal', ['id'=>'photo_legal']) !!}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="display: none">
                                                <div class="col-sm-6">
                                                    {!! Form::text('longitude', null , ['class' => 'form-control text-longi']) !!}
                                                </div>
                                                <div class="col-sm-6">
                                                    {!! Form::text('latitude', null , ['class' => 'form-control text-lati']) !!}
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <label class="chkbox">Dengan ini saya menyatakan formulir saya isikan dengan sebenar benarnya
                                                        {!! Form::checkbox('is_agree','is_agree', (isset($model) && $model->is_agree == true ? true : false ), ['class' => 'form-check-input', 'onclick' => 'geom()', 'id'=> 'is_agree']) !!}
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group row">
                                                <div class="col-sm-10">
                                                    <a href="{{ route('surveyor.profile.index')}}" class="btn btn-outline-warning">Kembali</a>
                                                    <button type="button" class="btn btn-primary" id="modal-btn-save">Save changes</button>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('dist/vendors/select2/js/select2.full.min.js')}}"></script>
<script src="{{ asset('dist/js/axios/dist/axios.min.js') }}" defer></script>--}}
<script>
    window.HomeUrl = "{{ route('surveyor.profile.index') }}";
    $(document).ready(function() {
        navigator.geolocation.getCurrentPosition(function(position) {
            $(".text-lati").val(position.coords.latitude);
            $(".text-longi").val(position.coords.longitude);
        });
    });

    $(function () {
        $('#provinsi_id').on('click', function () {
            axios.post('{{ route('surveyor.data.kota') }}', {id: $(this).val()})
                .then(function (response) {
                    $('#kota_kabupaten_id').empty();
                    $.each(response.data, function (id, name) {
                        $('#kota_kabupaten_id').append(new Option(name, id))
                    })
                });
        });

        $('#kota_kabupaten_id').on('click', function () {
            axios.post('{{ route('surveyor.data.kecamatan') }}', {id: $(this).val()})
                .then(function (response) {
                    $('#kecamatan_id').empty();
                    $.each(response.data, function (id, name) {
                        $('#kecamatan_id').append(new Option(name, id))
                    })
                });
        });

        $('#kecamatan_id').on('click', function () {
            axios.post('{{ route('surveyor.data.kelurahan') }}', {id: $(this).val()})
                .then(function (response) {
                    $('#kelurahan_id').empty();
                    $.each(response.data, function (id, name) {
                        $('#kelurahan_id').append(new Option(name, id))
                    })
                });
        });
    });

    function geom(){
        navigator.geolocation.getCurrentPosition(function(position) {
            $(".text-lati").val(position.coords.latitude);
            $(".text-longi").val(position.coords.longitude);
        });
    }

    $(".select-tag").select2({
        placeholder : 'Masukan Data',
        tags: true
    });
    // $(".select-untag").select2({
    //     placeholder : 'Pilih Data'
    // });
    if ($('.select-tag').find("option[value='" + data.id + "']").length) {
        $('.select-tag').val(data.id).trigger('change');
    }else{
        var newOption = new Option(data.text, data.id, true, true);
        $('.select-tag').append(newOption).trigger('change');
    }
</script>
<script src="{{ asset('js/surveyor/profile/form.js') }}"></script>
@endpush
