<!DOCTYPE html>
<html lang="en">
<!-- START: Head-->
<head>
    <meta charset="UTF-8">
    <title>Indonesia Melayani</title>
    <link rel="shortcut icon" href="{{ asset('dist/images/favicon.ico') }}" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    {{-- CSRF TOKEN --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- START: Template CSS-->
    <link rel="stylesheet" href="{{ asset('dist/vendors/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/jquery-ui/jquery-ui.theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/weather-icons/css/pe-icon-set-weather.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/flags-icon/css/flag-icon.min.css') }}">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="{{ asset('dist/vendors/toastr/toastr.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/sweetalert2/sweetalert2.css')}}">
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="{{ asset('dist/css/main.css') }}">
@stack('styles')
<!-- END: Custom CSS-->
</head>
<!-- END Head-->

<!-- START: Body-->
@if(Auth::user()->user_type_id == 2)
    <body id="main-container" class="default semi-dark compact-menu">
@else
    <body id="main-container" class="default semi-dark">
@endif

<!-- START: Pre Loader-->
    <div class="se-pre-con">
        <div class="loader"></div>
    </div>
<!-- END: Pre Loader-->

<!-- START: Header-->
    <div id="header-fix" class="header fixed-top fixed">
        <div class="site-width">
            <nav class="navbar navbar-expand-lg  p-0">
                <div class="navbar-header  h-100 h4 mb-0 align-self-center logo-bar text-left">
                    <a href="{{route('index')}}" class="horizontal-logo text-left">
                        <svg height="20pt" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512" width="20pt" xmlns="http://www.w3.org/2000/svg">
                            <g transform="matrix(.1 0 0 -.1 0 512)" fill="#1e3d73">
                                <path d="m1450 4481-1105-638v-1283-1283l1106-638c1033-597 1139-654 1139-619 0 4-385 674-855 1489-470 814-855 1484-855 1488 0 8 1303 763 1418 822 175 89 413 166 585 190 114 16 299 13 408-5 100-17 231-60 314-102 310-156 569-509 651-887 23-105 23-331 0-432-53-240-177-460-366-651-174-175-277-247-738-512-177-102-322-189-322-193s104-188 231-407l231-400 46 28c26 15 360 207 742 428l695 402v1282 1282l-1105 639c-608 351-1107 638-1110 638s-502-287-1110-638z"/><path d="m2833 3300c-82-12-190-48-282-95-73-36-637-358-648-369-3-3 580-1022 592-1034 5-5 596 338 673 391 100 69 220 197 260 280 82 167 76 324-19 507-95 184-233 291-411 320-70 11-89 11-165 0z"/>
                            </g>
                        </svg> <span class="h4 font-weight-bold align-self-center mb-0 ml-auto">IM</span>
                    </a>
                </div>
                @can('Stakeholder')
                    <div class="navbar">
                        <ul class="ml-auto p-0 m-0 list-unstyled d-flex top-icon">
                            <li class="d-inline-block align-self-center  d-block d-lg-none">
                                <a href="#" class="nav-link mobilesearch" data-toggle="dropdown" aria-expanded="false"><i class="icon-magnifier h4"></i>
                                </a>
                            </li>
                            <li class="dropdown user-profile align-self-center d-inline-block">
                                <a href="#" class="nav-link py-0" data-toggle="dropdown" aria-expanded="false">
                                    <div class="media">
                                        <img src="{{ asset('dist/images/author.jpg')}}" alt="" class="d-flex img-fluid rounded-circle" width="29">
                                    </div>
                                </a>
                                <div class="dropdown-menu border dropdown-menu-right p-0">
                                    <div class="dropdown-divider"></div>
                                    <a href="{{ route('logout') }}" class="dropdown-item px-2 text-danger align-self-center d-flex" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                        <span class="icon-logout mr-2 h6  mb-0"></span> Sign Out</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>

                            </li>

                        </ul>
                    </div>
                @endcan
                @canany(['SuperAdmin','Verifikator'])
                    <div class="navbar-header h4 mb-0 text-center h-100 collapse-menu-bar">
                        <a href="#" class="sidebarCollapse" id="collapse"><i class="icon-menu"></i></a>
                    </div>
                @endcanany
            </nav>
        </div>
    </div>
<!-- END: Header-->
@canany(['SuperAdmin','Verifikator'])
<!-- START: Main Menu-->
    <div class="sidebar">
        <div class="site-width">
            @if (Auth::check() and Auth::user()->hasVerifiedEmail() == 1)
                <ul id="side-menu" class="sidebar-menu">
                    @canany(['SuperAdmin','Verifikator'])
                    <li class="dropdown active"><a href="#">Welcome</a>
                        <ul>
                            <li class="dropdown"><a href="#"><i class="icon-user"></i>{{ Auth::user()->name }}</a>
                                <ul class="sub-menu" style="display: none;">
                                    <li><a href="{{ route('logout') }}" class="dropdown-item px-2 text-danger align-self-center d-flex" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="icon-logout mr-2 h6  mb-0"></span> Sign Out</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    @endcanany
                    <li class="dropdown active"><a href="#"><i class="icon-home mr-1"></i> Dashboard</a>
                        <ul>
                            <li><a href="{{ route('admin.dashboard.index') }}"><i class="icon-rocket"></i> Peta Infografis</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><i class="icon-layers mr-1"></i> User Management</a>
                        <ul>
                            <li><a href="{{ route('admin.surveyor-approval.index') }}"><i class="icon-user"></i> Surveyor Approval
                                @if ($unapprove > 0)
                                        <span class="badge badge-primary">{{ $unapprove }}</span>
                                @endif
                                </a></li>
                            <li><a href="{{ route('admin.user.index') }}"><i class="icon-user"></i> Dashboard Account</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><i class="icon-layers mr-1"></i> Survey</a>
                        <ul>
                            <li><a href="{{ route('admin.survey.index') }}"><i class="icon-folder"></i> List Survey
                                    @php
                                        $admin_survey_qty = (App\Survey::where('until', '<=' , now())->count()) - (App\Notification::where('user_id', '=' , Auth::id())->count());
                                    @endphp
                                    @if ($admin_survey_qty > 0)
                                        <span class="badge badge-primary">{{ $admin_survey_qty }}</span>
                                    @endif
                                </a></li>
                            <li><a href="{{ route('admin.result-instansi.index') }}"><i class="icon-folder"></i> Penilaian Survey</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><i class="icon-layers mr-1"></i> Content</a>
                        <ul>
                            <li><a href="{{ route('admin.landing-page.index') }}"><i class="icon-folder"></i> Landing Page</a></li>
                            <li><a href="{{ route('admin.how-to.index') }}"><i class="icon-folder"></i> Tata Cara Indonesia Menilai</a></li>
                        </ul>
                    </li>
                </ul>
            @endif
        <!-- START: Menu-->
            <!-- END: Menu-->
            @if(!Auth::check())
                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0 ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Masuk') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Daftar') }}</a>
                        </li>
                    @endif
                </ol>
            @endif
        </div>
    </div>
@endcanany
<!-- END: Main Menu-->

<!-- START: Main Content-->
<main>
    <div class="container-fluid site-width">
        @yield('breadcumb')
        @yield('content')
    </div>
</main>
<!-- END: Content-->
<!-- START: Footer-->
<footer class="site-footer">
    2020 © Indonesia Melayani
</footer>
<!-- END: Footer-->



<!-- START: Back to top-->
<a href="#" class="scrollup text-center">
    <i class="icon-arrow-up"></i>
</a>
<!-- END: Back to top-->


<!-- START: Template JS-->
<script src="{{ asset('dist/vendors/jquery/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('dist/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('dist/vendors/moment/moment.js') }}"></script>
<script src="{{ asset('dist/vendors/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('dist/vendors/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- END: Template JS-->

<!-- START: APP JS-->
<script src="{{ asset('dist/js/app.js') }}"></script>
<!-- END: APP JS-->

<!-- START: Page JS-->
<script src="{{ asset('dist/vendors/toastr/toastr.min.js')}}"></script>
<script src="{{ asset('dist/vendors/sweetalert2/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('dist/js/home.script.js') }}"></script>
<!-- END: Page JS-->

@stack('scripts')
</body>
<!-- END: Body-->
</html>
