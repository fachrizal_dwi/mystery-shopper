<!DOCTYPE html>
<html lang="en">
<!-- START: Head-->
<head>
    <meta charset="UTF-8">
    <title>Indonesia Melayani</title>
    <link rel="shortcut icon" href="{{ asset('dist/images/favicon.ico') }}" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    {{-- CSRF TOKEN --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- START: Template CSS-->
    <link rel="stylesheet" href="{{ asset('dist/vendors/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/jquery-ui/jquery-ui.theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/simple-line-icons/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/flags-icon/css/flag-icon.min.css') }}">
    <!-- END Template CSS-->

    <!-- START: Page CSS-->
    <link rel="stylesheet" href="{{ asset('dist/vendors/weather-icons/css/pe-icon-set-weather.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/toastr/toastr.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/sweetalert2/sweetalert2.css')}}">
    <!-- END: Page CSS-->

    <!-- START: Custom CSS-->
    <link rel="stylesheet" href="{{ asset('dist/css/main.css') }}">
@stack('styles')
    <!-- END: Custom CSS-->
</head>
<!-- END Head-->

<!-- START: Body-->
<body id="main-container" class="default horizontal-menu semi-dark">

<!-- START: Pre Loader-->
<div class="se-pre-con">
    <div class="loader"></div>
</div>
<!-- END: Pre Loader-->

<!-- START: Header-->
<div id="header-fix" class="header fixed-top">
    <div class="site-width">
        <nav class="navbar navbar-expand-lg  p-0">
            <div class="navbar-header  h-100 h4 mb-0 align-self-center logo-bar text-left">
                <a href="{{ route('index')}}" class="horizontal-logo text-left">
                    <svg height="20pt" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512" width="20pt" xmlns="http://www.w3.org/2000/svg">
                        <g transform="matrix(.1 0 0 -.1 0 512)" fill="#1e3d73">
                            <path d="m1450 4481-1105-638v-1283-1283l1106-638c1033-597 1139-654 1139-619 0 4-385 674-855 1489-470 814-855 1484-855 1488 0 8 1303 763 1418 822 175 89 413 166 585 190 114 16 299 13 408-5 100-17 231-60 314-102 310-156 569-509 651-887 23-105 23-331 0-432-53-240-177-460-366-651-174-175-277-247-738-512-177-102-322-189-322-193s104-188 231-407l231-400 46 28c26 15 360 207 742 428l695 402v1282 1282l-1105 639c-608 351-1107 638-1110 638s-502-287-1110-638z"/><path d="m2833 3300c-82-12-190-48-282-95-73-36-637-358-648-369-3-3 580-1022 592-1034 5-5 596 338 673 391 100 69 220 197 260 280 82 167 76 324-19 507-95 184-233 291-411 320-70 11-89 11-165 0z"/>
                        </g>
                    </svg> <span class="h4 font-weight-bold align-self-center mb-0 ml-auto">IM</span>
                </a>
            </div>
            <div class="navbar-header h4 mb-0 text-center h-100 collapse-menu-bar">
                <a href="#" class="sidebarCollapse" id="collapse"><i class="icon-menu"></i></a>
            </div>

            @if(Auth::check())
            <div class="navbar-right ml-auto h-100">
                <ul class="ml-auto p-0 m-0 list-unstyled d-flex top-icon h-100">
                    <li class="d-inline-block align-self-center  d-block d-lg-none">
                        <a href="#" class="nav-link mobilesearch" data-toggle="dropdown" aria-expanded="false"><i class="icon-magnifier h4"></i>
                        </a>
                    </li>
                    <li class="dropdown user-profile align-self-center d-inline-block">
                        <a href="#" class="nav-link py-0" data-toggle="dropdown" aria-expanded="false">
                            <div class="media">
                                @php
                                    $foto = \App\UserProfile::select('photo_profile')->where('user_id', '=', Auth::user()->id)->first();
                                @endphp
                                @if (empty($foto))
                                    <img src="{{ asset('dist/images/author.jpg')}}" alt="" class="d-flex img-fluid rounded-circle" width="29">
                                @else
                                    <img src="{{ asset($foto->photo_profile)}}" alt="" class="d-flex img-fluid rounded-circle" width="29">
                                @endif
                            </div>
                        </a>
                        <div class="dropdown-menu border dropdown-menu-right p-0">
                            @if (Auth::user()->email_verified_at != null)
{{--                                <a href="{{ route('surveyor.profile.edit',Auth::user()->uid) }}" class="dropdown-item px-2 align-self-center d-flex">--}}
{{--                                    <span class="icon-pencil mr-2 h6 mb-0"></span> Edit Profile</a>--}}
                            @endif
                            <a href="{{ route('surveyor.profile.index') }}" class="dropdown-item px-2 align-self-center d-flex">
                                <span class="icon-user mr-2 h6 mb-0"></span> View Profile</a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('logout') }}" class="dropdown-item px-2 text-danger align-self-center d-flex" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <span class="icon-logout mr-2 h6  mb-0"></span> Sign Out</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>

                    </li>

                </ul>
            </div>
            @endif
        </nav>
    </div>
</div>
<!-- END: Header-->

<!-- START: Main Menu-->
<div class="sidebar">
    <div class="site-width">
        @if (Auth::check() and Auth::user()->hasVerifiedEmail() == 1)
            <ul id="side-menu" class="sidebar-menu">
                <li class="dropdown"><a href="#"><i class="icon-home mr-1"></i> Home</a>
                    <ul>
                        <li><a href="{{ route('index')}}"><i class="icon-rocket"></i> Selamat Datang</a></li>
                        <li><a href="{{ route('how-to.index')}}"><i class="icon-rocket"></i> Tata Cara Menilai</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href="#"><i class="icon-layers mr-1"></i> Kegiatan</a>
                    <ul>
                        <li><a href="{{route('surveyor.survey.index')}}"><i class="icon-folder"></i> List Kegiatan
                            @php
                                $surveyor_survey = (App\Survey::where('until', '>' , now())->count()) - (App\SurveyApplicant::where('user_id', '=' , Auth::id())->where('survey_applicant_status_id', '=', 1)->count());
                            @endphp
                            @if ($surveyor_survey > 0)
                                <span class="badge badge-primary">{{ $surveyor_survey }}</span>
                            @endif
                            </a>
                        </li>
                        <li><a href="{{route('surveyor.lokasi.index')}}"><i class="icon-folder"></i> Lokasi Kegiatan
                                @php
                                    $daftar_survey = App\SurveyApplicant::select('survey_id')->where('user_id', '=' , Auth::id())->where('survey_applicant_status_id', '=', 1)->get();
                                    $surveyor_list = App\Survey::whereIn('id', $daftar_survey)->whereDate('until', '<' , carbon\Carbon::today()->addDays(7))->count();
                                @endphp
                                @if ($surveyor_list > 0)
                                    <span class="badge badge-primary">{{ $surveyor_list }}</span>
                                @endif
                            </a></li>
                    </ul>
                </li>
            </ul>
        @endif
        <!-- START: Menu-->

        <!-- END: Menu-->
            @if(!Auth::check())
                <ul id="side-menu" class="sidebar-menu pull-right">
                    <li class="dropdown"><a href="#"><i class="icon-home mr-1"></i> Home</a>
                        <ul>
                            <li><a href="{{ route('index')}}"><i class="icon-rocket"></i> Selamat Datang</a></li>
                            <li><a href="{{ route('how-to.index')}}"><i class="icon-rocket"></i> Tata Cara Menilai</a></li>
                        </ul>
                        <ul>
                            <li><a href="{{ route('index')}}"><i class="icon-rocket"></i> Selamat Datang</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><i class="icon-home mr-1"></i> Akun</a>
                        <ul>
                            <li><a href="{{ route('login') }}"><i class="icon-user"></i> {{ __('Masuk') }}</a></li>
                            @if (Route::has('register'))
                                <li><a href="{{ route('register') }}"><i class="icon-user"></i> {{ __('Daftar') }}</a></li>
                            @endif
                        </ul>
                    </li>
                </ul>
            @endif
    </div>
</div>
<!-- END: Main Menu-->

<!-- START: Main Content-->
<main>
    <div class="container-fluid site-width">
        @yield('breadcumb')
        @yield('content')
    </div>
</main>
<!-- END: Content-->
<!-- START: Footer-->
<footer class="site-footer">
    2020 © Indonesia Melayani
</footer>
<!-- END: Footer-->



<!-- START: Back to top-->
<a href="#" class="scrollup text-center">
    <i class="icon-arrow-up"></i>
</a>
<!-- END: Back to top-->


<!-- START: Template JS-->
<script src="{{ asset('dist/vendors/jquery/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('dist/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('dist/vendors/moment/moment.js') }}"></script>
<script src="{{ asset('dist/vendors/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('dist/vendors/slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- END: Template JS-->

<!-- START: APP JS-->
<script src="{{ asset('dist/js/app.js') }}"></script>
<!-- END: APP JS-->

<!-- START: Page JS-->
<script src="{{ asset('dist/vendors/toastr/toastr.min.js')}}"></script>
<script src="{{ asset('dist/vendors/sweetalert2/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('dist/js/home.script.js') }}"></script>
<!-- END: Page JS-->

@stack('scripts')
</body>
<!-- END: Body-->
</html>
