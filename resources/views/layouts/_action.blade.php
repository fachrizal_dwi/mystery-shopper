<a href="{{ $url_show }}" class="btn btn-info btn-show" title="{{ $model->name }}"><i class="icon-eye" ></i></a>
<a href="{{ $url_edit }}" class="btn btn-info modal-show edit" title="Edit {{ $model->name }}"><i class="icon-pencil text-inverse"></i></a>
<a href="{{ $url_destroy }}" class="btn btn-danger btn-delete" title="{{ $model->name }}"><i class="icon-trash text-white"></i></a>

