@extends('layouts.default')

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Profile</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="position-relative">
                <div class="background-image-maker py-5" style="background-image: url("{{ asset('dist/images/portfolio13.jpg')}}");"></div>
                <div class="holder-image">
                    <img src="{{ asset('dist/images/portfolio13.jpg')}}" alt="" class="img-fluid d-none">
                </div>
                <div class="position-relative px-3 py-5">
                    <div class="media d-md-flex d-block">
                        <a href="#"><img src="{{ asset('dist/images/contact-3.jpg')}}" alt="" class="img-fluid rounded-circle" width="100"></a>
                        <div class="media-body z-index-1">
                            <div class="pl-4">
                                <h1 class="display-4 text-uppercase text-white mb-0">{{--John Deo--}}</h1>
                                <h6 class="text-uppercase text-white mb-0">{{--Nama Kampus--}}</h6>
                                <h6 class="text-uppercase text-white mb-0">{{--Kota--}}</h6>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Riwayat Survey</h4>
                </div>
                <div class="card-body p-0">
                    <ul class="nav flex-column chat-menu">
                        <li class="nav-item  px-3">
                            <h8>- Belum Ada Riwayat</h8>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xl-9">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 col-lg-12 mt-3">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Data Diri</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <form>
                                                <div class="form-group row">
                                                    <label for="username" class="col-sm-2 col-form-label">Nama</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="username" placeholder="Nama">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input type="email" class="form-control" id="email" placeholder="Email">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" class="col-sm-2 col-form-label">NIK</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputPassword3" placeholder="NIK">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2">Checkbox</div>
                                                    <div class="col-sm-10">

                                                        <label class="chkbox"> Example checkbox
                                                            <input name="gridCheck1" value="gridCheck1" class="form-check-input" type="checkbox">
                                                            <span class="checkmark"></span>
                                                        </label>

                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-10">
                                                        <button type="submit" class="btn btn-primary">Update Profile</button>
                                                        <a href="{{ route('front.user.profile.index')}}" class="btn btn-outline-warning">Kembali</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
