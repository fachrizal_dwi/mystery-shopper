@extends('layouts.default')

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Profile</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="position-relative">
                <div class="background-image-maker py-5" style="background-image: url("{{ asset('dist/images/portfolio13.jpg')}}");"></div>
                <div class="holder-image">
                    <img src="{{ asset('dist/images/portfolio13.jpg')}}" alt="" class="img-fluid d-none">
                </div>
                <div class="position-relative px-3 py-5">
                    <div class="media d-md-flex d-block">
                        <a href="#"><img src="{{ asset('dist/images/contact-3.jpg')}}" alt="" class="img-fluid rounded-circle" width="100"></a>
                        <div class="media-body z-index-1">
                            <div class="pl-4">
                                <h1 class="display-4 text-uppercase text-white mb-0">{{--John Deo--}}</h1>
                                <h6 class="text-uppercase text-white mb-0">{{--Nama Kampus--}}</h6>
                                <h6 class="text-uppercase text-white mb-0">{--Kota--}}</h6>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-xl-3">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Riwayat Survey</h4>
                </div>
                <div class="card-body p-0">
                    <ul class="list-group list-unstyled">
                        <li class="p-2 border-bottom zoom">
                            <div class="media d-flex w-100">
                                <span class="badge outline-badge-info my-auto">Selesai</span>
                                <div class="media-body align-self-center  text-right">
                                    <span class="mb-0 font-w-600">SURVEY KEPUASAN PENERIMA LAYANAN</span><br>
{{--                                    <p class="mb-0 font-w-500 tx-s-12">SIP Purchase</p>--}}
                                </div>
                                <div class="ml-auto my-auto pl-4 font-weight-bold  text-right text-success">
{{--                                    +500<br>--}}
{{--                                    <small class="d-block">USD</small>--}}
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xl-9">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 col-lg-12 mt-3">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Data Diri</h4>
                            </div>
                            <div class="card-content">
                                <div class="col-12  mt-3">
                                    <div class="card outline-badge-primary">
                                        <div class="card-content">
                                            <div class="card-body p-2">
                                                <div class="d-md-flex">
                                                    <p class="mb-0 my-auto font-w-500 tx-s-12">Akun anda belum terverifikasi, mohon lengkapi data anda dengan menekan link berikut.</p>
                                                    <div class="my-auto ml-auto">
                                                        <a href="{{ route('front.user.profile.edit') }}" class="btn btn-outline-primary font-w-600 my-auto text-nowrap"><i class="fas fa-external-link-alt"></i></a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table layout-primary bordered">
                                            <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th scope="row">Nama</th>
                                                <td>Nama Mahasiswa</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Nama Lengkap</th>
                                                <td>Nama Lengkap</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">e-mail</th>
                                                <td>surveyor@mail.com</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Status akun</th>
                                                <td><span class="badge badge-warning"><b>Belum Verifikasi</b></span></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">NIK</th>
                                                <td>3232 332 2222 21212 </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Nama Kampus</th>
                                                <td>Nama Kampus</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">NIM</th>
                                                <td>123456798</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">No. Kontak</th>
                                                <td>+62891234567891</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Kontak Darurat</th>
                                                <td>+62 6565265122</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Tempat Tanggal Lahir</th>
                                                <td>Kota, 1 Januari 2002</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Alamat</th>
                                                <td>Alamat Surveyor</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Provinsi</th>
                                                <td>Provinsi</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Kota / Kabupaten</th>
                                                <td>Kota / Kabupaten</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Kecamatan</th>
                                                <td>Kecamatan</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Kelurahan</th>
                                                <td>Kelurahan</td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
