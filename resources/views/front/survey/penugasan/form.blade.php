@extends('layouts.default')
@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/jquery-ui/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/jquery-ui/jquery-ui.theme.min.css')}}">
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Penugasan</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">Survey</a></li>
                    <li class="breadcrumb-item active"><a href="#">Penugasan</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12  mt-3">
            <div class="card overflow-hidden">
                <div class="card-header">
                    <h6>SURVEY KEPUASAN PENERIMA LAYANAN</h6>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-12 mt-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="wizard mb-4">
                                            <div class="connecting-line"></div>
                                            <ul class="nav nav-tabs d-flex mb-3">
                                                <li class="nav-item mr-auto">
                                                    <a class="nav-link position-relative round-tab text-left p-0 active border-0" data-toggle="tab" href="#id1">
                                                        <i class="icon-user position-relative text-white h5 mb-3"></i>
{{--                                                        <small class="d-none d-md-block ">Personal Information</small>--}}
                                                    </a>
                                                </li>
                                                <li class="nav-item mx-auto">
                                                    <a class="nav-link position-relative round-tab text-sm-center text-left p-0 border-0" data-toggle="tab" href="#id2">
                                                        <i class="icon-key position-relative text-white h5 mb-3"></i>
{{--                                                        <small class="d-none d-md-block">Password Information</small>--}}
                                                    </a>
                                                </li>
                                                <li class="nav-item ml-auto">
                                                    <a class="nav-link position-relative round-tab text-sm-right text-left p-0 border-0" data-toggle="tab" href="#id3">
                                                        <i class="icon-credit-card position-relative text-white h5 mb-3"></i>
{{--                                                        <small class="d-none d-md-block">Confirmation</small>--}}
                                                    </a>
                                                </li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane fade active show" id="id1">
                                                    <div class="form">
                                                        <h6>1. Prosedur pelayanan yang mudah dan tidak berbelit-belit</h6>
                                                        <hr>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline1" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline1">Buruk Sekali</label>
                                                        </div>
                                                        <!-- Default inline 2-->
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline2" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline2">Buruk</label>
                                                        </div>
                                                        <!-- Default inline 3-->
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline3" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline3">Cukup</label>
                                                        </div>
                                                        <!-- Default inline 4-->
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline4" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline4">Baik</label>
                                                        </div>
                                                        <!-- Default inline 5-->
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline5" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline5">Baik Sekali</label>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group">
                                                            <label class=" ">Catatan</label>
                                                            <textarea class="form-control bg-transparent"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-12 col-sm-6">
                                                                <label for="fileUpload6" class="file-upload btn btn-outline-primary btn-block rounded-pill shadow"><i class="fa fa-upload mr-2"></i>Tambahkan Foto<input id="fileUpload6" type="file">
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn float-right btn-primary nexttab">Next</button>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="id2">
                                                    <div class="form">
                                                        <h6>2. Petugas memenuhi pelayanan yang telah dijanjikan</h6>
                                                        <hr>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline6" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline6">Buruk Sekali</label>
                                                        </div>
                                                        <!-- Default inline 2-->
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline7" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline7">Buruk</label>
                                                        </div>
                                                        <!-- Default inline 3-->
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline8" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline8">Cukup</label>
                                                        </div>
                                                        <!-- Default inline 4-->
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline9" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline9">Baik</label>
                                                        </div>
                                                        <!-- Default inline 5-->
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" class="custom-control-input" id="defaultInline10" name="inlineDefaultRadiosExample">
                                                            <label class="custom-control-label" for="defaultInline10">Baik Sekali</label>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group">
                                                            <label class=" ">Catatan</label>
                                                            <textarea class="form-control bg-transparent"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-12 col-sm-6">
                                                                <label for="fileUpload6" class="file-upload btn btn-outline-primary btn-block rounded-pill shadow"><i class="fa fa-upload mr-2"></i>Tambahkan Foto<input id="fileUpload6" type="file">
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex">
                                                            <button type="button" class="btn btn-primary prevtab">Previous</button>
                                                            <button type="button" class="btn btn-primary nexttab ml-auto">Next</button>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="id3">
                                                    <div class="form p-5 text-center">
                                                        <h3>Thank you !</h3>
                                                        <p>Quisque nec turpis at urna dictum luctus. Suspendisse convallis dignissim eros at volutpat. In egestas mattis dui. Aliquam mattis dictum aliquet.</p>
{{--                                                        <button type="button" class="btn btn-primary prevtab">Previous</button>--}}
                                                        <a href="{{ route('front.survey.penugasan.index') }}" class="btn btn-primary">Selesai</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>

</script>
@endpush
