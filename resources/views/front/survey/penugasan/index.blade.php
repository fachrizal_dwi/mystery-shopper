@extends('layouts.default')

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Penugasan</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">Survey</a></li>
                    <li class="breadcrumb-item active"><a href="#">Penugasan</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12  mt-3">
            <div class="card overflow-hidden">
                <div class="card-header">
                    <h6>SURVEY KEPUASAN PENERIMA LAYANAN</h6>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table layout-primary bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Penempatan</th>
                                    <th scope="col">Kota</th>
                                    <th scope="col">Status Kegiatan</th>
                                    <th scope="col">Tanggal Survey</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Dinas PUPR Kota A</td>
                                    <td>Kota A</td>
                                    <td><span class="badge badge-primary">OPEN</span></td>
                                    <td>-</td>
                                    <th><a href="{{ route('front.survey.penugasan.form')}}" class="btn p-2 btn-primary mb-1 text-white" title="Nama Tempat">Klik untuk memulai</a></th>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Dinas Kependudukan dan Pencatatan Sipil</td>
                                    <td>Kota A</td>
                                    <td><span class="badge badge-success">CLOSED</span></td>
                                    <td>-</td>
                                    <th>&nbsp;</th>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Dinas PUPR</td>
                                    <td>Kota A</td>
                                    <td><span class="badge badge-success">CLOSED</span></td>
                                    <td>-</td>
                                    <th>&nbsp;</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $('body').on('click', '.btn-daftar', function (event) {
        event.preventDefault();
        var me = $(this),
            title = me.attr('title');
        swal({
            title: title,
            text: "Yakin akan mendaftar?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            setTimeout(function () {
                swal("Terimakasih sudah mendaftar,\n mohon menunggu verifikasi!");
            }, 1200);
        });
    });
</script>
@endpush
