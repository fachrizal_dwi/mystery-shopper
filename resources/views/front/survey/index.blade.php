@extends('layouts.default')

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Survey</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item active"><a href="#">Survey</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12  mt-3">
            <div class="card overflow-hidden">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table layout-primary bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama Kegiatan</th>
                                    <th scope="col">Status Kegiatan</th>
                                    <th scope="col">Mulai Tanggal</th>
                                    <th scope="col">Hingga Tanggal</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>SURVEY KEPUASAN PENERIMA LAYANAN</td>
                                    <td><span class="badge badge-primary">Dibuka Pendaftaran</span></td>
                                    <td>1 November 2020</td>
                                    <td>21 November 2020</td>
                                    <th><a class="badge p-2 badge-primary mb-1 text-white btn-daftar" title="SURVEY KEPUASAN PENERIMA LAYANAN">Daftar</a></th>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>SURVEY KINERJA LAYANAN</td>
                                    <td><span class="badge badge-info">Berjalan</span></td>
                                    <td>1 November 2020</td>
                                    <td>21 November 2020</td>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>SURVEY TATA TERTIB LAYANAN</td>
                                    <td><span class="badge badge-success">Selesai</span></td>
                                    <td>1 November 2020</td>
                                    <td>21 November 2020</td>
                                    <th></th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $('body').on('click', '.btn-daftar', function (event) {
        event.preventDefault();
        var me = $(this),
            title = me.attr('title');
        swal({
            title: title,
            text: "Yakin akan mendaftar?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            setTimeout(function () {
                swal("Terimakasih sudah mendaftar,\n mohon menunggu verifikasi!");
            }, 1200);
        });
    });
</script>
@endpush
