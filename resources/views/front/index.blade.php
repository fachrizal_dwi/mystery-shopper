@extends('layouts.default')

@section('breadcumb')

@endsection

@section('content')
    <div class="bdz-news-text-single" bis_skin_checked="1">
        <br>
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9" id="NewsCon" bis_skin_checked="1">
            <h1 class="bdz-news-title-single-with-detail">Indonesia Melayani</h1>
            <div class="bdz-news-date-single" bis_skin_checked="1">{{ now() }}</div>
            <p>
                <em>
                    <strong>
                        {{ $model->title ?? '' }}
                    </strong>
                </em>
            </p>
            <hr>
            <div class="row">
                <div class="col">
                    {!! $model->content ?? '' !!}
                </div>
            </div>
    </div>

@endsection
