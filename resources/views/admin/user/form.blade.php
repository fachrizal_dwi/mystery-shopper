{!! Form::model($model, [
    'route' => $model->exists ? ['admin.user.update', $model->id] : 'admin.user.store',
    'method' => $model->exists ? 'PATCH' : 'POST',
    'enctype' => $model->exists ?  'application/x-www-form-urlencoded' : "multipart/form-data"
]) !!}

<div class="form-group row">
    @if(!$model->exists)
        {!! Form::hidden('email_verified_at',\Carbon\Carbon::now()) !!}
    @endif
    {!! Form::label('name', 'Nama', ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('email', 'Email Akun', ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('user_type_id', 'Jenis Akun', ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('user_type_id', $userTypes, $model->user_type_id ?? '', ['class' => 'form-control select-type']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('password', 'Password', ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        {!! Form::password('password', null, ['class' => 'form-control', 'autocomplete' => 'new-password']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('password_confirmation', 'Password', ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        {!! Form::password('password_confirmation', null, ['class' => 'form-control', 'autocomplete' => 'new-password']) !!}
    </div>
</div>

{!! Form::close() !!}

<script>
    $(".select-type").select2({
        placeholder : 'Jenis Pengguna'
    });
</script>
