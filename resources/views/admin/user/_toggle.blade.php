@if ($model->email_verified_at == null)
    <?php
    $btn = 'btn-success';
    $title = 'Aktifkan ';
    $val = '1';
    $icon = 'icon-user-following';
    ?>
@else
    <?php
    $btn = 'btn-dark';
    $title = 'Non-Aktifkan ';
    $val = '0';
    $icon = 'icon-user-unfollow';
    ?>
@endif
<a href="{{ $url_toggle }}" class="btn {{$btn}} btn-toggle" title="{{ $title.$model->name }}" data-value="{{$val}}"><i class="{{$icon}} text-white"></i></a>

