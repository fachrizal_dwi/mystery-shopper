@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2-bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/datatable/css/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">List User</h4></div>
                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item active"><a href="#">List</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header  justify-content-between align-items-center">
                </div>
                <div class="card-body">
                    <div class="card-header text-right">
                        <a href="{{ route('admin.user.create') }}" class="btn btn-primary pull-right modal-show" style="margin-top: -8px;" title="Tambah User"><i class="icon-plus"></i> Tambah User </a>
                    </div>
                    <div class="table-responsive">
                        <hr>
                        <table id="datatable" class="table" style="font-size:12px !important;">
                            <thead>
                            <tr>
                                <th scope="col">Nama</th>
                                <th scope="col">Email Akun</th>
                                <th scope="col">Jenis Akun</th>
                                <th scope="col"></th>
                                <th scope="col">#</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts._modal')
@push('scripts')
    <script src="{{ asset('dist/vendors/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/select2/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{ asset('dist/js/gallery.script.js')}}"></script>

    <script>
        var oTable = $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            cache: false,
            ajax: "{{ route('admin.data.user') }}",
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'user_type_name', name: 'user_types.name'},
                {data: 'toggle', name: 'toggle', orderable: false},
                {data: 'action', name: 'action', orderable: false}
            ]
        });
        function reload(){
            oTable.draw();
        };
    </script>
    <script src="{{ asset('js/admin/user/index.js') }}"></script>
@endpush
