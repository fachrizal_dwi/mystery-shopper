<table class="table table-hover" style="font-size:12px !important;">
    <thead>
        <tr>
            <th scope="col">Kategori Survey</th>
            <th scope="col">Status</th>
            <th scope="col">Tanggal Mulai</th>
            <th scope="col">Tanggal Akhir</th>
            <th scope="col">Tahun</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $model->kategoriSurvey->name }}</td>
            <td>{{ $model->surveyStatus->name }}</td>
            <td>{{ $model->from }}</td>
            <td>{{ $model->until }}</td>
            <td>{{ $model->tahun }}</td>
        </tr>
    </tbody>
</table>
<hr>
<div class="card card-header"><h6>List Pertanyaan</h6></div>
<table class="table table-hover" style="font-size:12px !important;">
    <thead>
        <tr>
            <th scope="col">Pertanyaan</th>
            <th scope="col">Kategori</th>
            <th scope="col">Jenis</th>
            <th scope="col">Bobot</th>
            <th scope="col">alias</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        @foreach($pertanyaan as $tanya)
        <tr>
            <td><strong>{{ $tanya->name }}</strong></td>
            <td><strong>{{ $tanya->kategoriPertanyaan->name }}</strong></td>
            <td><strong>{{ $tanya->jenisPertanyaan->name }}</strong></td>
            <td><strong>{{ $tanya->bobot }}</strong></td>
            <td><strong>{{ $tanya->alias }}</strong></td>
            <td><img src="{{ asset($tanya->logo) }}" style=" max-height: 20px;" alt=""></td>
        </tr>
            @foreach($tanya->penilaian as $nilai)
                <tr>
                    @if ($tanya->jenis_pertanyaan_id == 1)
                        <td  class="text-right"><input type="radio" name="radio" id="{{ $nilai->id }}"></td>
                    @elseif ($tanya->jenis_pertanyaan_id == 2)
                        <td  class="text-right"><input type="checkbox" id="{{ $nilai->id }}"> </td>
                    @else
                        <td  class="text-right"><img src="{{ asset('dist/images/star.png') }}" style=" max-height: 20px;"></td>
                    @endif
                    <td colspan="2">{!! Form::label($nilai->id, $nilai->name, ['class' => 'col-sm col-form-label']) !!}</td>
                    <td colspan="2">{!! Form::label($nilai->id, $nilai->bobot, ['class' => 'col-sm col-form-label']) !!}</td>
                    <td>&nbsp;</td>
                </tr>
            @endforeach
            <thead>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </thead>
        @endforeach
    </tbody>
</table>

