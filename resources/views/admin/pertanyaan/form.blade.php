{!! Form::model($model, [
    'route' => $model->exists ? ['admin.pertanyaan.update', $model->id] : 'admin.pertanyaan.store',
    'method' => $model->exists ? 'PATCH' : 'POST',
    'enctype' => $model->exists ?  'application/x-www-form-urlencoded' : "multipart/form-data",
    'name' => "formpertanyaan"
]) !!}

<div class="form-group">
    <div class="form-row">
        <div class="form-group col-md-6">
            {!! Form::hidden('survey_id',$survey_id) !!}
            {!! Form::label('name', 'Pertanyaan', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-md-1">
            {!! Form::label('bobot', 'Bobot', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::text('bobot', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-md-2">
            {!! Form::label('alias', 'Alias', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::text('alias', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-md-2">
            {!! Form::label('kategori_pertanyaan_id', 'Kategori', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::select('kategori_pertanyaan_id', $kategori, $model->kategori_pertanyaan_id ?? '', ['class' => 'form-control select-kategori']) !!}
        </div>
        <div class="form-group col-md-1">
            {!! Form::label('jenis_pertanyaan_id', 'Jenis', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::select('jenis_pertanyaan_id', $jenis, $model->jenis_pertanyaan_id ?? '', ['class' => 'form-control select-jenis', 'onChange'=>'gantiTipe()']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="logo" class="file-upload btn btn-primary btn-block"><i class="fa fa-upload mr-2"></i>Upload Logo ...
                {!! Form::file('logo', ['id'=>'logo']) !!}
            </label>
        </div>
        <div id="upload_prev" class="form-group col-md-9 text-center">
            <img id="photoShowId" src="{{ $model->logo ? asset($model->logo) : null }}" style=" max-height: 100px;" />
        </div>

    </div>
</div>
<div class="card card-body">
    <div id="tablePenilaian" class="table-responsive">
        <table class="table" style="font-size:12px !important;">
            <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th>Bobot</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
            @if ($model->exists)
                @foreach($model->penilaian as $nilai)
                    <tr>
                        @if ($model->jenis_pertanyaan_id == 1)
                            <td style="max-width: 100px!important;"><input type="hidden" name="penilaian_id[]" value="{{ $nilai->id }}"><input type="radio" class="input-jenis" name="input-jenis"><img src="" style="max-height: 20px; display: none" id="imgJenis" class="gambar-jenis"></td>
                        @elseif ($model->jenis_pertanyaan_id == 2)
                            <td><input type="hidden" name="penilaian_id[]" value="{{ $nilai->id }}"><input type="checkbox" class="input-jenis" name="input-jenis"><img src="" style="max-height: 20px; display: none" id="imgJenis" class="gambar-jenis"></td>
                        @else
                            <td><input type="hidden" name="penilaian_id[]" value="{{ $nilai->id }}"><input type="radio" class="input-jenis" style="display: none" name="input-jenis"><img src="{{ asset('dist/images/star.png') }}" style="max-height: 20px;" id="imgJenis" class="gambar-jenis"></td>
                        @endif
                        <td><div class="form-group"><input type="text" id="penilaian_name.{{ $nilai->id }}" name="penilaian_name[]" placeholder="Teks Penilaian" class="form-control text-penilaian" {{ $model->jenis_pertanyaan_id == 3 ? 'readonly' : '' }} value="{{ $nilai->name }}" /><small>mandatory</small></div></td>
                        <td><div class="form-group"><input type="text" id="penilaian_bobot.{{ $nilai->id }}" name="penilaian_bobot[]" placeholder=" . . . " class="form-control text-bobot" {{ $model->jenis_pertanyaan_id == 3 ? 'readonly' : '' }} value="{{ $nilai->bobot }}" /><small>mandatory</small></div></td>
                        <td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>
                    </tr>
                @endforeach
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><button type="button" name="add" id="add" class="btn btn-success"><i class="icon-plus"></i> penilaian</button></td>
                </tr>
            @else
                <tr>
                    <td><input type="hidden" name="penilaian_id[]"><input type="radio" class="input-jenis" name="input-jenis"><img src="{{ asset('dist/images/radio.png') }}" style="max-height: 20px; display: none" id="imgJenis" class="gambar-jenis"></td>
                    <td><div class="form-group"><input type="text" id="penilaian_name.0" name="penilaian_name[]" placeholder="Teks Penilaian" class="form-control text-penilaian" /><small>mandatory</small></div></td>
                    <td><div class="form-group"><input type="text" id="penilaian_bobot.0" name="penilaian_bobot[]" placeholder=" . . . " class="form-control text-bobot" /><small>mandatory</small></div></td>
                    <td><button type="button" name="add" id="add" class="btn btn-success"><i class="icon-plus"></i> penilaian</button></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
{!! Form::close() !!}

<script>
    $(".select-jenis").select2({
        placeholder : 'Jenis Pertanyaan'
    });
    $(".select-kategori").select2({
        placeholder : 'Kategori',
        tags: true
    });
    //biar bisa dikirim value select na
    if ($('.select-kategori').find("option[value='" + data.id + "']").length) {
        $('.select-kategori').val(data.id).trigger('change');
    }else{
        var newOption = new Option(data.text, data.id, true, true);
        $('.select-kategori').append(newOption).trigger('change');
    }
</script>

<script type="text/javascript">
    function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#' + id).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo").change(function () {
        readURL(this, 'photoShowId');
    });
</script>

<script type="text/javascript">
    function gantiTipe() {
        let hinis = document.getElementById("jenis_pertanyaan_id").value;
        if(hinis == 1){
            $('.gambar-jenis').attr('style','display : none;');
            $('.input-jenis').attr('type','radio');
            $('.input-jenis').show();
            $('.text-penilaian').prop('readonly', false);
            $('.text-bobot').prop('readonly', false);
        }else if(hinis == 2){
            $('.gambar-jenis').attr('style','display : none;');
            $('.input-jenis').attr('type','checkbox');
            $('.input-jenis').show();
            $('.text-penilaian').prop('readonly', false);
            $('.text-bobot').prop('readonly', false);
        }else{
            $('.input-jenis').attr('style','display : none;');
            $('.gambar-jenis').attr('src','{{ asset('dist/images/star.png') }}');
            $('.gambar-jenis').attr('style','max-height: 20px;');
            $('.gambar-jenis').show();
            $('.text-penilaian').val("-");
            $('.text-penilaian').prop('readonly', true);
            $('.text-bobot').prop('readonly', true);
            hitungBobot();
        }

    }
    function hitungBobot()
    {
        let jml = document.querySelectorAll("[name='penilaian_bobot[]']").length;
        jml = (100 / jml).toFixed(2);
        $('.text-bobot').val(jml);
    }
</script>

<script type="text/javascript">
    var i = 0;
    $("#add").click(function(){
        i++;
        let hinis = document.getElementById("jenis_pertanyaan_id").value;
        if(hinis == 1){
            $("#tablePenilaian tbody").append(
                '<tr>' +
                '<td><input type="hidden" name="penilaian_id[]"><input type="radio" class="input-jenis" name="input-jenis"><img src="{{ asset('dist/images/star.png' ) }}" style=" max-height: 20px;  display :none;" id="imgJenis" class="gambar-jenis"></td>' +
                '<td><div class="form-group"><input type="text" id="penilaian_name.'+i+'" name="penilaian_name[]" placeholder="Teks Penilaian" class="form-control text-penilaian" /><small>mandatory</small></div></td>' +
                '<td><div class="form-group"><input type="text" id="penilaian_bobot.'+i+'" name="penilaian_bobot[]" placeholder=" . . . " class="form-control text-bobot" /><small>mandatory</small></div></td>' +
                '<td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>' +
                '</tr>');
        }else if(hinis == 2){
            $("#tablePenilaian tbody").append(
                '<tr>' +
                '<td><input type="hidden" name="penilaian_id[]"><input type="checkbox" class="input-jenis" name="input-jenis"><img src="{{ asset('dist/images/star.png' ) }}" style=" max-height: 20px; display :none;" id="imgJenis" class="gambar-jenis"></td>' +
                '<td><div class="form-group"><input type="text" id="penilaian_name.'+i+'" name="penilaian_name[]" placeholder="Teks Penilaian" class="form-control text-penilaian" /><small>mandatory</small></div></td>' +
                '<td><div class="form-group"><input type="text" id="penilaian_bobot.'+i+'" name="penilaian_bobot[]" placeholder=" . . . " class="form-control text-bobot" /> <small>mandatory</small></div></td>' +
                '<td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>' +
                '</tr>');
        }else{
            $("#tablePenilaian tbody").append(
                '<tr>' +
                '<td><input type="hidden" name="penilaian_id[]"><input type="radio" class="input-jenis" style="display:none;" name="input-jenis"><img src="{{ asset('dist/images/star.png') }}" style=" max-height: 20px;" id="imgJenis" class="gambar-jenis"></td>' +
                '<td><div class="form-group"><input type="text" id="penilaian_name.'+i+'" name="penilaian_name[]" placeholder="Teks Penilaian" class="form-control text-penilaian" value="-" readonly /><small>mandatory</small></div></td>' +
                '<td><div class="form-group"><input type="text" id="penilaian_bobot.'+i+'" name="penilaian_bobot[]" placeholder=" . . . " class="form-control text-bobot" readonly /><small>mandatory</small></div></td>' +
                '<td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>' +
                '</tr>');
            hitungBobot();
        }
    });

    $(document).on('click', '.remove-tr', function(){
        $(this).parents('tr').remove();
        let hinis = document.getElementById("jenis_pertanyaan_id").value;
        if(hinis == 3){
            hitungBobot();
        }
    });
</script>
