{!! Form::model($model, [
    'route' => $model->exists ? ['admin.kategori-pertanyaan.update', $model->id] : 'admin.kategori-pertanyaan.store',
    'method' => $model->exists ? 'PATCH' : 'POST',
    'enctype' => $model->exists ?  'application/x-www-form-urlencoded' : "multipart/form-data",
    'name' => "formKategoriPertanyaan"
]) !!}

<div class="form-group">
    <div class="form-row">
        <div class="form-group col-md">
            {!! Form::label('name', 'Nama Kategori', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="form-group col-md">
            {!! Form::label('alias', 'Alias', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::text('alias', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="form-group col-md">
            {!! Form::label('bobot_minimal', 'Bobot Minimal', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::text('bobot_minimal', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="logo" class="file-upload btn btn-primary btn-block"><i class="fa fa-upload mr-2"></i>Upload Logo ...
                {!! Form::file('logo', ['id'=>'logo']) !!}
            </label>
        </div>
        <div id="upload_prev" class="form-group col-md-9 text-center">
            <img id="photoShowId" src="{{ $model->logo ? asset($model->logo) : null }}" style=" max-height: 100px;" />
        </div>

    </div>
</div>
{!! Form::close() !!}

<script type="text/javascript">
    function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#' + id).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo").change(function () {
        readURL(this, 'photoShowId');
    });
</script>
