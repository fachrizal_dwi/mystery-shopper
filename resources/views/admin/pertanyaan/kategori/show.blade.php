<div class="card card-body">
    <table id="kategori-table" class="table table-hover" style="font-size:12px !important;">
        <thead>
        <tr>
            <th scope="col">Nama Kategori</th>
            <th scope="col">Alias</th>
            <th scope="col">Logo</th>
            <th scope="col">Bobot Minimal</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>


<script>
    var oTable = $('#kategori-table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        cache: false,
        ajax: "{{ route('admin.data.kategori-pertanyaan') }}",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'alias', name: 'alias'},
            {data: 'logo', name: 'logo'},
            {data: 'bobot_minimal', name: 'bobot_minimal'},
            {data: 'action', name: 'action', orderable: false}
        ]
    });
    function reload(){
        oTable.draw();
    };
</script>
