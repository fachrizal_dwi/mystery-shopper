@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2-bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/datatable/css/dataTables.bootstrap4.min.css')}}" />
    <style>
        td.details-control {
            background: url('{{ asset('uploads/images/dt').'/plus.png' }}') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('{{ asset('uploads/images/dt').'/minus.png' }}') no-repeat center center;
        }
    </style>
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"></div>
                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">Survey</a></li>
                    <li class="breadcrumb-item"><a href="#">List</a></li>
                    <li class="breadcrumb-item active"><a href="#">Pertanyaan</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header  justify-content-between align-items-center">
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm">
                                <div class="w-sm-100 mr-auto"><h6 class="mb-0">{{$survey->name}}</h6></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm text-right">
                                <a href="{{ route('admin.survey.index') }}" class="btn btn-info pull-right" style="margin-top: -8px;" title="Kembali ke halaman survey"> Kembali </a>
                                <a href="{{ route('admin.kategori-pertanyaan.index') }}" class="btn btn-dark pull-right btn-show text-white" style="margin-top: -8px;" title="List Kategori"><i class="icon-doc"></i> List Kategori </a>
                                <a href="{{ route('admin.kategori-pertanyaan.create') }}" class="btn btn-dark pull-right modal-show text-white" style="margin-top: -8px;" title="Tambah Kategori"><i class="icon-plus"></i> Tambah Kategori </a>
                                <a href="{{ route('admin.pertanyaan.create', $survey->id ) }}" class="btn btn-primary pull-right modal-show" style="margin-top: -8px;" title="Buat Pertanyaan"><i class="icon-plus"></i> Tambah Pertanyaan </a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="datatable" class="table" style="font-size:12px !important;">
                            <thead>
                            <tr>
                                <th scope="col">Pertanyaan</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Jenis</th>
                                <th scope="col">Bobot</th>
                                <th scope="col">Alias</th>
                                <th scope="col">Logo</th>
                                <th scope="col">#</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts._modal')
@push('scripts')
    <script src="{{ asset('dist/vendors/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/select2/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{ asset('dist/js/gallery.script.js')}}"></script>
    <script>
        var oTable = $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            cache: false,
            ajax: {
                url : "{{ route('admin.data.pertanyaan') }}",
                type: 'GET',
                data: function (d) {
                    d.surveyId = {{ $survey->id ?? null }};
                }
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'kategori_pertanyaan.name', name: 'kategori_pertanyaan.name', searchable : false, orderable: false},
                {data: 'jenis', name: 'jenis', searchable : false, orderable: false},
                {data: 'bobot', name: 'bobot'},
                {data: 'alias', name: 'alias'},
                {data: 'logo', name: 'logo'},
                {data: 'action', name: 'action', orderable: false}
            ]
        });
        function reload(){
            oTable.draw();
        };
    </script>
    <script src="{{ asset('js/admin/pertanyaan/index.js') }}"></script>
@endpush
