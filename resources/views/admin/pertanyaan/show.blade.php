<div class="card card-body">
    <table class="table table-hover" style="font-size:12px !important;">
        <tr>
            <th scope="col">Kategori</th>
            <th scope="col">Jenis</th>
            <th scope="col">Bobot</th>
            <th scope="col">alias</th>
            <th scope="col"></th>
        </tr>
        <tr>
            <td>{{ $model->kategoriPertanyaan->name }}</td>
            <td>{{ $model->jenisPertanyaan->name }}</td>
            <td>{{ $model->bobot }}</td>
            <td>{{ $model->alias }}</td>
            <td><img src="{{ asset($model->logo) }}" style=" max-height: 50px;" alt="-"></td>
        </tr>
    </table>
</div>
<hr>
<div class="card card-body">
    <table class="table table-hover" style="font-size:12px !important;">
        <tr>
            <th scope="col"></th>
            <th scope="col">Penilaian</th>
            <th scope="col">Bobot</th>
        </tr>
        @foreach($model->penilaian as $penilaian)
        <tr>
            @if ($model->jenis_pertanyaan_id == 1)
                <td><input type="radio" name="radio" id="{{ $penilaian->id }}"></td>
            @elseif ($model->jenis_pertanyaan_id == 2)
                <td><input type="checkbox" name="checkbox" id="{{ $penilaian->id }}"></td>
            @else
                <td><img src="{{ asset('dist/images/star.png') }}" style=" max-height: 18px;"></td>
            @endif
            <td><label for="{{ $penilaian->id }}" class="col-sm col-form-label">{{ $penilaian->name }}</label></td>
            <td><label for="{{ $penilaian->id }}" class="col-sm col-form-label">{{ $penilaian->bobot }}</label></td>
        </tr>
        @endforeach
    </table>
</div>
