@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2-bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('dist/vendors/datatable/css/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Penilaian Survey</h4></div>
                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a>Survey</a></li>
                    <li class="breadcrumb-item active"><a >Penilaian</a></li>
                    <li class="breadcrumb-item active"><a >Instansi</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header  justify-content-between align-items-center">
                </div>
                <div class="card-body">
                    <div class="panel-body">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            <div class="form-group">
                                {!! Form::select('filterSurvey', $surveys, null, ['id' => 'filterSurvey', 'class' => 'form-control']) !!}
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <hr>
                        <table id="datatable" class="table" style="font-size:12px !important;">
                            <thead>
                            <tr>
                                <th scope="col">Survey</th>
                                <th scope="col">Instansi</th>
                                <th scope="col">Provinsi</th>
                                <th scope="col">Jumlah Kategori Pertanyaan</th>
                                <th scope="col">Jumlah Kategori Lulus</th>
                                <th scope="col">Persentase (%)</th>
                                <th scope="col">#</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('admin.result.instansi._modal')
@push('scripts')
    <script src="{{ asset('dist/vendors/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('show.bs.modal', '.modal', function (event) {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function() {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);
            });
        });
    </script>
    <script>
        var oTable = $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            cache: false,
            ajax: {
                url : "{{ route('admin.data.result-instansi') }}",
                type: 'GET',
                data: function (d) {
                    d.filterSurvey = $('#filterSurvey').val();
                }
            },
            columns: [
                {data: 'survey_name', name: 'surveys.name'},
                {data: 'instansi_name', name: 'instansis.name'},
                {data: 'provinsi_name', name: 'provinsis.name'},
                {data: 'kategori_pertanyaan_count', name: 'kategori_pertanyaan_count'},
                {data: 'result', name: 'result'},
                {data: 'percentage', name: 'percentage'},
                {data: 'action', name: 'action', orderable: false}
            ]
        });
        function reload(){
            oTable.draw();
        };
        $('#filterSurvey').on('change', function(e) {
            oTable.draw();
            e.preventDefault();
        });
        $('#filterSurvey').select2();
    </script>
    <script src="{{ asset('js/admin/result/index.js') }}"></script>
@endpush
