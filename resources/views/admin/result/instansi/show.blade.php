
<div class="card card-body">
    <div class="table-responsive">
        <hr>
        <table id="detailtable" class="table" style="font-size:12px !important;">
            <thead>
            <tr>
                <th scope="col">Lokus</th>
                <th scope="col">Kota / Kabupaten</th>
                <th scope="col">Jumlah Kategori Pertanyaan Tersedia</th>
                <th scope="col">Jumlah Kategori Pertanyaan Lulus</th>
                <th scope="col">Jumlah Bobot Nilai</th>
                <th scope="col">#</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>


<script>
    var oTable = $('#detailtable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        cache: false,
        ajax: "{{ route('admin.data.result-locus',[$survey_id, $instansi_id]) }}",
        columns: [
            {data: 'locus_name', name: 'locuses.name'},
            {data: 'kota_kabupaten_name', name: 'kota_kabupatens.name'},
            {data: 'kategori_pertanyaan_count', name: 'kategori_pertanyaan_count'},
            {data: 'result', name: 'result'},
            {data: 'summary', name: 'summary'},
            {data: 'action', name: 'action', orderable: false}
        ]
    });
    function reload(){
        oTable.draw();
    };
</script>
