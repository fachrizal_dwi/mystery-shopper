<link rel="stylesheet" href="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.css')}}">
<script src="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.js')}}"></script>
<script src="{{ asset('dist/js/gallery.script.js')}}"></script>
<div class="card card-body">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <a href="{{ route('admin.result-instansi.show', [$survey_id ,$instansi_id]) }}" class="btn btn-info pull-right btn-show" style="margin-top: -8px;" title="{{ $survey_name->name .' - '.$instansi_name->name }}"><i class="icon-minus"></i> Kembali </a>
            </div>
            <div class="col text-right">
                <a href="{{ route('admin.result-locus.destroy', $survey_locus_id->id) }}" class="btn btn-danger btn-delete" title="{{ $locus->name }}"><i class="icon-trash text-white"></i> Reject Hasil Survey</a>
            </div>
        </div>

    </div>
    <div class="table-responsive">
        <hr>
        <table id="detaillocus" class="table" style="font-size:12px !important;">
            <thead>
            <tr>
                <th scope="col">Surveyor</th>
                <th scope="col">Pertanyaan</th>
                <th scope="col">Kategori</th>
                <th scope="col">Bobot Pertanyaan</th>
                <th scope="col">Bobot Penilaian</th>
                <th scope="col">Hasil Pembobotan</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col">
            <div class="card card-body">
                <div id="grid" class="row">
                @foreach($dokumentasis as $dokumentasi)
                    <div class="item col-12 col-md-6 col-lg-4 mb-4 cation-hover text-center">
                        <div class="modImage">
                            <img src="{{ asset($dokumentasi->path)}}" alt="" class="portfolioImage img-fluid" style="max-height: 200px;">
                            <div class="d-flex">
                                <a data-fancybox-group="gallery" href="{{ asset($dokumentasi->path)}}" class="fancybox btn rounded-0 btn-dark w-50">Lihat Foto</a>
                                <a href="{{ route('admin.result-locus.map', [$dokumentasi->id, $locus->id]) }}" class="btn btn-dark rounded-0 w-50 btn-map"><i class="fa fa-map-marker"> View Location</i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var oTable = $('#detaillocus').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        cache: false,
        ajax: "{{ route('admin.data.result-locus.details',[$survey_id, $locus->id]) }}",
        columns: [
            {data: 'surveyor_name', name: 'users.name'},
            {data: 'pertanyaan', name: 'pertanyaans.name'},
            {data: 'kategori', name: 'kategori_pertanyaans.name'},
            {data: 'bobot_pertanyaan', name: 'bobot_pertanyaan'},
            {data: 'bobot', name: 'bobot'},
            {data: 'nilai_pertanyaan', name: 'nilai_pertanyaan'}
            // {data: 'kota_kabupaten_name', name: 'kota_kabupatens.name'},
        ]
    });
    function reload(){
        oTable.draw();
    };
</script>
