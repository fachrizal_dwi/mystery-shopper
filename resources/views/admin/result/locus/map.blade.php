<link rel="stylesheet" href="{{ asset('leaflet/leaflet.css') }}">
<style type="text/css">
    #location{
        height:500px;
    }
</style>

<div class="row">
    <div class="col">
        <div class="card">
            <div id="location" class="card-body"></div>
        </div>
    </div>
</div>

<script src="{{ asset('leaflet/leaflet.js') }}"></script>
<script>
    var map = L.map('location').setView([{{ $model->latitude ?? '' }}, {{ $model->longitude ?? '' }}], 18);

    L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png').addTo(map);
    L.marker([{{ $model->latitude ?? '' }}, {{ $model->longitude ?? '' }}])
        .addTo(map)
        .bindPopup(
            '{{ $locus->name }}'
        ).openPopup();;
</script>
