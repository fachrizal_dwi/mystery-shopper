@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('leaflet/leaflet.css') }}">
    <style type="text/css">
        #peta{
            height:500px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('dist/vendors/icheck/skins/all.css')}}">
    <style>
        .instansis {
            column-count: 2;
            column-gap: 40px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('dist/vendors/datatable/css/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('breadcumb')
    <div class="row fade">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Peta Infografis</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a>Dashboard</a></li>
                </ol>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row row-eq-height">
        <div class="col-12 col-lg-2 mt-3 todo-menu-bar flip-menu pr-lg-0">
            <a href="#" class="d-inline-block d-lg-none mt-1 flip-menu-close"><i class="icon-close"></i></a>
            <div class="card border h-100 contact-menu-section bg-dark text-white">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <a class="bg-secondary py-2 px-2 rounded ml-auto text-white w-100 text-center">
                        <span class="d-none d-xl-inline-block">List Survey</span>
                    </a>
                </div>
                @if (isset($models))
                    <div class="row">
                        <div class="col-12">
                            @foreach($models as $id=>$name)
                                <div class="row">
                                    <div class="col">
                                        <a href="{{ route('admin.dashboard.show', $id) }}" class="btn btn-dark btn-block text-left"><i class="icon-list"></i> {{ $name }}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-12 col-lg-10 mt-3 pl-lg-0">
            <div class="card border h-100 contact-list-section">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-3 text-right">Legend: </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md text-center" style="background-color: #C2BFED; font-size: 10px"> Lulus</div>
                                            <div class="col-md text-center" style="background-color: #F5BFBA; font-size: 10px">Tidak Lulus</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div id="peta" class="card-body"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <div class="row">
                    <div class="col text-right">
                        <a class="btn btn-dark text-white" onclick="resetPeta()"><strong>Reset</strong></a>
                    </div>
                </div>
                <div class="instansis">
                    @foreach($instansis as $instansi)
                        <div class="row py-1">
                            <div class="col mx-auto">
                                <a class="btn btn-block btn-sm text-white" onclick="getInstansi({{$instansi->survey_id}},{{$instansi->kota_kabupaten_id}},{{$instansi->instansi_id}})" style="{{$instansi->style}};"><strong>{{ $instansi->instansi_name }}</strong></a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col">
                        <div id="divDatatable" class="table-responsive" style="display: none;">
                            <table id="tableLocus" class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama Lokus</th>
                                    <th scope="col">Kota / Kabupaten</th>
                                    <th scope="col">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('dist/vendors/icheck/icheck.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript">
        var mapOptions = {
            center: [-4.323435, 119.896298],
            zoom: 5.4
        }
        var peta = new L.map('peta', mapOptions);
        L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png').addTo(peta);
        var geojsonFeature = null;
        @if(!$shp_provinsis->isEmpty())
            @foreach($shp_provinsis as $shp)
                geojsonFeature = null;
                geojsonFeature = {!! $shp->geojson ?? '' !!};
                    L.geoJSON(geojsonFeature, {opacity: .01, color: '{{$shp->color}}', weight: 0, transparent: true})
                        .bindPopup('<table><thead><tr>' +
                            '<th colspan="2">{{ $shp->provinsi_name }}</th></tr></thead>' +
                            '<tbody>' +
                            '<tr><td>Jumlah Survey: </td><td>{{ $shp->qty_instansi }}</td></tr>' +
                            '<tr><td >Jumlah Instansi Lulus: </td><td>{{ $shp->qty_lulus }}</td></tr>' +
                            '</tbody></table>')
                        .addTo(peta);
            @endforeach
        @endif
        @foreach($shp_kota_kabupatens as $kota)
            @if(!empty($kota->geojson))
                geojsonFeature = null;
                geojsonFeature = {!! $kota->geojson !!};
                    L.geoJSON(geojsonFeature, {opacity: 0.3, color: '{{$kota->color}}'})
                        .bindPopup('<table><thead><tr>' +
                            '<th colspan="2">{{ $kota->kota_kabupaten_name }}</th></tr></thead>' +
                            '<tbody>' +
                            '<tr><td>Jumlah Survey Locus</td><td>{{ $kota->qty_locus }}</td></tr>' +
                            '<tr><td >Jumlah Lulus Survey</td><td>{{ $kota->qty_lulus }}</td></tr>' +
                            '</tbody></table>')
                        .addTo(peta);
            @endif
        @endforeach
    </script>

    <script>
        var oTable = $('#tableLocus').DataTable({
            dom: 'Bfrtip',
            responsive: true
        });
        function getInstansi(survey_id, kota_kabupaten_id, instansi_id)
        {
            peta.off();
            peta.remove();
            var mapOptions = {
                center: [-4.323435, 119.896298],
                zoom: 5.4
            }
            peta = new L.map('peta', mapOptions);
            L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png').addTo(peta);
            @foreach($shp_kota_kabupatens as $kota)
            if(kota_kabupaten_id == {{$kota->kota_kabupaten_id}}){
                geojsonFeature = null;
                geojsonFeature = {!! $kota->geojson !!};
                L.geoJSON(geojsonFeature, {opacity: 0.3, color: '{{$kota->color}}'})
                    .bindPopup('<table><thead><tr>' +
                        '<th colspan="2">{{ $kota->kota_kabupaten_name }}</th></tr></thead>' +
                        '<tbody>' +
                        '<tr><td>Jumlah Survey Locus</td><td>{{ $kota->qty_locus }}</td></tr>' +
                        '<tr><td >Jumlah Lulus Survey</td><td>{{ $kota->qty_lulus }}</td></tr>' +
                        '</tbody></table>')
                    .addTo(peta);
            }
            @endforeach
            oTable.clear().draw();
                 no = 0;
            // $('#tableLocus thead').empty();
            // $('#tableLocus thead').append('<tr><th scope="col">#</th><th scope="col">Nama Lokus</th><th scope="col">Kota / Kabupaten</th><th scope="col">Status</th></tr>');
            @foreach($locuses as $locus)
            if(instansi_id == {{$locus->instansi_id}}){
                no++;
                oTable.row.add( [
                    no,
                    '{{ $locus->name }}',
                    '{{ $locus->kota_kabupaten_name }}',
                    '<a class="{{ $locus->class }} text-white" style="{{$locus->style}};">{{ $locus->lulus }}</a>'
                ] ).draw( false );
            }
            @endforeach
            oTable.draw();
            $('#divDatatable').show();
        }
    </script>

    <script>
        /** reset peta */
        function resetPeta()
        {
            peta.off();
            peta.remove();
            var mapOptions = {
                center: [-4.323435, 119.896298],
                zoom: 5.4
            }
            peta = new L.map('peta', mapOptions);
            L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png').addTo(peta);
            @foreach($shp_provinsis as $shp)
                geojsonFeature = null;
            geojsonFeature = {!! $shp->geojson !!};
            L.geoJSON(geojsonFeature, {opacity: .01, color: '{{$shp->color}}', weight: 0, transparent: true})
                .bindPopup('<table><thead><tr>' +
                    '<th colspan="2">{{ $shp->provinsi_name }}</th></tr></thead>' +
                    '<tbody>' +
                    '<tr><td>Jumlah Survey: </td><td>{{ $shp->qty_instansi }}</td></tr>' +
                    '<tr><td >Jumlah Instansi Lulus: </td><td>{{ $shp->qty_lulus }}</td></tr>' +
                    '</tbody></table>')
                .addTo(peta);
            @endforeach

                @foreach($shp_kota_kabupatens as $kota)
                geojsonFeature = null;
            geojsonFeature = {!! $kota->geojson !!};
            L.geoJSON(geojsonFeature, {opacity: 0.3, color: '{{$kota->color}}'})
                .bindPopup('<table><thead><tr>' +
                    '<th colspan="2">{{ $kota->kota_kabupaten_name }}</th></tr></thead>' +
                    '<tbody>' +
                    '<tr><td>Jumlah Survey Locus</td><td>{{ $kota->qty_locus }}</td></tr>' +
                    '<tr><td >Jumlah Lulus Survey</td><td>{{ $kota->qty_lulus }}</td></tr>' +
                    '</tbody></table>')
                .addTo(peta);
            @endforeach
            var oTable = $('#tableLocus').DataTable();
            oTable.clear().draw();
            $('#divDatatable').hide();
        }
    </script>
@endpush
