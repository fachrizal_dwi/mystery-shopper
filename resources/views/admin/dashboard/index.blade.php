@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('leaflet/leaflet.css') }}">
    <style type="text/css">
        #peta{
            height:500px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('dist/vendors/icheck/skins/all.css')}}" >
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Peta Infografis</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a>Dashboard</a></li>
                </ol>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row row-eq-height">
        <div class="col-12 col-lg-2 mt-3 todo-menu-bar flip-menu pr-lg-0">
            <a href="#" class="d-inline-block d-lg-none mt-1 flip-menu-close"><i class="icon-close"></i></a>
            <div class="card border h-100 contact-menu-section bg-dark text-white">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <a class="bg-secondary py-2 px-2 rounded ml-auto text-white w-100 text-center">
                        <span class="d-none d-xl-inline-block">List Survey</span>
                    </a>
                </div>
                    @if (isset($models))
                    <div class="row">
                        <div class="col-12">
                            @foreach($models as $id=>$name)
                                <div class="row">
                                    <div class="col">
                                        <a href="{{ route('admin.dashboard.show', $id) }}" class="btn btn-dark btn-block text-left"><i class="icon-list"></i> {{ $name }}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
            </div>
        </div>
        <div class="col-12 col-lg-10 mt-3 pl-lg-0">
            <div class="card border h-100 contact-list-section">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-3 text-right">Legend: </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md text-center" style="background-color: #C2BFED; font-size: 10px"> Lulus</div>
                                            <div class="col-md text-center" style="background-color: #F5BFBA; font-size: 10px">Tidak Lulus</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div id="peta" class="card-body"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('dist/vendors/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript">
        var mapOptions = {
            center: [-4.323435, 119.896298],
            zoom: 5.4
        }
        var peta = new L.map('peta', mapOptions);
        L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png').addTo(peta);
    </script>
@endpush
