{!! Form::model($model, [
    'route' => $model->exists ? ['admin.survey.update', $model->id] : 'admin.survey.store',
    'method' => $model->exists ? 'PATCH' : 'POST',
    'enctype' => $model->exists ?  'application/x-www-form-urlencoded' : "multipart/form-data"
]) !!}
<div class="form-group">
    {!! Form::label('name', 'Nama Survey', ['class' => 'col-sm col-form-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col">
        <div class="form-group">
            {!! Form::label('kategori_survey_id', 'Kategori', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::select('kategori_survey_id', $kategori, $model->kategori_survey_id ?? '', ['class' => 'form-control select-kategori']) !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            {!! Form::label('bobot_minimal', 'Nilai Minimal rata-rata', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::number('bobot_minimal', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="form-group">
            {!! Form::label('from', 'Mulai Tanggal', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::date('from', \Carbon\Carbon::now(),['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            {!! Form::label('until', 'Hingga Tanggal', ['class' => 'col-sm col-form-label']) !!}
            {!! Form::date('until', null,['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-2">Status</div>
    <div class="col-sm-10">
        <label class="chkbox"> Publish
            {!! Form::checkbox('is_publish','is_publish', (isset($model) && $model->is_publish == true ? true : false ), ['class' => 'form-check-input']) !!}
            <span class="checkmark"></span>
        </label>
    </div>
</div>
{!! Form::close() !!}

<script>
    $(".select-kategori").select2({
        placeholder : 'Kategori Survey',
        tags: true
    });

    if ($('.select-kategori').find("option[value='" + data.id + "']").length) {
        $('.select-kategori').val(data.id).trigger('change');
    }else{
        var newOption = new Option(data.text, data.id, true, true);
        $('.select-kategori').append(newOption).trigger('change');
    }
</script>
