@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('leaflet/leaflet.css') }}">
    <style type="text/css">
        #peta{
            height:500px;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Peta Infografis</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div id="peta" class="card-body"></div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="{{ asset('leaflet/leaflet.js') }}">
    </script>
    <script type="text/javascript">
        var mapOptions = {
            center: [-4.323435, 119.896298],
            zoom: 5.4
        }
        var peta = new L.map('peta', mapOptions);
        L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png').addTo(peta);
        var circle = L.circle([-4.448649, 119.804474], {
            fillColor: "yellow",
            fillOpacity: 0.2,
            radius: 30000,
            weight: 2
        }).addTo(peta);

        var circle = L.circle([-2.202304, 112.081870], {
            fillColor: "grey",
            fillOpacity: 0.2,
            radius: 30000,
            weight: 2
        }).addTo(peta);
        var circle = L.circle([-3.881917, 104.546561], {
            fillColor: "blue",
            fillOpacity: 0.2,
            radius: 30000,
            weight: 2
        }).addTo(peta);
        var circle = L.circle([-7.281173, 109.453444], {
            fillColor: "yellow",
            fillOpacity: 0.2,
            radius: 30000,
            weight: 2
        }).addTo(peta);
        var circle = L.circle([-7.022400, 110.986167], {
            fillColor: "blue",
            fillOpacity: 0.2,
            radius: 30000,
            weight: 2
        }).addTo(peta);
        var circle = L.circle([-3.588238, 119.890308], {
            fillColor: "yellow",
            fillOpacity: 0.2,
            radius: 30000,
            weight: 2
        }).addTo(peta);
        var circle = L.circle([-1.238422, 132.667725], {
            fillColor: "blue",
            fillOpacity: 0.2,
            radius: 30000,
            weight: 2
        }).addTo(peta);
    </script>
@endpush
