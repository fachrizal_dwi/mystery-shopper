<link rel="stylesheet" href="{{ asset('dist/vendors/summernote/summernote-bs4.css')}}" />

{!! Form::model($model, [
    'route' => $model->exists ? ['admin.how-to.update', $model->id] : 'admin.how-to.store',
    'method' => $model->exists ? 'PATCH' : 'POST',
    'enctype' => $model->exists ?  'application/x-www-form-urlencoded' : "multipart/form-data",
    'id' => "form-how-to-page"
]) !!}
<div class="form-group">
    {!! Form::label('title', 'Judul', ['class' => 'col-sm col-form-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <textarea id="summernote" class="summernote" name="content">{{$model->content ?? ''}}</textarea>
</div>
{!! Form::close() !!}

<script>
    $(document).ready(function() {
        $('.summernote').summernote();
    });
</script>

<script src="{{ asset('dist/vendors/summernote/summernote-bs4.js')}}"></script>
<script src="{{ asset('dist/js/summernote.script.js')}}"></script>
