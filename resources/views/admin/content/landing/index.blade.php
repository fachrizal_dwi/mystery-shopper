@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('dist/vendors/summernote/summernote-bs4.css')}}" />
@endpush

@section('breadcumb')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Content</h4></div>
                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a>Landing</a></li>
                    <li class="breadcrumb-item active"><a >Page</a></li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-sm-12 mt-3">
            <div class="card">
                <div class="card-header text-right">
                    <a href="{{ route('admin.landing-page.create') }}" class="btn btn-primary pull-right modal-show" style="margin-top: -8px;" title="Isi Data Survey"><i class="icon-plus"></i> Edit Landing Page </a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5>{{ $model->title ?? '' }}</h5>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col">
                            {!! $model->content ?? '' !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@include('layouts._modal')
@push('scripts')
    <script src="{{ asset('dist/vendors/summernote/summernote-bs4.js')}}"></script>
    <script src="{{ asset('dist/js/summernote.script.js')}}"></script>
    <script src="{{ asset('js/admin/content/landing/index.js') }}"></script>
@endpush
