@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="{{ asset('leaflet/leaflet.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{ asset('dist/vendors/datatable/css/dataTables.bootstrap4.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('dist/vendors/select2/css/select2.min.css')}}"/>
@endpush

@section('content')
    <div class="row">
        <div class="col-12  align-self-center">
            <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
                <div class="w-sm-100 mr-auto"><h4 class="mb-0">Pendaftaran Surveyor</h4></div>

                <ol class="breadcrumb bg-transparent align-self-center m-0 p-0">
                    <li class="breadcrumb-item"><a href="#">Surveyor</a></li>
                    <li class="breadcrumb-item"><a href="#">List</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header  justify-content-between align-items-center">
                    <div class="panel-body">
                        <form method="POST" id="search-form" class="form-inline" role="form">
                            <div class="form-group">
                                <label for="inputStatus">Status &nbsp;</label>
                                <select id="inputStatus" name="inputStatus" class="form-control">
                                    <option value="0" selected>Menunggu Approval</option>
                                    <option value="1">Terdaftar</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table" style="font-size:12px !important;">
                            <thead>
                            <tr>
                                <th scope="col">Nama</th>
                                <th scope="col">Universitas</th>
                                <th scope="col">Provinsi</th>
                                <th scope="col">Kota</th>
                                <th scope="col">#</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('admin.surveyor._modal')
@push('scripts')
    <script src="{{ asset('dist/vendors/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/datatable/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('dist/vendors/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{ asset('dist/js/gallery.script.js')}}"></script>
    <script src="{{ asset('dist/vendors/select2/js/select2.full.min.js')}}"></script>
    <script>
        var oTable = $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            cache: false,
            ajax: {
                url : "{{ route('admin.data.surveyor-approval') }}",
                type: 'GET',
                data: function (d) {
                    d.inputStatus = $('#inputStatus').val();
                }
            },
            columns: [
                {data: 'nama_lengkap', name: 'nama_lengkap'},
                {data: 'universitas_name', name: 'universitas_name'},
                {data: 'provinsi_name', name: 'provinsi_name'},
                {data: 'kota_kabupaten_name', name: 'kota_kabupaten_name', searchable : false, orderable: false},
                {data: 'action', name: 'action', orderable: false}
            ]
        });
        function reload(){
            oTable.draw();
        };

        $('#inputStatus').on('change', function(e) {
            oTable.draw();
            e.preventDefault();
        });

        $('#inputStatus').select2();
    </script>
    <script src="{{ asset('js/admin/surveyor/index.js') }}"></script>
@endpush
