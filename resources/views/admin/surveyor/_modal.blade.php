<div class="modal fade text-xs-left modal-card" id="modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" id="modal-header">
                <h4 class="modal-title" id="modal-title">Form Input</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body" id="modal-body">

            </div>

            <div class="modal-footer" id="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                <a href="#" class="btn btn-danger" id="modal-btn-reject" title="Reject">Reject</a>
                <button type="button" class="btn btn-primary" id="modal-btn-save">Approve</button>
            </div>
        </div>
    </div>
</div>
