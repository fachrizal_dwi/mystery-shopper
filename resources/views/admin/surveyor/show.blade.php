<style type="text/css">
    #peta{
        height:500px;
    }
</style>
<div id="grid" class="row">
    <div class="item col-12 col-md-6 col-lg-4 mb-4 text-center">
        <img src="{{ asset($model->photo_ktp)}}" alt="" class="portfolioImage img-fluid" style="max-height: 200px;">
        <div class="d-flex">
            <a data-fancybox-group="gallery" href="{{ asset($model->photo_ktp)}}" class="fancybox btn rounded-0 btn-dark w-100">Lihat KTP</a>
        </div>

    </div>
    <div class="item col-12 col-md-6 col-lg-4 mb-4 cation-hover text-center">
        <div class="modImage">
            <img src="{{ asset($model->photo_selfie)}}" alt="" class="portfolioImage img-fluid" style="max-height: 200px;">
            <div class="d-flex">
                <a data-fancybox-group="gallery" href="{{ asset($model->photo_selfie)}}" class="fancybox btn rounded-0 btn-dark w-100">Lihat Selfie</a>
            </div>
        </div>
    </div>
    <div class="item col-12 col-md-6 col-lg-4 mb-4 cation-hover text-center">
        <div class="modImage">
            <img src="{{ asset($model->photo_legal)}}" alt="" class="portfolioImage img-fluid" style="max-height: 200px;">
            <div class="d-flex">
                <a data-fancybox-group="gallery" href="{{ asset($model->photo_legal)}}" class="fancybox btn rounded-0 btn-dark w-100">Lihat Surat Penugasan</a>
            </div>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped" style="font-size:12px !important;">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">Nama</th>
            <td>{{ $model->nama_lengkap ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">NIK</th>
            <td>{{ $model->nik ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">No. Handphone</th>
            <td>{{ $model->phone ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">Tempat Lahir</th>
            <td>{{ $model->tempat_lahir ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">Tanggal Lahir</th>
            <td>{{ $model->tanggal_lahir ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">Kota</th>
            <td>{{ $model->kotaKabupaten->name ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">Universitas</th>
            <td>{{ $model->universitas->name ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">NIM</th>
            <td>{{ $model->nim ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">Provinsi</th>
            <td>{{ $model->provinsi->name ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">Kota / Kabupaten</th>
            <td>{{ $model->kotaKabupaten->name ?? '' }}</td>
        </tr>
        <tr>
            <th scope="row">Alamat</th>
            <td>{{ $model->alamat ?? '' }}</td>
        </tr>
        </tbody>
    </table>
    {!! Form::model($model, [
        'route' => $model->exists ? ['admin.surveyor-approval.update', $model->user_id] : 'admin.surveyor-approval.store',
        'method' => $model->exists ? 'PATCH' : 'POST',
        'enctype' => $model->exists ?  'application/x-www-form-urlencoded' : "multipart/form-data"
    ]) !!}
    {!! Form::close() !!}
    <div class="row">
        <div class="col">
            <div class="card">
                <div id="peta" class="card-body"></div>
            </div>
        </div>
    </div>
    <hr>
</div>
<script src="{{ asset('leaflet/leaflet.js') }}"></script>
<script>
    var mapOptions = {
        center: [{{ $model->latitude }}, {{ $model->longitude }}],
        zoom: 12
    }
    var peta = new L.map('peta', mapOptions);
    L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png').addTo(peta);
    var circle = L.circle([{{ $model->latitude }}, {{ $model->longitude }}], {
        fillColor: "blue",
        fillOpacity: 0.2,
        radius: 500,
        weight: 2
    }).addTo(peta);
</script>
<script src="{{ asset('dist/js/gallery.script.js')}}"></script>

